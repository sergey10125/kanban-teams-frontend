import React, { Component } from 'react'
import classNames from 'classnames'

export default class DropdownMenu extends Component {
  constructor() {
    super()
    this.state = {
      isOpen: false,
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.closeMenuOnOutsideClick)
  }

  showMenu = (event) => {
    event.preventDefault()
    this.setState({ isOpen: true }, () => {
      document.addEventListener('click', this.closeMenuOnOutsideClick)
    });
  }
  closeMenuOnOutsideClick = (event) => {
    if (!this.dropdownMenu.contains(event.target)) {
      this.setState({ isOpen: false }, () => {
        document.removeEventListener('click', this.closeMenuOnOutsideClick)
      })
    }
  }
  closeMenuAfter = (func) => () => {
    this.setState({ isOpen: false }, () => {
      if (func) func()
      document.removeEventListener('click', this.closeMenuOnOutsideClick)
    })
  }

  renderItem = (item, index) => {
    return (
      <div
        key={index}
        className="dd-menu__item"
        onClick={this.closeMenuAfter(item.action)}
      >
        {item.title}
      </div>
    )
  }

  render() {
    const { children, list, className } = this.props
    const { isOpen } = this.state

    return (
      <div className={classNames('dd-menu', className, {
          'dd-menu--open': isOpen
      })}>
        <div onClick={this.showMenu}>
          {children}
        </div>
        <div
          className={classNames('dd-menu__list', {
            'dd-menu__list': isOpen
          })}
          ref={(element) => {this.dropdownMenu = element}}
        >
          {isOpen &&
            list.map((item, index) => (this.renderItem(item, index)))
          }
        </div>
      </div>
    );
  }
}
