import React from 'react'

const NotFoundPage = () => (
  <div className='not-found'>Page not found</div>
)

export default NotFoundPage;
