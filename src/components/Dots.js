import React from 'react'
import classNames from 'classnames'

const sizeVocabulary = ['small', 'large']
const colorVocabulary = ['white']

const Dots = ({ className, size, color }) => {
  let additionalClass = []

  if (!!size && sizeVocabulary.includes(size)) {
    additionalClass.push(`dots--${size}`);
  };
  if (!!color && colorVocabulary.includes(color)) {
    additionalClass.push(`dots--${color}`);
  };

  const dotClassName = classNames('dots', additionalClass, className);

  return (
    <div className={dotClassName}>
      <div className='dots__item'></div>
      <div className='dots__item'></div>
      <div className='dots__item'></div>
    </div>
  );
};

export default Dots;
