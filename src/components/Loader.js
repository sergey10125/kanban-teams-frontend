import React, { useState, useRef, useEffect } from 'react'

 const Loader = () => {
   const [ isShow, setShowStatus ] = useState(false);
   const timer = useRef(null);

   useEffect(() => {
     timer.current = setTimeout(() => setShowStatus(true), 2000);
     return () => {
       if (!timer.current) return;
       clearTimeout(timer.current);
     }
   }, []);

   if (!isShow) return null;

   return (
     <div>Loading...</div>
   );
 };


export default Loader;
