import React from 'react'
import Select from 'react-select'

import errorConvertor from 'utils/errorConvertor'

export default ({
  input,
  options,
  label,
  disabled,
  isSearchable,
  meta: { touched, error }}
) => {
  const isError = touched && !!error

  const className = isError? 'select' : 'select--error'

  return (
    <div className='wrap-input'>
      <Select
        {...input}
        isSearchable={isSearchable || false}
        name={input.name}
        isDisabled={disabled}
        className={className}
        placeholder={label}
        optionClassName="needsclick"
        classNamePrefix="select"
        options={options}
        value={options.find(item => item.value === input.value)}
        onChange={(item) => input.onChange(item.value)}
        onBlur={(event) => {
          event.preventDefault()
        }}
      />
      {isError &&
        <span className="error input-error">{errorConvertor(error)}</span>
      }
    </div>
  )
}
