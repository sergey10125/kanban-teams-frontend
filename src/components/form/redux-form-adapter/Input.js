import React from 'react'

import Input from '../Input'

const ReduxFormInput = ({
  input,
  label,
  type,
  disabled,
  setRef,
  meta
}) => {

  const isError = meta && meta.touched && meta.error
  const error = isError? meta.error : null;

  const options = { label, type, ref: setRef, disabled, error, ...input };

  return <Input {...options} />
}

export default ReduxFormInput
