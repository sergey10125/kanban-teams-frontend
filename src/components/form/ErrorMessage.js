import React from 'react'

import errorConvertor from 'utils/errorConvertor'

const ErrorWrap = ({ error }) => (
  <div className='error-wrap'>
    {error && (
      <span className='error error-wrap__text'>
        {errorConvertor(error)}
      </span>
    )}
  </div>
)

export default ErrorWrap
