import React from 'react'
import classNames from 'classnames'

import errorConvertor from 'utils/errorConvertor'

const InputDOM = (props) => (<input placeholder="test" {...props} />);
const TextareaDOM = (props) => (<textarea placeholder="test" {...props} />);

const Input = ({
  label,
  type,
  error,
  multiline,
  ...props
}) => {
  const options = { placeholder: label, type, ...props };
  const InputElement = !!multiline ? TextareaDOM : InputDOM;

  return (
    <div className={classNames('wrap-input')}>
      <InputElement
        className={classNames('input', { 'input--error': !!error })}
        {...options}
      />
    {!!error && (
      <span className="error field-error">{errorConvertor(error)}</span>
    )}
    </div>
  )
}

export default Input;
