export default (...args) => {
  const object = {};
  args.forEach(epics => {
    Object.keys(epics).forEach(key => {
      if (!!object[key]) throw new Error(`Key '${key}' duplication`);
      object[key] = epics[key];
    });
  })

  return object;
}
