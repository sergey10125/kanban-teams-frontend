export default (url, method = 'GET', data) => {
  const methodArr = ['GET', 'POST', 'PUT', 'DELETE'];
  let urlString = url;

  if (!methodArr.includes(method))
    throw new Error(`Unexpected method ${method}`)

  const fetchOptions = {
    method: method,
    credentials: 'same-origin',
    headers: {}
  };

  if (data) {
    if (data.meta && data.meta.socketId) {
      const { meta } = data
      fetchOptions.headers['X-Socket-Id'] = meta.socketId
    }

    if (data.payload) {
      const body = data.payload
      if (method === 'GET') {
        urlString += '?'
        urlString += Object.keys(body).map(key => `${key}=${body[key]}`).join('&')
      } else if (body instanceof FormData) {
        fetchOptions.body = body;
      } else {
        fetchOptions.headers['Content-Type'] = 'application/json'
        fetchOptions.body = JSON.stringify(body);
      }
    }
  }


  return fetch(urlString, fetchOptions)
    .then((response) => {
        if (!response.ok) {
          const contentType = response.headers.get("content-type")
          if (contentType && contentType.indexOf("application/json") !== -1) {
            return response.json().then(errRespomse => {
              const error = new Error(response.status)
              error.response = errRespomse
              return Promise.reject(error)
            })
          }
          return Promise.reject(response.status)
        } else {
          return Promise.resolve(response)
        }
    });

}
