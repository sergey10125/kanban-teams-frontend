export default {
  required: () => 'This field is required',
  minLength: ([ limit ]) => `Use at least ${limit} characters`,
  maxLength: ([ limit ]) => `Use no more than ${limit} characters`,
  shouldByEmail: () => 'Incorrect email',
  authFail: () => 'Invalid email or password',
  emailExist: () => 'User with this email is already exist',
  inviteExist: () => 'This user is already invited or exist in the team'
}
