import vocabulary from './vocabulary';

export default (err) => {
  const [ errType, ...context ] = err.split(':');
  const convertorFunction = vocabulary[errType];
  return convertorFunction? convertorFunction(context) : err;
};
