export default (permissions) => (action) => {
  return permissions.some(item => item === action);
};
