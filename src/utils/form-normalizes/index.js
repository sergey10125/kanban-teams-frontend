export { default as combineNormalizes } from './combineNormalizes';

export { default as lowerCase } from './lowerCase';
export { default as upperCase } from './upperCase';
export { default as trim } from './trim';
