export default (funcArr) => (value) => {
  return funcArr.reduce((value, func) => func(value), value);
};
