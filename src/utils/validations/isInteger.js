export default (value) => {
  if (value && isNaN(Number(value)))
    return 'shouldByNumber'
  if (!Number.isInteger(+value))
    return 'shouldByInteger'

  return undefined
}
