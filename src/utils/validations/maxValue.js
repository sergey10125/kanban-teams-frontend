export default (maxValue) => (value) =>
  value && maxValue < value ?
    `maxValue:${maxValue}` : undefined
