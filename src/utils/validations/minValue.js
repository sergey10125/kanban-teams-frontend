export default (minValue) => (value) =>
  value && minValue > value ?
    `minValue:${minValue}` : undefined
