export default (maxLength) => (value) =>
  value && maxLength >= value.trim().length ?
    undefined : `maxLength:${maxLength}`
