export default (value) => {
  if (value && isNaN(Number(value)))
    return 'shouldByNumber'

  return undefined
}
