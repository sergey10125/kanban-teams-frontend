export default (value, allValues) =>{
  return value && value.trim() !== allValues.password.trim() ?
    'repeatPass'
    : undefined
}
