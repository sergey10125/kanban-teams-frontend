const letters = /^[0-9a-zA-Z ]+$/;

export default (value) => {
  if (value.match(letters)) {
    return undefined;
  } else {
    return 'requiredAlphanumeric';
  };
};
