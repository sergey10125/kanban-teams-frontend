export default (value) => {
  const splitCount = value.split(' ').length;
  if (splitCount === 1) {
    return undefined;
  } else {
    return 'spaceNotAllowed';
  }
}
