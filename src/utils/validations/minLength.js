export default (minLenght) => (value) =>
  value && minLenght <= value.trim().length ?
    undefined : `minLength:${minLenght}`
