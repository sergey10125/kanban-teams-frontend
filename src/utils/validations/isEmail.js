//eslint-disable-next-line
const emailRegularExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default (value) => {
  if (!emailRegularExp.test(value.toLowerCase()))
    return 'shouldByEmail'

  return undefined
}
