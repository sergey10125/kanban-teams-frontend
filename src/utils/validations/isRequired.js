export default (value) => {
  if (value) {
    if (typeof value === 'string') {
      const trimValue = value.trim();
      return trimValue? undefined : 'required';
    } else {
      return undefined;
    };
  } else {
    return 'required';
  }
}
