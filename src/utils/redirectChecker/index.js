import { reverse } from 'named-urls'
import routesMap from './routesMap'

function getRedirectUrl(currentUrl, meta, routesMap) {
	if (!routesMap.childrens) return false;

	const childrens = routesMap.childrens;
  let generatedUrl = '';
	const index = childrens.findIndex(children => {
  	generatedUrl = reverse(children.url, children.meta.reduce((obj, key) => {
			obj[key] = meta[key] || null;
			return obj
		}, {}));
    return currentUrl.indexOf(generatedUrl) === 0;
  });

  if (index === -1) return false;

  const extraParam = Object.keys(meta).filter(item => !childrens[index].meta.includes(item));

	if (extraParam.length) {
		return getRedirectUrl(currentUrl, meta, childrens[index]);
	};

 return reverse(routesMap.url, routesMap.meta.reduce((obj, key) => {
	 obj[key] = meta[key] || null;
	 return obj
 }, {}));
}

export default (currentUrl, meta) => {
	const riderectUrl = getRedirectUrl(currentUrl, meta, routesMap);
	return {
		isRedirect: !!riderectUrl,
		url: riderectUrl
	};
}
