export default {
  url: '/',
  meta: [],
  childrens: [
    {
      url: '/auth',
      meta: [],
      childrens: [
        { url: '/auth/signIn', meta: [] },
        { url: '/auth/signUp', meta: [] },
        { url: '/auth/restore', meta: [] },
        { url: '/auth/changePass', meta: [] },
      ]
    },
    { url: '/team/new', meta: [] },
    { url: '/team/join', meta: [] },
    {
      url: '/:teamId',
      meta: [ 'teamId' ],
      childrens: [
        { url: '/:teamId/edit', meta: [ 'teamId' ] },
        {
          url: '/:teamId/members',
          meta: [ 'teamId' ],
          childrens: [
            {
              url: '/:teamId/members/:memberId',
              meta: [ 'teamId', 'memberId' ],
            },
            {
              url: '/:teamId/members/invites',
              meta: [ 'teamId' ],
              childrens: [
                {
                  url: '/:teamId/members/invites/create',
                  meta: [ 'teamId' ],
                },
                {
                  url: '/:teamId/members/invites/:token/edit',
                  meta: [ 'teamId', 'token' ],
                }
              ]
            }
          ]
        },
        {
          url: '/:teamId/kanbans',
          meta: [ 'teamId' ],
          childrens: [
            {
              url: '/:teamId/kanbans/new',
              meta: [ 'teamId' ],
            },
            {
              url: '/:teamId/kanbans/:deskId/edit',
              meta: [ 'teamId', 'deskId' ],
            },
            {
              url: '/:teamId/kanbans/:deskId',
              meta: [ 'teamId', 'deskId' ],
              childrens: [
                {
                  url: '/:teamId/kanbans/:deskId/card/:cardId',
                  meta: [ 'teamId', 'deskId', 'cardId' ],
                }
              ]
            }
          ]
        },
        {
          url: '/:teamId/chats',
          meta: [ 'teamId' ],
          childrens: [
            {
              url: '/:teamId/chats/new',
              meta: [ 'teamId' ]
            },
            {
              url: '/:teamId/chats/:roomId',
              meta: [ 'teamId', 'roomId' ]
            },
            {
              url: '/:teamId/chats/:roomId/edit',
              meta: [ 'teamId', 'roomId' ]
            }
          ]
        }
      ]
    }
  ]
};
