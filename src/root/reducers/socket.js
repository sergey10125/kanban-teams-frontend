import { Map } from 'immutable';

import { LOGOUT_SUCCESS } from 'modules/auth/actions/types'
import * as types from '../constants/actionTypes/socket'


const initialState = new Map({
  isConnection: false,
  socketId: null
});

const setConnection = (state, { payload }) => state.merge({
  isConnection: true,
  socketId: payload.socketId
})


export default (state = initialState, action) => {
  switch (action.type) {
    case types.CREATE_CONNECTION_SUCCESS:
      return setConnection(state, action)
    case types.DISCONNECT_CONNECTION:
    case LOGOUT_SUCCESS:
      return initialState
    default:
      return state;
  }
}
