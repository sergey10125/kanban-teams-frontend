import { Map, List } from 'immutable';
import uuidV4 from 'uuid/v4';
import { LOCATION_CHANGE } from 'connected-react-router';

import { LOGOUT_SUCCESS } from 'modules/auth/actions/types'
import * as types from '../constants/actionTypes/system'

const modalStateInit = new Map({
  isOpen: false,
  modalType: null,
  context: null,
})

const notFoundStateInit = new Map({
  status: false,
  location: ''
})

const initialState = new Map({
  headerNotification: new List([]),
  modalState: modalStateInit,
  notFoundState: notFoundStateInit,
  isTabFocus: true
});


export const setTabFocus = (state, { status }) => {
  return state.set('isTabFocus', status);
}


// header error heandlers
const addHeaderError = (state, action) => {
  return state.update('headerNotification', list =>
    list.unshift({
      id: uuidV4(),
      ...action.payload
    })
  );
}

const removeHeaderError = (state, { id }) => {
  return state.update('headerList', list =>
    list.filter(item => item.get('id') !== id)
  );
}

// not fount state heandlers
const setNotFoundError = (state, { location }) => {
  return state.update('notFoundState', notFoundState => notFoundState.merge({
    status: true,
    location
  }))
}

const setModal = (state, { modalType, context }) => {
  return state.update('modalState', modalState => modalState.merge({
    isOpen: true,
    modalType: modalType,
    context: context
  }))
}

// modal state heandlers
const clearModal = (state) => {
  return state.update('modalState', modalState => modalStateInit)
}

const clearForLocationChange = (state) => {
  return state
    .update('notFoundState', notFoundState => notFoundStateInit)
    .update('modalState', modalState => modalStateInit)
}


export default (state = initialState, action) => {
  switch (action.type) {
    case types.SET_TAB_FOCUS:
      return setTabFocus(state, action.payload);
    case types.ADD_HEADER_ERROR:
      return addHeaderError(state, action.payload);
    case types.REMOVE_HEADER_ERROR:
      return removeHeaderError(state, action.payload);

    case types.SET_NOT_FOUNT_ERROR:
      return setNotFoundError(state, action.payload);

    case types.OPEN_MODAL:
      return setModal(state, action.payload);
    case types.CLOSE_MODAL:
      return clearModal(state);

    case LOCATION_CHANGE:
      return clearForLocationChange(state);
    case LOGOUT_SUCCESS:
      return initialState;
    default:
      return state;
  }
}
