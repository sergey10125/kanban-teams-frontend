import io from 'socket.io-client'

import * as socketActionTypes from 'root/actions/types/socket';
import { LOGOUT_SUCCESS } from 'modules/auth/actions/types';

import * as socketActions from 'root/actions/creators/socket';


const initSocket = (dispatch, socketActionMap) => {
  const socket = io().on('connect', () => {
   dispatch(socketActions.createConnectionSuccess(socket.id))
 });

 socket.on('disconnect', () => {
   dispatch(socketActions.disconnectConnection())
 });

 socket.on('joinRoomSuccess', (invite) => {
   dispatch(socketActions.joinSocketRoomSuccess(invite))
 });

 socket.on('action', ({ action: key, data, meta }) => {
   const action = socketActionMap[key];
   if (action) {
     dispatch(action(data, meta));
   };
 });

 return socket;
};


export default (socketActionMap) => {
  let socket = null;
  return (store) => (next) => action => {
    const { socket: socketStore } = store.getState()
    if (action.type.slice(0, 8) === 'REQUEST/' && socketStore.get('isConnection')) {
      action.meta = action.meta? action.meta: {};
      action.meta.socketId = socketStore.get('socketId');
    }

    const result = next(action)
    
    if (!socket && action.type === socketActionTypes.CREATE_CONNECTION_REQUEST) {
      socket = initSocket(store.dispatch, socketActionMap)
    }

    if (socket && action.type === socketActionTypes.JOIN_SOCKET_ROOM)  {
      const { token } = action.payload;
      socket.emit('joinRoom', { token })
    }

    if (socket && action.type === socketActionTypes.LEAVE_SOCKET_ROOM)  {
      const { roomType, id } = action.payload;
      socket.emit('leaveRoom', { roomType, id });
    }

    switch (action.type) {
      case socketActionTypes.DISCONNECT_CONNECTION:
      case LOGOUT_SUCCESS:
        socket.disconnect()
        socket = null
        break;
      default:
    }

    return result;
  };
}
