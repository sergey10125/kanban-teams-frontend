import { push } from 'connected-react-router'

import redirectCheck from 'utils/redirectChecker'
import { actionTypes } from 'modules'

const checkActions = [
  actionTypes.REMOVE_TEAM_USER_SUCCESS,
  actionTypes.REMOVE_KANBAN_SUCCESS,
  actionTypes.REMOVE_KANBAN_CARD_SUCCESS,
  actionTypes.REMOVE_CHAT_ROOM_SUCCESS,
];

const redirectMiddleware = store => next => action => {

  if (checkActions.includes(action.type)) {
    const { router } = store.getState();
    const { pathname } = router.location;

    const meta = { ...action.meta };
    delete meta.socketId;

    const redirectState = redirectCheck(pathname, meta);
    if (redirectState.isRedirect) {
      store.dispatch(push(redirectState.url));
    };
  };

  return next(action);
}

export default redirectMiddleware
