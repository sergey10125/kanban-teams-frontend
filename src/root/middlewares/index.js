import errorMiddleware from './error';
import socketConnect from './socketConnect';
import deleteRedirect from './deleteRedirect';

export default (socketActions, middlewares) => [
  errorMiddleware,
  deleteRedirect,
  socketConnect(socketActions),
  ...middlewares
]
