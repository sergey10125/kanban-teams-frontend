import { stopSubmit } from 'redux-form'

import rootTypes from '../actions/types'
import rootActions from '../actions/creators'

import { logoutSuccess } from 'modules/auth/actions/creators'

const errorMiddleware = store => next => action => {
  if (action.type === rootTypes.INIT_ERROR) {
    const { error, options } = action.payload;
    if (options.formName) {
      let response = error;
      if ((typeof error === "object") && (error !== null))
        response = error.response;

      next(stopSubmit(options.formName, response));
    }

    switch (error) {
      case 401:
        return next(logoutSuccess());
      case 404:
        const location = options.notFoundLocation || 'global';
        return next(rootActions.setNotFoundError(location));
      default:
        return next(rootActions.addHeaderError(error));
    }
  }

  return next(action);
}

export default errorMiddleware
