import * as systemActionCreator from './system'
import * as socketActionCreator from './socket'

export default {
  ...systemActionCreator,
  ...socketActionCreator
}
