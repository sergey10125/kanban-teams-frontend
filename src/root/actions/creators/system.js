import * as types from '../types/system'

export const setTabFocus = (status) => ({
  type: types.SET_TAB_FOCUS,
  payload: { status }
})

export const initError = (
  error,
  {
    formName,
    notFoundLocation
  }  = {}
) => ({
  type: types.INIT_ERROR,
  payload: {
    error,
    options: {
      formName,
      notFoundLocation
    }
  }
})

export const setNotFoundError = (location = 'global') => ({
  type: types.SET_NOT_FOUNT_ERROR,
  payload: { location }
})

export const addHeaderError = (error) => ({
  type: types.ADD_HEADER_ERROR,
  payload: error
})
export const removeHeaderError = (id) => ({
  type: types.REMOVE_HEADER_ERROR,
  payload: { id }
})

export const openModal = (modalType, context) => ({
  type: types.OPEN_MODAL,
  payload: { modalType, context }
})

export const closeModal = (payload) => ({
  type: types.CLOSE_MODAL,
})
