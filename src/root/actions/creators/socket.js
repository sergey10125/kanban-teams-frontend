import * as types from '../types/socket'

export const createConnectionRequest = () => ({
  type: types.CREATE_CONNECTION_REQUEST
})
export const createConnectionSuccess = (socketId) => ({
  type: types.CREATE_CONNECTION_SUCCESS,
  payload: { socketId }
})

export const joinSocketRoom = (token) => ({
  type: types.JOIN_SOCKET_ROOM,
  payload: { token }
})

export const joinSocketRoomSuccess = (payload) => ({
  type: types.JOIN_SOCKET_ROOM_SUCCESS,
  payload
})

export const leaveSocketRoom = (roomType, id) => ({
  type: types.LEAVE_SOCKET_ROOM,
  payload: { roomType, id }
})

export const disconnectConnection = (socketId) => ({
  type: types.DISCONNECT_CONNECTION
})
