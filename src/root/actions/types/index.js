import * as systemActionTypes from './system'
import * as socketActionTypes from './socket'

export default {
  ...systemActionTypes,
  ...socketActionTypes
}
