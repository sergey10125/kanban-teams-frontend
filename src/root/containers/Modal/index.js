import React, { Fragment } from 'react'
import { useDispatch, useSelector } from "react-redux";

import * as actions from '../../actions/creators/system'

import ConfirmModal from './modalTypes/ConfirmModal'
import InputModal from './modalTypes/InputModal'


const renderModal = (modalState, closeModal) => {
  switch (modalState.get('modalType')) {
    case "ConfirmModal":
      return (
        <ConfirmModal
          context={modalState.get('context')}
          closeModal={closeModal}
        />
      )
    case "InputModal":
      return (
        <InputModal
          context={modalState.get('context')}
          closeModal={closeModal}
        />
      )
    default:
      return null;
  }
}


const Modal = () => {
  const dispatch = useDispatch();

  const modalState = useSelector(({ system }) => system.get('modalState'));
  const closeModal = () => dispatch(actions.closeModal());

  if (!modalState.get('isOpen') || !modalState.get('modalType')) return null;

  return (
    <Fragment>
      <div key='overlay' className='overlay' onClick={closeModal}/>
      {renderModal(modalState, closeModal)}
    </Fragment>
  )
}

export default Modal
