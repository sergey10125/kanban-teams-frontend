import React, { Component } from 'react'
import { IoIosClose } from "react-icons/io";

class ConfirmModal extends Component {
  confirmSubmit = () => {
    const { context, closeModal } = this.props;
    context.actionFun();
    closeModal();
  }

  render() {
    const { context, closeModal } = this.props;
    return (
      <div className='modal'>
        <IoIosClose onClick={closeModal} className='modal__closer'/>
        <div className='modal__text'>{context.message}</div>
        <div className='modal__button-bar'>
          <button onClick={closeModal} className='modal__button'>
            Cansel
          </button>
          <button
            className='modal__button modal__button--alert'
            onClick={this.confirmSubmit}
          >
            Yes
          </button>
        </div>
      </div>
    )
  }
}

export default ConfirmModal
