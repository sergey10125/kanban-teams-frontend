import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { IoIosClose } from "react-icons/io";

import Input from 'components/form/redux-form-adapter/Input'
import ErrorMessage from 'components/form/ErrorMessage'

class InputModal extends Component {
  handleSubmit = ({ input }) => {
    const { context, closeModal } = this.props;
    context.actionFun(input);
    closeModal();
  }
  closeModal = (e) => {
    e.preventDefault();
    this.props.closeModal();
  }

  render() {
    const {
      handleSubmit,
      context,
      closeModal,
      submitting,
      error
    } = this.props;
    return (
      <div className='modal'>
        <IoIosClose onClick={closeModal} className='modal__closer'/>
        <div className='modal__text modal__text--bp10'>{context.message}</div>
        <form className="modal-form" onSubmit={handleSubmit(this.handleSubmit)}>
          <div className="modal-form__input">
            <Field
              component={Input}
              type="text"
              name='input'
              label={context.label || 'input'}
              validate={context.validate || []}
              disabled={submitting}
            />
          </div>
          <ErrorMessage error={error}/>
          <div className='modal__button-bar'>
            <button onClick={this.closeModal} className='modal__button'>
              Cansel
            </button>
            <button
              className='modal__button modal__button--access'
              onClick={this.confirmSubmit}
            >
              Change
            </button>
          </div>
        </form>
      </div>
    )
  }
}

export default reduxForm({
  form: 'inputModalForm',
  touchOnBlur: false
})(InputModal)
