import React, { useMemo } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { push } from 'connected-react-router'
import { useDispatch, useSelector } from "react-redux";

import Select from 'react-select'
import * as authActions from 'modules/auth/actions/creators'
import DropdownMenu from 'components/DropdownMenu'


const Header = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const isLoadedTeamList = useSelector(({ team }) => team.get('isLoaded'));
  const teamList = useSelector(({ team }) => team.get('list'));
  const auth = useSelector((state) => state.auth);

  // target url path part has index 1
  // example: host/{teamId}/members
  const pathnameArr = location.pathname.split('/');
  const teamId = +pathnameArr[1];

  const changeTeam = ({ value }) => dispatch(push(`/${value}`));
  const selectList = [ { value: 'team/new', label: 'Create a new team' } ];
  teamList.forEach((item) => {
    selectList.push({ value: item.get('id'), label: item.get('title') })
  });


  const selectTeam = useMemo(() => {
    return selectList.find(item => item.value === teamId)
  }, [ selectList, teamId ]);
  const menuList = useMemo(() => ([
    { title: 'Logout', action: () => dispatch(authActions.logout()) }
  ]), [ dispatch ]);

  return (
    <header className='header'>
      <Link to='/' className='header__logo'>Kanban teams</Link>
      {isLoadedTeamList &&
        <Select
          name='teamList'
          className='teamSelect'
          value={selectTeam}
          onChange={changeTeam}
          options={selectList}
        />
      }
      {auth.get('loggedIn') &&
        <DropdownMenu className='header__nav' list={menuList}>
          <div className='header-nav'>
            {auth.getIn(['user', 'username'])[0].toUpperCase()}
          </div>
        </DropdownMenu>
      }
    </header>
  )
}

export default Header
