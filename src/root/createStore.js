import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import { createEpicMiddleware } from 'redux-observable'
import { connectRouter } from 'connected-react-router'
import { combineEpics } from 'redux-observable';

import { reducer as formReducer } from 'redux-form'
import { routerMiddleware } from 'connected-react-router'

import systemReducer from './reducers/system'
import socketReducer from './reducers/socket'

import middlewaresConstructor from './middlewares'


export default ({
  history,
  middlewares: modulesMiddlewares,
  reducers: modulesReducers,
  epics: modulesEpics,
  socketActions: modulesSocketActions
}) => {
  const epicMiddleware = createEpicMiddleware();

  let composeEnhancers = compose
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  }

  const enhacer = composeEnhancers(applyMiddleware(
    routerMiddleware(history),
    epicMiddleware,
    ...middlewaresConstructor(modulesSocketActions, modulesMiddlewares),
  ))

  const reducers = combineReducers({
    router: connectRouter(history),
    form: formReducer,
    system: systemReducer,
    socket: socketReducer,
    ...modulesReducers,
  })

  const store = createStore(reducers, {}, enhacer);

  epicMiddleware.run(combineEpics(
    ...Object.values(modulesEpics)
  ));

  return store;
}
