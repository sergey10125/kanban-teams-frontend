import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'

import AppRoutes, { reducers, epics, middlewares, socketActions } from './modules'
import * as serviceWorker from './serviceWorker'
import createStore from './root/createStore';

import './styles/index.sass'

const history = createBrowserHistory();
const store = createStore({
  history,
  middlewares,
  reducers,
  epics,
  socketActions
});


render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <AppRoutes />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister();
