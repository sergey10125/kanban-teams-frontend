import React, { useEffect } from 'react'
import { Switch, Route, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";

import hasTeamPermissionInit from 'utils/hasTeamPermissionInit'
import { joinSocketRoom, leaveSocketRoom } from 'root/actions/creators/socket';
import Loader from 'components/Loader';
import NotFoundPage from 'components/NotFoundPage';

import * as actions from '../actions/creators'

import EditChatRoomForm from '../containers/form/EditChatRoomForm';


const SelectChatRoomRoutes = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');
  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );

  const chatState = useSelector(({ chat }) => chat.get('selectRoom'));
  const chatId = chatState.getIn(['data', 'id']);
  const isLoaded = chatState.get('isLoaded');

  const isSocketConnected = chatState.get('isSocketConnected');
  const connectedToken = chatState.get('connectedToken');

  const paramChatRoomId = +params.chatRoomId;

  useEffect(() => {
    if (chatId !== paramChatRoomId) {
      dispatch(actions.fetchChatRoom(teamId, paramChatRoomId));
      if (isSocketConnected) {
        dispatch(leaveSocketRoom('chat', chatId));
      };
    };

    if (isLoaded && !isSocketConnected) {
      dispatch(joinSocketRoom(connectedToken));
    };
  });


  if (!isLoaded || !isSocketConnected) {
    return <Loader />;
  };

  return (
    <Switch>
      {hasTeamPermission('updateChatRoom') && (
        <Route
          exact
          path='/:teamId/chats/:chatRoomId/edit'
          component={EditChatRoomForm}
        />
      )}
      <Route path='*' component={NotFoundPage} />
    </Switch>
  )
}

export default SelectChatRoomRoutes;
