import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { useSelector } from "react-redux";

import hasTeamPermissionInit from 'utils/hasTeamPermissionInit'
import NotFoundPage from 'components/NotFoundPage';

import chatPage from '../containers/chatPage';
import ChatRoomList from '../containers/ChatRoomList';
import CreatChatRoomForm from '../containers/form/ChatRoomForm';

import SelectDeskRoutes from './selectChatRoom';

const KanbanRoutes = () => {
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );


  let createChatPageComponent = CreatChatRoomForm;
  if (!hasTeamPermission('editDesk')) {
    createChatPageComponent = NotFoundPage;
  }

  return (
    <Switch>
      <Route exact path='/:teamId/chats' component={ChatRoomList} />
      <Route exact path='/:teamId/chats/new' component={createChatPageComponent} />
      <Route exact path='/:teamId/chats/:chatRoomId' component={chatPage} />
      <Route path='/:teamId/chats/:chatRoomId' component={SelectDeskRoutes} />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  )
};

export default KanbanRoutes;
