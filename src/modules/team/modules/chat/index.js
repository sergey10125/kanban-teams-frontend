import * as actionTypes from './actions/types'
import * as actionCreators from './actions/creators'
import * as epics from './epics'
import reducers from './reducers'
import routes from './routes'
import middlewares from './middlewares'
import socketActions from './socketActions';


export default {
  actionTypes,
  actionCreators,
  epics,
  reducers: { chat: reducers },
  middlewares,
  routes,
  socketActions
}
