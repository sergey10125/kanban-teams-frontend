import { from } from 'rxjs'
import request from 'utils/request'

const api = {
  fetchChatRoomList: (payload, { teamId, ...meta }) => {
    const url = `/api/team/${teamId}/chat`;
    const req = request(url, 'GET', { meta })
      .then(response => response.json());

    return from(req);
  },


  fetchChatRoom: (payload, { teamId, roomId, ...meta }) => {
    const url = `/api/team/${teamId}/chat/${roomId}`;
    const req = request(url, 'GET', { meta })
      .then(response => response.json());

    return from(req);
  },
  createChatRoom: (payload, { teamId, ...meta }) => {
    const url = `/api/team/${teamId}/chat`;
    const req = request(url, 'POST', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  clearChatRoomNotification: (payload, { teamId, roomId, ...meta }) => {
    const url = `/api/team/${teamId}/chat/${roomId}/notification`;
    const req = request(url, 'DELETE', { meta });

    return from(req);
  },
  updateChatRoom: (payload, { teamId, roomId, ...meta }) => {
    const url = `/api/team/${teamId}/chat/${roomId}`;
    const req = request(url, 'PUT', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  removeChatRoom: (payload, { teamId, roomId, ...meta }) => {
    const url = `/api/team/${teamId}/chat/${roomId}`;
    const req = request(url, 'DELETE', { meta });

    return from(req);
  },


  fetchChatMessageList: (payload, { teamId, roomId, ...meta }) => {
    const url = `/api/team/${teamId}/chat/${roomId}/message`;
    const req = request(url, 'GET', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  createChatMessage: (payload, { teamId, roomId, ...meta }) => {
    const url = `/api/team/${teamId}/chat/${roomId}/message`;
    const req = request(url, 'POST', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  removeChatMessage: (payload, { teamId, roomId, ...meta }) => {
    const url = `/api/team/${teamId}/chat/${roomId}/message`;
    const req = request(url, 'DELETE', { meta });

    return from(req);
  },
};

export default api;
