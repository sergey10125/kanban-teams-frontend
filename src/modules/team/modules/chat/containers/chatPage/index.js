import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";

import { joinSocketRoom, leaveSocketRoom } from 'root/actions/creators/socket';
import Loader from 'components/Loader';

import * as actions from '../../actions/creators';

import ChatSidebar from './Sidebar';
import ChatMessageList from './messageList';


const ChatPage = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');

  const chatState = useSelector(({ chat }) => chat.get('selectRoom'));
  const roomId = chatState.getIn(['data', 'id']);
  const isLoaded = chatState.get('isLoaded');
  const isLoadedMessages = chatState.getIn(['messages', 'isLoaded']);

  const isSocketConnected = chatState.get('isSocketConnected');
  const connectedToken = chatState.get('connectedToken');

  const paramChatRoomId = +params.chatRoomId;


  useEffect(() => {
    if (roomId !== paramChatRoomId) {
      dispatch(actions.fetchChatRoom(teamId, paramChatRoomId));
      if (isSocketConnected) {
        dispatch(leaveSocketRoom('chat', roomId));
      };
    };
  }, [ teamId, roomId, paramChatRoomId, dispatch, isSocketConnected ]);

  useEffect(() => {
    if (isLoaded && !isLoadedMessages) {
      dispatch(actions.fetchChatMessageList(teamId, roomId));
    };
  }, [ teamId, roomId, isLoaded, isLoadedMessages, dispatch ]);

  useEffect(() => {
    if (isLoaded && !isSocketConnected) {
      dispatch(joinSocketRoom(connectedToken));
    };
  }, [ isLoaded, isSocketConnected, connectedToken, dispatch ]);


  return (
    <div className='chat-container'>
      <ChatSidebar roomId={roomId}/>
      {!isLoadedMessages && (<Loader/>)}
      {isLoadedMessages && (<ChatMessageList roomId={roomId}/>)}
    </div>
  );
};

export default ChatPage;
