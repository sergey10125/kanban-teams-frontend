import React, {
  Fragment,
  useState,
  useCallback,
  useLayoutEffect,
  useRef
} from 'react';

import { useDispatch, useSelector } from "react-redux";

import usePrevious from 'utils/hooks/usePrevious';

import useScrollPosition from '../../helpers/useScrollPosition';
import * as actions from '../../actions/creators';
import Message from '../../components/Message';
import DateLabel from '../../components/DateLabel';

import ChatMessageInput from './messageInput';

const scrollToButtom = (el) => {
  if (!el.current) return;
  const target = el.current;
  target.scrollTop = target.scrollHeight - target.clientHeight;
};

const isScrollInButtom = (listDOM) => {
  const listEl = listDOM.current;
  const lastMsgEl = listEl.lastElementChild;

  return (listEl.offsetHeight + listEl.scrollTop + lastMsgEl.clientHeight) >= listEl.scrollHeight;
};


const ChatMessageList = ({ roomId }) => {
  const dispatch = useDispatch();

  const userId = useSelector(({ auth }) => auth.getIn(['user', 'id']));
  const isTabFocus = useSelector(({ system }) => system.get('isTabFocus'));
  const messageState = useSelector(({ chat }) =>
    chat.getIn(['selectRoom', 'messages'])
  );
  const teamId = useSelector(({ team }) =>
    team.getIn(['selectTeam', 'data', 'id'])
  );

  const messageList = messageState.get('list');
  const prevMsgListSize = usePrevious(messageList.size);
  const prevStateFlags = usePrevious(messageState.delete('list'));
  const [ newMsgStatus, setNewMsgStatus ] = useState(false)

  const listDOM = useRef(null);
  const scrollFromBottomRef = useRef(0);
  const firstNotReadMsg = useRef(null);

  const firstMsgRef = useRef(messageList.first());
  firstMsgRef.current = messageList.first();

  const messageStateRef = useRef(messageState);
  messageStateRef.current = messageState;


  const createMessage = useCallback((value) => {
    dispatch(actions.createChatMessage(teamId, roomId, value));
  }, [ teamId, roomId, dispatch ])

  const clearRoomNotification = useCallback(() => {
    firstNotReadMsg.current = false;
    dispatch(actions.clearChatRoomNotification(teamId, roomId));
    setNewMsgStatus(false);
  }, [ teamId, roomId, dispatch ])

  const fetchMessages = useCallback((lasteDateTime = null) => {
    dispatch(actions.fetchChatMessageList(teamId, roomId, lasteDateTime));
  },  [ teamId, roomId, dispatch ])



  // First render
  useLayoutEffect(() => {
    scrollToButtom(listDOM);
    clearRoomNotification();
  }, [ clearRoomNotification ])


  // Save scroll position for addition message loading time
  useLayoutEffect(() => {
    if (!prevStateFlags) return;

    const isAdditionLoading = messageState.get('isAdditionLoading');
    const prevIsAdditionLoading = prevStateFlags.get('isAdditionLoading');

    if (prevIsAdditionLoading && !isAdditionLoading) {
      const scrollFromBottom = scrollFromBottomRef.current;
      const listEl = listDOM.current;

      listEl.scrollTop =  listEl.scrollHeight - scrollFromBottom;
      scrollFromBottomRef.current = listEl.scrollTop;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ messageState, prevStateFlags ])


  // Heandle new message
  useLayoutEffect(() => {
    const messageList= messageState.get('list');

    if (
      !!prevStateFlags && !prevStateFlags.get('isAdditionLoading') &&
      prevMsgListSize !== messageList.size
    ) {

      const lastMsg = messageList.last();
      if (lastMsg.getIn(['author', 'id']) === userId) {
        if (newMsgStatus) {
          clearRoomNotification();
        };
        scrollToButtom(listDOM);
        return;
      }

      if (!firstNotReadMsg.current) {
        firstNotReadMsg.current = listDOM.current.lastElementChild;
        if (isScrollInButtom(listDOM) && isTabFocus) {
          clearRoomNotification();
          scrollToButtom(listDOM);
        } else {
          setNewMsgStatus(true);
        }
      }
    }
  // eslint-disable-next-line
  }, [ messageState ])

  useScrollPosition(({ scrollTop }) => {
    const listEl = listDOM.current;
    const messageState = messageStateRef.current;

    scrollFromBottomRef.current = listEl.scrollHeight - listEl.scrollTop;

    const isScrollOnTop = scrollTop < 250;
    const isAdditionLoading = messageState.get('isAdditionLoading');
    const isAllListFetch = messageState.get('isAllListFetch');

    if (isScrollOnTop && !isAllListFetch && !isAdditionLoading) {
      const firstMsg = firstMsgRef.current;
      fetchMessages(firstMsg.get('createdAt'));
    }

    const firstNotReadEl =  firstNotReadMsg.current;
    const listOffsetFromButtom = listEl.scrollTop + listEl.clientHeight;

    if (firstNotReadEl && listOffsetFromButtom + 0 > firstNotReadEl.offsetTop ) {
      clearRoomNotification();
    }
  }, listDOM);



  return (
    <div className="chat">
      <div ref={listDOM} className="chat__messages">
        {messageList.map((msg, i) =>
          <Fragment key={`cont${msg.get('id')}`}>
            <DateLabel
              message={msg}
              prevMessage={(i > 0)? messageList.get(i - 1) : null}
              isAllListFetch={messageState.get('isAllListFetch')}
            />
            <Message
              key={msg.get('id')}
              data={msg}
              isMine={msg.getIn(['author', 'id']) === userId}
              date={msg.get('createdAt')}
              />
          </Fragment>
        )}
      </div>
      <ChatMessageInput
        className="chat__input"
        createMessage={createMessage}
      />
    </div>
  )
}

export default ChatMessageList;
