import React, { useEffect } from 'react'
import { useDispatch, useSelector } from "react-redux";

import * as actions from '../../actions/creators'
import Loader from 'components/Loader'

import MessageList from './messageList';

const ChatMessageList = ({ roomId }) => {
  const dispatch = useDispatch();

  const teamId = useSelector(({ team }) =>
    team.getIn(['selectTeam', 'data', 'id'])
  );
  const roomId = useSelector(({ chat }) =>
    chat.getIn(['selectRoom', 'data', 'id'])
  );


  useEffect(() => {
    if (!messages.get('isLoaded')) {
      dispatch(actions.fetchChatMessageList(teamId, roomId));
    }
  }, []);


  if (!messageState.get('isLoaded')) return <Loader/>;
  return <MessageList teamId={teamId} roomId={roomId} />;
}

export default ChatMessageList;
