import React, { useEffect } from 'react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";

import Loader from 'components/Loader'
import * as actions from '../../actions/creators'

const ChatSidebar = ({ roomId }) => {
  const dispatch = useDispatch();
  const chatRoomList = useSelector(({ chat }) => chat.get('list'));
  const teamId = useSelector(({ team }) =>
    team.getIn(['selectTeam', 'data', 'id'])
  );

  useEffect(() => {
    if (!chatRoomList.get('isLoaded')) {
      dispatch(actions.fetchChatRoomList(teamId))
    }
  // eslint-disable-next-line
  }, [])

  return (
    <div className='chat-sidebar'>
      {!chatRoomList.get('isLoaded') && <Loader />}
      {chatRoomList.get('isLoaded') && chatRoomList.get('data').map(item =>
        <div
          key={item.get('id')}
          className={classNames('chat-sidebar-item', {
            'chat-sidebar-item--active': roomId === item.get('id')
          })}
        >
          <Link
            className='chat-sidebar-item__title'
            to={`/${teamId}/chats/${item.get('id')}`}
          >
            {item.get('title')}
            {!!item.get('notificationCount') &&
              <div className='chat-sidebar-item__notification'>
                {item.get('notificationCount')}
              </div>
            }
          </Link>
        </div>
      )}
    </div>
  )
}


export default ChatSidebar;
