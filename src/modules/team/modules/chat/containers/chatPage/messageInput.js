import React, { useState, useCallback } from 'react'
import classNames from 'classnames'

const MessageInput = ({ createMessage, className }) => {
  const [ content, setContent ] = useState('');

  const submitForm = useCallback((e) => {
    e.preventDefault();
    if (!content) return;
    createMessage(content);
    setContent('');
  }, [ content, createMessage ])

  return (
    <div className = {classNames('chat-input', className)}>
      <form className='chat-input__form' onSubmit={submitForm} >
        <input
          className='chat-input__input'
          placeholder='Write a message...'
          onChange={({ target }) => setContent(target.value)}
          value={content}
        />
        {!!content.length &&
          <button className='chat-input__button'>Send</button>
        }
      </form>
    </div>
  )
}


export default MessageInput
