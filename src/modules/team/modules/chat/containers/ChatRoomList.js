import React, { useEffect, useCallback } from 'react'
import { Link } from 'react-router-dom'
import { push } from 'connected-react-router'
import { useDispatch, useSelector } from "react-redux";

import Loader from 'components/Loader'
import Dots from 'components/Dots'
import DropdownMenu from 'components/DropdownMenu'
import hasTeamPermissionInit from 'utils/hasTeamPermissionInit'

import { openModal } from 'root/actions/creators/system'

import * as actions from '../actions/creators'


const ChatRoomList = () => {
  const dispatch = useDispatch();

  const listState = useSelector(({ chat }) => chat.get('list'));
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');

  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );

  const canEditChatRoom = hasTeamPermission('updateChatRoom');
  const canDeleteChatRoom = hasTeamPermission('removeChatRoom');

  const createDDmenu = useCallback((chatId) => {
    const redirectToChatRoomEdit = () => {
      const url = `/${teamId}/chats/${chatId}/edit`;
      dispatch(push(url));
    };
    const callDeleteModal = () => {
      dispatch(openModal('ConfirmModal', {
        message: 'Are you sure ?',
        actionFun: () => dispatch(actions.removeChatRoom(teamId, chatId))
      }));
    };

    const options = [];
    if (canEditChatRoom) {
      options.push({ title: 'Edit', action: redirectToChatRoomEdit });
    };
    if (canDeleteChatRoom) {
      options.push({ title: 'Delete', action: callDeleteModal });
    };

    return options
}, [ teamId, canEditChatRoom, canDeleteChatRoom, dispatch ]);


  useEffect(() => {
    if (listState.get('teamId') !== teamId) {
      dispatch(actions.fetchChatRoomList(teamId));
    };
  });


  if (!listState.get('isLoaded')) {
    return <Loader />;
  }

  const showDDMenu = canEditChatRoom || canDeleteChatRoom;

  return (
    <div className='page'>
      <h1 className='page__headline'>Chat room list</h1>
      <div className='page-toolbar'>
      {hasTeamPermission('createChatRoom') && (
        <Link
          className='btn page-toolbar__btn'
          to={`/${teamId}/chats/new`}
        >
          Create new chat room
        </Link>
      )}
      </div>
      <div className='table100'>
        <table>
          <thead>
            <tr className="row100 head">
              <th className="column100">Title</th>
              <th className="column100 col-dd"></th>
            </tr>
          </thead>
          <tbody>
            {listState.get('data').map(item =>
              <tr className='row100' key={item.get('id')} >
                <td className="column100 column1">
                  <Link
                    to={`/${teamId}/chats/${item.get('id')}`}
                    className='list-item__title link'
                  >
                    {item.get('title')}
                  </Link>
                </td>
                <td className="column100 col-dd">
                  {(!item.get('isMain') && showDDMenu) && (
                    <DropdownMenu
                      className='list-item__menu'
                      list={createDDmenu(item.get('id'))}
                    >
                      <Dots />
                    </DropdownMenu>
                  )}
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  )
}


export default ChatRoomList;
