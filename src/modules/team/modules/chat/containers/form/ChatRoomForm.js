import React, { useCallback } from 'react';
import { Field, reduxForm } from 'redux-form';
import { useDispatch, useSelector } from "react-redux";

import * as actions from '../../actions/creators';

import Input from 'components/form/redux-form-adapter/Input';
import ErrorMessage from 'components/form/ErrorMessage';

import * as validations from 'utils/validations';

const minLength3 = validations.minLength(3);
const maxLength60 = validations.maxLength(60);


const ChatRoomForm = ({ roomId, handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();

  const teamId = useSelector(({ team }) =>
    team.getIn(['selectTeam', 'data', 'id'])
  );


  const heandle = useCallback(({ title }) => {
    if (!roomId) {
      dispatch(actions.createChatRoom(teamId, title));
    } else {
      dispatch(actions.updateChatRoom(teamId, roomId, title));
    }
  }, [ teamId, roomId, dispatch ]);


  return (
    <div className='page-form'>
      <div className="page-form__headline">
        {!!roomId? 'Update chat room' : 'Creat new chat room'}
      </div>
      <form className="form" onSubmit={handleSubmit(heandle)}>
        <fieldset className="form-fieldset">
          <Field
            component={Input}
            name="title"
            type="text"
            label="Room title"
            validate={[
              validations.isRequired,
              minLength3,
              maxLength60,
            ]}
            disabled={submitting}
          />
        <ErrorMessage error={error}/>
        </fieldset>
        <button disabled={submitting} className="btn btn--w200">
          {!!roomId? 'Update' : 'Creat'}
        </button>
      </form>
    </div>
  );
};

export default reduxForm({
  form: 'chatRoomForm'
})(ChatRoomForm);
