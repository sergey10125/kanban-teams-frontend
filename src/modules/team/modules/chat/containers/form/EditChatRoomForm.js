import React from 'react';
import { useSelector } from "react-redux";

import ChatRoomForm from './ChatRoomForm'

const EditChatRoomForm  = () => {
  const selectRoom = useSelector(({ chat }) =>
    chat.getIn(['selectRoom', 'data'])
  );

  const initialValues = {
    title: selectRoom.get('title')
  };

  return (
    <ChatRoomForm
      roomId={selectRoom.get('id')}
      initialValues={initialValues}
    />
  );
};

export default EditChatRoomForm;
