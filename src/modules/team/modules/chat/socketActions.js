import * as actions from './actions/creators';

export default {
  'createChatRoom': actions.createChatRoomSuccess,
  'updateChatRoom': actions.updateChatRoomSuccess,
  'removeChatRoom': actions.removeChatRoomSuccess,

  'chatMessageNotification': actions.chatMessageNotification
};
