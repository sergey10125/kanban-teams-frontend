import { useEffect } from 'react';

import debounce from 'utils/behaviors/debounce';
import throttle from 'utils/behaviors/throttle';

const isBrowser = typeof window !== `undefined`;

function getScrollPosition(targetDOM) {
  if (!isBrowser || !targetDOM) return { x: 0, y: 0 };
  return targetDOM.scrollTop;
};

export default function useScrollPosition(effect, element) {

  const callBack = () => {
    const targetDOM = element.current;
    const scrollTop = getScrollPosition(targetDOM);
    effect({ scrollTop });
  }

  const scrollThrottle = throttle(callBack, 100);
  const scrollDebounce = debounce(callBack, 150);

  useEffect(() => {
    const targetDOM = element.current;

    targetDOM.addEventListener('scroll', scrollThrottle);
    targetDOM.addEventListener('scroll', scrollDebounce);

    return () => {
      targetDOM.removeEventListener('scroll', scrollThrottle);
      targetDOM.removeEventListener('scroll', scrollDebounce);
    }
  // eslint-disable-next-line
  }, []);
};
