import * as types from './types'

export const fetchChatRoomList = (teamId) => ({
  type: types.FETCH_CHAT_ROOM_LIST,
  meta: { teamId }
})
export const fetchChatRoomListSuccess = (payload, meta) => ({
  type: types.FETCH_CHAT_ROOM_LIST_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const fetchChatRoom = (teamId, roomId) => ({
  type: types.FETCH_CHAT_ROOM,
  meta: { teamId, roomId }
})
export const fetchChatRoomSuccess = (payload, meta) => ({
  type: types.FETCH_CHAT_ROOM_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const createChatRoom = (teamId, title) => ({
  type: types.CREATE_CHAT_ROOM,
  payload: { title },
  meta: { teamId }
})
export const createChatRoomSuccess = (payload, meta) => ({
  type: types.CREATE_CHAT_ROOM_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const clearChatRoomNotification = (teamId, roomId) => ({
  type: types.CLEAR_CHAT_ROOM_NOTIFICATION,
  meta: { teamId, roomId }
})

export const updateChatRoom = (teamId, roomId, title) => ({
  type: types.UPDATE_CHAT_ROOM,
  payload: { title },
  meta: { teamId, roomId }
})
export const updateChatRoomSuccess = (payload, meta) => ({
  type: types.UPDATE_CHAT_ROOM_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})


export const removeChatRoom = (teamId, roomId) => ({
  type: types.REMOVE_CHAT_ROOM,
  meta: { teamId, roomId }
})
export const removeChatRoomSuccess = (payload, meta) => ({
  type: types.REMOVE_CHAT_ROOM_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})



export const fetchChatMessageList = (teamId, roomId, lastDatetime = null) => ({
  type: types.FETCH_CHAT_MESSAGE_LIST,
  payload: { lastDatetime },
  meta: { teamId, roomId }
})
export const fetchChatMessageListSuccess = (payload, meta) => ({
  type: types.FETCH_CHAT_MESSAGE_LIST_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})


export const createChatMessage = (teamId, roomId, content) => ({
  type: types.CREATE_CHAT_MESSAGE,
  payload: { content },
  meta: { teamId, roomId }
})
export const createChatMessageSuccess = (payload, meta) => ({
  type: types.CREATE_CHAT_MESSAGE_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const chatMessageNotification = (payload, meta) => ({
  type: types.CHAT_MESSAGE_NOTIFICATION,
  payload: payload || {},
  meta: meta || {}
});

export const removeChatMessage = (teamId, roomId, msgId) => ({
  type: types.REMOVE_CHAT_MESSAGE,
  meta: { teamId, roomId, msgId }
})
export const removeChatMessageSuccess = (payload, meta) => ({
  type: types.REMOVE_CHAT_MESSAGE_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
