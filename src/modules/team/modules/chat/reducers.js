import { Map, List, fromJS } from 'immutable';

import { LOGOUT_SUCCESS } from 'modules/auth/actions/types';
import {
  JOIN_SOCKET_ROOM_SUCCESS,
  DISCONNECT_CONNECTION
} from 'root/actions/types/socket';


import { FETCH_TEAM } from 'modules/team/actions/types';

import * as types from './actions/types';

const MESSAGE_FETCH_LIST_LIMIT = 20;

const initMessageData = new Map({
  isLoaded: false,
  isAllListFetch: false,
  isAdditionLoading: false,
  list: new List()
});

const initSelectRoom = new Map({
  data: new Map({}),
  messages: initMessageData,
  isLoaded: false,
  connectedToken: null,
  isSocketConnected: false
});

const initRoomList = new Map({
  data: new List([]),
  teamId: null,
  isLoaded: false
});

const initialState = new Map({
  list: initRoomList,
  selectRoom: initSelectRoom
});

const fetchChatRoomList = (state, payload, meta) => {
  const { teamId } = meta;
  return state.update('list', list => initRoomList.set('teamId', teamId));
};

const fetchChatRoomListSuccess = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('list', listState => {
    if (listState.get('teamId') !== teamId) return listState;
    return listState.merge({
      data: fromJS(payload),
      isLoaded: true
    });
  });
};

const fetchChatRoom = (state, payload, meta) => {
  const { roomId } = meta;
  return state.update('selectRoom', selectRoom => {
    return initSelectRoom.setIn(['data','id'], roomId)
  });
};

const fetchChatRoomSuccess = (state, payload, meta) => {
  const { chat, invite } = payload;
  const { roomId } = meta;

  return state.update('selectRoom', selectRoom => {
    if (selectRoom.getIn(['data', 'id']) !== roomId) return selectRoom;
    return selectRoom.merge({
      data: fromJS(chat),
      connectedToken: invite,
      isLoaded: true,
    });
  });
};

const chatRoomSokcetConnected = (state, payload) => {
  if (payload.roomType !== 'chat') return state;
  return state.update('selectRoom', team => {
    if (team.getIn(['data', 'id']) !== payload.id) return team;
    return team.set('isSocketConnected', true)
  });
};

const createChatRoomSuccess = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('list', listState => {
    if (listState.get('teamId') !== teamId) return listState;
    return listState.update('data', list => list.push(fromJS({
      ...payload,
      notificationCount: 0
    })));
  });
};

const updateChatRoomSuccess = (state, payload, meta) => {
  const { roomId } = meta;

  return state
    .update('list', listState => {
      if (!listState.get('isLoaded')) return listState;
      return listState.update('data', list => {
        const index = list.findIndex(room => room.get('id') === roomId);
        if (index < 0) return list;
        return list.update(index, room => room.merge(payload))
      });
    })
    .update('selectRoom', roomState => {
      if (!roomState.get('isLoaded')) return roomState;
      return roomState.update('data', data => {
        if (data.get('id') !== roomId) return data;
        return fromJS(payload);
      });
    });
};

const removeChatRoomSuccess = (state, payload, meta) => {
  const { teamId, roomId } = meta;

  return state
    .update('list', listState => {
      if (listState.get('teamId') !== teamId) return listState;
      return listState.update('data', list => {
        return list.filter(item => item.get('id') !== roomId);
      });
    })
    .update('selectRoom', room => {
      if (room.get('isLoaded') && room.getIn(['data', 'id']) === roomId) {
        return initSelectRoom;
      } else {
        return room;
      }
    });
};

const clearChatRoomNotificationCount = (state, payload, meta) => {
  const { teamId, roomId } = meta;

  return state.update('list', listState => {
    if (listState.get('teamId') !== teamId) return listState;
    return listState.update('data', list => {
      const index = list.findIndex(room => room.get('id') === roomId);
      if (index < 0) return list;
      return list.update(index, room => room.set('notificationCount', 0))
    });
  });
};

const fetchChatMessageList = (state, payload, meta) => {
  const { lastDatetime } = payload
  const { roomId } = meta;

  return state.update('selectRoom', selectRoom => {
    if (selectRoom.getIn(['data', 'id']) !== roomId) return selectRoom;
    return selectRoom.update('messages', messages => messages.merge({
      isAdditionLoading: !!lastDatetime,
    }));
  });
};

const fetchChatMessageListSuccess = (state, payload, meta) => {
  const { roomId } = meta;

  return state.update('selectRoom', selectRoom => {
    if (selectRoom.getIn(['data', 'id']) !== roomId) return selectRoom;
    return selectRoom.update('messages', messages => {
      const isLoaded = messages.get('isLoaded')
      const list = messages.get('list')

      return messages.merge({
        list: isLoaded? fromJS(payload).concat(list) : fromJS(payload),
        isAllListFetch: payload.length < MESSAGE_FETCH_LIST_LIMIT,
        isAdditionLoading: false,
        isLoaded: true
      })
    });
  });
};

const chatMessageNotification = (state, payload, meta) => {
  const { teamId, roomId, itMyMessage } = meta;

  return state
    .update('list', listState => {
      if (itMyMessage || listState.get('teamId') !== teamId) return listState;
      return listState.update('data', list => {
        const index = list.findIndex(room => room.get('id') === roomId);
        if (index < 0) return list;
        return list.update(index, room =>
          room.update('notificationCount', count => count + 1 )
        )
      });
    })
    .update('selectRoom', selectRoom => {
      if (selectRoom.getIn(['data', 'id']) !== roomId) return selectRoom;
      return selectRoom.update('messages', messages => {
        if (!messages.get('isLoaded')) return messages;
        return messages.update('list', list => list.push(fromJS(payload)));
      });
    });
};

const addChatMessageSuccess = (state, payload, meta) => {
  const { roomId } = meta;

  return state.update('selectRoom', selectRoom => {
    if (selectRoom.getIn(['data', 'id']) !== roomId) return selectRoom;
    return selectRoom.update('messages', messages => {
      if (!messages.get('isLoaded')) return messages;
      return messages.update('list', list => list.push(fromJS(payload)));
    });
  });
};


export default (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_CHAT_ROOM_LIST:
      return fetchChatRoomList(state, action.payload, action.meta)
    case types.FETCH_CHAT_ROOM_LIST_SUCCESS:
      return fetchChatRoomListSuccess(state, action.payload, action.meta)
    case types.CREATE_CHAT_ROOM_SUCCESS:
      return createChatRoomSuccess(state, action.payload, action.meta)
    case types.UPDATE_CHAT_ROOM_SUCCESS:
      return updateChatRoomSuccess(state, action.payload, action.meta)
    case types.REMOVE_CHAT_ROOM_SUCCESS:
      return removeChatRoomSuccess(state, action.payload, action.meta)

    case types.CLEAR_CHAT_ROOM_NOTIFICATION:
      return clearChatRoomNotificationCount(state, action.payload, action.meta)

    case types.FETCH_CHAT_ROOM:
      return fetchChatRoom(state, action.payload, action.meta);
    case types.FETCH_CHAT_ROOM_SUCCESS:
      return fetchChatRoomSuccess(state, action.payload, action.meta);
    case JOIN_SOCKET_ROOM_SUCCESS:
      return chatRoomSokcetConnected(state, action.payload, action.meta);

    case types.FETCH_CHAT_MESSAGE_LIST:
      return fetchChatMessageList(state, action.payload, action.meta);
    case types.FETCH_CHAT_MESSAGE_LIST_SUCCESS:
      return fetchChatMessageListSuccess(state, action.payload, action.meta);
    case types.CHAT_MESSAGE_NOTIFICATION:
      return chatMessageNotification(state, action.payload, action.meta)
    case types.CREATE_CHAT_MESSAGE_SUCCESS:
      return addChatMessageSuccess(state, action.payload, action.meta);

    case DISCONNECT_CONNECTION:
    case LOGOUT_SUCCESS:
    case FETCH_TEAM:
      return initialState

    default:
      return state
  }
}
