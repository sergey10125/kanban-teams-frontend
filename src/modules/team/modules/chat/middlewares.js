import { CHAT_MESSAGE_NOTIFICATION } from './actions/types'

const accessNotification = store => next => action => {
  if (action.type === CHAT_MESSAGE_NOTIFICATION) {
    const { author: { id: authorId } } = action.payload;
    const { auth } = store.getState();
    const userId = auth.getIn(['user', 'id']);
    action.meta.itMyMessage = userId === authorId;
  }

  return next(action);
}

export default [
  accessNotification
]
