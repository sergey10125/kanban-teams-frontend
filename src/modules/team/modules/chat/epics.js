import { catchError, takeUntil, ignoreElements, map, mergeMap, take, tap } from 'rxjs/operators'
import { of, merge } from 'rxjs'
import { ofType } from 'redux-observable'
import { startSubmit } from 'redux-form'
import { LOCATION_CHANGE, push } from 'connected-react-router';

import { INIT_ERROR } from 'root/actions/types/system'
import { initError } from 'root/actions/creators/system'

import * as types from './actions/types'
import * as actions from './actions/creators'
import api from './api'

export const fetchChatRoomList = action$ => action$.pipe(
  ofType(types.FETCH_CHAT_ROOM_LIST),
  mergeMap(({ payload, meta}) => api.fetchChatRoomList(payload, meta).pipe(
    map(payload => actions.fetchChatRoomListSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_CHAT_ROOM_LIST)),
    catchError(error => of(initError(error))),
  ))
)

export const fetchChatRoom = action$ => action$.pipe(
  ofType(types.FETCH_CHAT_ROOM),
  mergeMap(({ payload, meta}) => api.fetchChatRoom(payload, meta).pipe(
    map(payload => actions.fetchChatRoomSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_CHAT_ROOM)),
    catchError(error => of(initError(error))),
  ))
)

export const createChatRoom = action$ => action$.pipe(
  ofType(types.CREATE_CHAT_ROOM),
  mergeMap(({ payload, meta }) => merge(
    of(startSubmit('chatRoomForm')),
    api.createChatRoom(payload, meta).pipe(
      map(payload => actions.createChatRoomSuccess(payload, meta)),
      catchError((error) => of(initError(error, {
        formName: 'chatRoomForm'
      })))
    ),
    action$.pipe(
      ofType(types.CREATE_CHAT_ROOM_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${meta.teamId}/chats/${payload.id}`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  ))
)

export const updateChatRoom = action$ => action$.pipe(
  ofType(types.UPDATE_CHAT_ROOM),
  mergeMap(({ payload, meta }) => merge(
    of(startSubmit('chatRoomForm')),
    api.updateChatRoom(payload, meta).pipe(
      map((payload) => actions.updateChatRoomSuccess(payload, meta)),
      catchError((error) => of(initError(error, {
        formName: 'chatRoomForm'
      })))
    ),
    action$.pipe(
      ofType(types.UPDATE_CHAT_ROOM_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${meta.teamId}/chats`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  ))
)

export const clearChatRoomNotification = action$ => action$.pipe(
  ofType(types.CLEAR_CHAT_ROOM_NOTIFICATION),
  tap(({ payload, meta }) => api.clearChatRoomNotification(payload, meta)),
  ignoreElements()
)

export const removeChatRoom = action$ => action$.pipe(
  ofType(types.REMOVE_CHAT_ROOM),
  mergeMap(({ payload, meta}) => api.removeChatRoom(payload, meta).pipe(
    map(() => actions.removeChatRoomSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)



export const fetchChatMessageListList = action$ => action$.pipe(
  ofType(types.FETCH_CHAT_MESSAGE_LIST),
  mergeMap(({ payload, meta}) => api.fetchChatMessageList(payload, meta).pipe(
    map(payload => actions.fetchChatMessageListSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_CHAT_MESSAGE_LIST)),
    catchError(error => of(initError(error))),
  ))
)

export const createChatMessage = action$ => action$.pipe(
  ofType(types.CREATE_CHAT_MESSAGE),
  mergeMap(({ payload, meta}) => api.createChatMessage(payload, meta).pipe(
    map(payload => actions.createChatMessageSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)
