import React from 'react'
import classNames from 'classnames'
import moment from 'moment'

const Message = ({ data, isMine = false, date = null }) => {
  const msgClassName = classNames('message', {
    'message--is-mine': isMine
  })

  return (
    <div className='message-wrap'>
      <div className={msgClassName}>
        <div className='message__author'>
          {data.getIn(['author', 'username'])}
        </div>
        <div className='message__content'>
          <div className='message__text'>
            {data.get('content')}
            <div className='message__date'>
              {moment(data.get('createdAt')).format('HH:mm')}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Message
