import React from 'react'
import moment from 'moment'

const DAY_IN_MILLISECOND = 1000 * 60 * 60 * 24;

const renderLabel = (date) => {
  const currentDate = new Date();
  const isYearNeed = currentDate.getFullYear() !== date.getFullYear();
  const formatStr = `MMMM D${isYearNeed? ', YYYY' : ''}`;

  return (
    <div key={date.getDate()} className='message-date-label'>
      {moment(date).format(formatStr)}
    </div>
  )
}

const DateLabel = ({ prevMessage, message, isAllListFetch = true }) => {
  const currentMsgDate = new Date(message.get('createdAt'));
  if (!prevMessage && isAllListFetch) return renderLabel(currentMsgDate);
  if (!prevMessage) return null;

  const prevMsgDate = new Date(prevMessage.get('createdAt'));

  const currentUTC = Date.UTC(
    currentMsgDate.getFullYear(),
    currentMsgDate.getMonth(),
    currentMsgDate.getDate()
  );
  const prevUTC = Date.UTC(
    prevMsgDate.getFullYear(),
    prevMsgDate.getMonth(),
    prevMsgDate.getDate()
  );

  if (DAY_IN_MILLISECOND <= currentUTC - prevUTC) {
    return renderLabel(currentMsgDate);
  } else {
    return null;
  }
}

export default DateLabel;
