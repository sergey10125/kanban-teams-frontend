import chat from './chat';
import kanban from './kanban';
import member from './member';

export default {
  chat,
  kanban,
  member
}
