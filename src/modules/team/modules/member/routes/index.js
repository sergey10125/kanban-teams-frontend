import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { useSelector } from "react-redux";

import hasTeamPermissionInit from 'utils/hasTeamPermissionInit';
import NotFoundPage from 'components/NotFoundPage';

import InviteList from '../containers/InviteList';
import MemberList from '../containers/MemberList';
import CreateInviteForm from '../containers/form/InviteForm';

import SelectInviteRoutes from './selectInvite';
import SelectMemberRoutes from './selectMember';

const memberMenegmentRouteArray = [
  <Route exact path='/:teamId/members/invites' component={InviteList} />,
  <Route exact path='/:teamId/members/invites/create' component={CreateInviteForm} />,
  <Route path='/:teamId/members/invites/:inviteToken' component={SelectInviteRoutes} />,
  <Route path='/:teamId/members/:memberId' component={SelectMemberRoutes} />
];

const MemberRoutes = () => {
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );

  return (
    <Switch>
      <Route exact path='/:teamId/members' component={MemberList} />
      {hasTeamPermission('memberMenegment') && memberMenegmentRouteArray}
      <Route path='*' component={NotFoundPage} />
    </Switch>
  );
};

export default MemberRoutes;
