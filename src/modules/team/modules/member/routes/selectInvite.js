import React, { useEffect } from 'react';
import { Switch, Route, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";

import Loader from 'components/Loader';
import NotFoundPage from 'components/NotFoundPage';

import UpdateInviteRoles from '../containers/form/UpdateInviteRoles';
import { fetchTeamInvite } from '../actions/creators';

const SelectInviteRoutes = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const inviteState = useSelector(({ member }) => member.get('selectInvite'));
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');

  const paramInviteToken = params.inviteToken;

  useEffect(() => {
    if (inviteState.getIn(['data', 'token']) !== paramInviteToken) {
      dispatch(fetchTeamInvite(teamId, paramInviteToken));
    };
  });

  if (!inviteState.get('isLoaded')) return <Loader />;

  return (
    <Switch>
      <Route
        path='/:teamId/members/invites/:inviteToken/edit'
        component={UpdateInviteRoles}
      />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  );
};

export default SelectInviteRoutes;
