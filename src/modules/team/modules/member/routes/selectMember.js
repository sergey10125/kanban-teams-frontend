import React, { useEffect } from 'react';
import { Switch, Route, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";

import Loader from 'components/Loader';
import NotFoundPage from 'components/NotFoundPage';

import UpdateMemberRoles from '../containers/form/UpdateMemberRoles';
import { fetchTeamMember } from '../actions/creators'

const SelectMemberRoutes = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const memberState = useSelector(({ member }) => member.get('selectMember'));
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');

  const paramMemberId = +params.memberId;

  useEffect(() => {
    if (memberState.getIn(['data', 'id']) !== paramMemberId) {
      dispatch(fetchTeamMember(teamId, paramMemberId));
    };
  });

  if (!memberState.get('isLoaded')) return <Loader />;

  return (
    <Switch>
      <Route
        path='/:teamId/members/:memberId/edit'
        component={UpdateMemberRoles}
      />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  )
}

export default SelectMemberRoutes;
