import * as actions from './actions/creators';

export default {
  'addTeamMember': actions.createTeamMemberNotification,
  'updateTeamMember': actions.updateTeamMemberRoleSuccess,
  'deleteTeamMember': actions.removeTeamMemberSuccess,

  'createTeamInvite': actions.createTeamInviteSuccess,
  'updateTeamInvite': actions.updateTeamInviteSuccess,
  'deleteTeamInvite': actions.removeTeamInviteSuccess
};
