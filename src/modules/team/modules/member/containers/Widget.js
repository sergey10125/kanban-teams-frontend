import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";

import Loader from 'components/Loader'
import hasTeamPermissionInit from 'utils/hasTeamPermissionInit'

import { fetchTeamMemberList } from '../actions/creators'


const MemberListWidget = () => {
  const dispatch = useDispatch();

  const listState = useSelector(({ member }) => member.get('memberList'));
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');

  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );

  useEffect(() => {
    if (listState.get('teamId') !== teamId) {
      dispatch(fetchTeamMemberList(teamId));
    };
  });

  return (
    <div className='widget-container'>
      <div className='widget'>
        <div className='widget__headline'>
          <Link to={`/${teamId}/members`} className='widget__link'>
            Member
          </Link>
          {hasTeamPermission('memberMenegment') &&
            <Link to={`/${teamId}/members/invites/create`} className='widget__create'>
              +
            </Link>
          }
        </div>
        <div className='widget__content'>
          {!listState.get('isLoaded') && <Loader />}
          {listState.get('isLoaded') &&
            listState.get('data').take(4).map(item =>
              <div className='widget-item' key={item.get('id')}>
                <div className='widget-item__title'>
                  {item.get('username')}
                </div>
              </div>
            )
          }
      </div>
      </div>
    </div>
  )
}


export default MemberListWidget;
