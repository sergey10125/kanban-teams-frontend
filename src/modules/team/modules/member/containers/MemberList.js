import React, { useCallback, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { push } from 'connected-react-router'
import { useDispatch, useSelector } from "react-redux";

import Loader from 'components/Loader'
import Dots from 'components/Dots'
import DropdownMenu from 'components/DropdownMenu'
import hasTeamPermissionInit from 'utils/hasTeamPermissionInit'

import { openModal } from 'root/actions/creators/system'

import { fetchTeamMemberList, removeTeamMember } from '../actions/creators'


const MemberListPage = () => {
  const dispatch = useDispatch();

  const listState = useSelector(({ member }) => member.get('memberList'));
  const userId = useSelector(({ auth }) => auth.getIn(['user', 'id']));
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');

  const hasTeamPermission = hasTeamPermissionInit(team.getIn(['userRoles', 'permissions']));
  const hasMemberMenegmentPermission = hasTeamPermission('memberMenegment');

  const canEditMember = useCallback((member) => {
    if (!hasMemberMenegmentPermission) return null;

    const memberIsMy = (userId === member.get('id'));
    const memberIsOwner = (member.get('roleTitle') === 'owner');

    return !memberIsMy && !memberIsOwner
  }, [ userId, hasMemberMenegmentPermission ])

  const createDDmenu = useCallback((memberId) => {
    const redirectToMemberEdit = () => {
      const url = `/${teamId}/members/${memberId}/edit`;
      dispatch(push(url));
    };
    const callDeleteModal = () => {
      dispatch(openModal('ConfirmModal', {
        message: 'Are you sure you want to remove a team member ?',
        actionFun: () => dispatch(removeTeamMember(teamId, memberId))
      }))
    }

    return [
      { title: 'Edit', action: redirectToMemberEdit },
      { title: 'Delete', action: callDeleteModal }
    ]
  }, [ teamId, dispatch ]);


  useEffect(() => {
    if (listState.get('teamId') !== teamId) {
      dispatch(fetchTeamMemberList(teamId));
    };
  });

  if (!listState.get('isLoaded')) {
    return <Loader />;
  }

  return (
    <div className='page'>
      <h1 className='page__headline'>Team members</h1>
        {hasTeamPermission('memberMenegment') && (
          <div className='page-toolbar'>
            <Link
              className='btn page-toolbar__btn'
              to={`/${teamId}/members/invites`}
            >
              Team invites
            </Link>
            <Link
              className='btn page-toolbar__btn'
              to={`/${teamId}/members/invites/create`}
            >
              Invite in team
            </Link>
          </div>
        )}
      <div className='table100 ver4'>
        <table>
          <thead>
            <tr className="row100 head">
              <th className="column100">Username</th>
              <th className="column100">E-mail</th>
              <th className="column100">Team role</th>
              <th className="column100 col-dd"></th>
            </tr>
          </thead>
          <tbody>
            {listState.get('data').map(item =>
              <tr className='row100' key={item.get('id')} >
                <td className="column100">
                  {item.get('username')}
                </td>
                <td className="column100">
                  {item.get('email')}
                </td>
                <td className="column100">
                  {item.get('roleTitle')}
                </td>
                <td className="column100 col-dd">
                  {canEditMember(item) &&
                    <DropdownMenu className='list-item__menu' list={createDDmenu(item.get('id'))}>
                      <Dots/>
                    </DropdownMenu>
                  }
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  )
}


export default MemberListPage;
