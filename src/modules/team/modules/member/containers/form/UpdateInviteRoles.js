import React from 'react';
import { useSelector } from "react-redux";

import InviteForm from './InviteForm';

const UpdateInviteRolesForm = () => {
  const inviteData = useSelector(({ member }) =>
    member.getIn(['selectInvite', 'data'])
  );

  const roles = {};
  inviteData.get('roleArray').mapKeys((key, value) => {
    roles[key] = value.get('id');
  });

  const initialValues = {
    email: inviteData.get('email'),
    roles
  }

  return (
    <InviteForm
      token={inviteData.get('token')}
      initialValues={initialValues}
    />
  )
}

export default UpdateInviteRolesForm;
