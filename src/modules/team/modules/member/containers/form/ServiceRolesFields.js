import React from 'react';
import { Field } from 'redux-form';

import Select from 'components/form/redux-form-adapter/Select';
import * as validations from 'utils/validations';

import * as constants from '../../constants';

const ServiceRolesFields = ({
  teamRoles,
  submitting,
  selectTeamRole
}) => {
  return teamRoles.map((serviceRoles, i) => {
    const options = serviceRoles.get('roles').map(role => ({
      value: role.get('id'),
      label: role.get('title'),
    }))

    if ((i >= 1) && (selectTeamRole === constants.ADMIN_ROLE_ID)) return null;

    return (
      <fieldset key={serviceRoles.get('id')} className='form-fieldset'>
        <div className='form-fieldset__headline'>
          Please select a role for {serviceRoles.get('title')}
        </div>
        <Field
          component={Select}
          name={`roles.${serviceRoles.get('title')}`}
          options={options}
          validate={validations.isRequired}
          disabled={submitting}
        />
      </fieldset>
    )
  });
}

export default ServiceRolesFields;
