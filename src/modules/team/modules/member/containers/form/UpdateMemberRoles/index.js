import React, { useEffect } from 'react'
import { useDispatch, useSelector } from "react-redux";

import Loader from 'components/Loader';

import * as actions from '../../../actions/creators';
import UpdateMemberRolesForm from './form';

const UpdateMemberRoles = () => {
  const dispatch = useDispatch();
  
  const teamRolesState = useSelector(({ member }) => member.get('teamRoles'));
  const selectMember = useSelector(({ member }) =>
    member.getIn(['selectMember', 'data'])
  );

  const roles = {};
  selectMember.get('roleArray').mapKeys((key, value) => {
    roles[key] = value.get('id');
  });

  const initialValues = { roles };

  useEffect(() => {
    if (!teamRolesState.get('isLoaded')) {
      dispatch(actions.fetchTeamRoles());
    }
  // eslint-disable-next-line
  }, [])

  if (!teamRolesState.get('isLoaded')) return <Loader />

  return (
    <UpdateMemberRolesForm initialValues={initialValues} />
  )
}


export default UpdateMemberRoles
