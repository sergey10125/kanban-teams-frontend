import React, { useCallback } from 'react';
import { reduxForm, formValueSelector } from 'redux-form';
import { useDispatch, useSelector } from "react-redux";

import ErrorMessage from 'components/form/ErrorMessage';

import * as actions from '../../../actions/creators';
import * as constants from '../../../constants';
import ServiceRolesFields from '../ServiceRolesFields';

const selector = formValueSelector('updateMemberRolesForm');

const UpdateMemberRolesForm = ({ token, handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();

  const selectTeamRole = useSelector((state) => selector(state, 'roles.team'));
  const teamId = useSelector(({ team }) =>
    team.getIn(['selectTeam', 'data', 'id'])
  );
  const member = useSelector(({ member }) =>
    member.getIn(['selectMember', 'data'])
  );
  const teamRoles = useSelector(({ member }) =>
    member.getIn(['teamRoles', 'data'])
  );


  const heandle = useCallback(({ email, roles }) => {
    let roleArray = Object.values(roles);
    if (roles[constants.TEAM_SERVICE_TITLE] === constants.ADMIN_ROLE_ID) {
      roleArray = [
        roles[constants.TEAM_SERVICE_TITLE]
      ];
    };

    dispatch(
      actions.updateTeamMemberRole(teamId, member.get('id'), roleArray)
    );
  }, [ teamId, member, dispatch ]);


  return (
    <div className='page-form'>
      <div className="page-form__headline">
        Update member roles
      </div>
      <form className="form" onSubmit={handleSubmit(heandle)}>
        <fieldset className="form-fieldset">
          <div>Username: {member.get('username')}</div>
          <div>Email: {member.get('email')}</div>
        </fieldset>
        <ServiceRolesFields
          teamRoles={teamRoles}
          selectTeamRole={selectTeamRole}
          disabled={submitting}
        />
        <ErrorMessage error={error} />
        <button disabled={submitting} className="btn btn--w200">
          Update roles
        </button>
      </form>
    </div>
  )
}


export default reduxForm({
  form: 'updateMemberRolesForm',
  enableReinitialize: true
})(UpdateMemberRolesForm);
