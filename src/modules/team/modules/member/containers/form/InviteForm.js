import React, { useEffect } from 'react';
import { withRouter } from 'react-router';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { useDispatch, useSelector } from "react-redux";

import { combineNormalizes, trim, lowerCase } from 'utils/form-normalizes';
import * as validations from 'utils/validations';

import Input from 'components/form/redux-form-adapter/Input';
import ErrorMessage from 'components/form/ErrorMessage';
import Loader from 'components/Loader';

import * as actions from '../../actions/creators';
import * as constants from '../../constants';
import ServiceRolesFields from './ServiceRolesFields';

const trimCaseNormalize = combineNormalizes([ trim, lowerCase ]);
const minLength3 = validations.minLength(3);
const maxLength30 = validations.maxLength(30);

const selector = formValueSelector('teamInviteForm');

const CreateInviteForm = ({ token, handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();

  const teamId = useSelector(({ team }) =>
    team.getIn(['selectTeam', 'data', 'id'])
  );
  const teamRoles = useSelector(({ member }) => member.get('teamRoles'));
  const selectTeamRole = useSelector((state) => selector(state, 'roles.team'));

  useEffect(() => {
    if (!teamRoles.get('isLoaded')) {
      dispatch(actions.fetchTeamRoles());
    }
  // eslint-disable-next-line
  }, [])


  const heandle = ({ email, roles }) => {
    let roleArray = Object.values(roles);
    email = trimCaseNormalize(email);

    if (roles[constants.TEAM_SERVICE_TITLE] === constants.ADMIN_ROLE_ID) {
      roleArray = [
        roles[constants.TEAM_SERVICE_TITLE]
      ];
    };

    if (token) {
      dispatch(actions.updateTeamInvite(teamId, token, roleArray ));
    } else {
      dispatch(actions.createTeamInvite(teamId, { email, roleArray }));
    }
  };

  if (!teamRoles.get('isLoaded')) return <Loader />

  return (
    <div className='page-form'>
      <div className="page-form__headline">
        {token? 'Edit invite' : 'Invite a new member'}
      </div>
      <form className="form" onSubmit={handleSubmit(heandle)}>
        <fieldset className="form-fieldset">
          <Field
            component={Input}
            name="email"
            type="text"
            label="E-mail"
            validate={[
              validations.isRequired,
              minLength3,
              maxLength30,
            ]}
            disabled={submitting || !!token}
          />
        </fieldset>
        <ServiceRolesFields
          teamRoles={teamRoles.get('data')}
          selectTeamRole={selectTeamRole}
          disabled={submitting}
        />
        <ErrorMessage error={error} />
        <button disabled={submitting} className="btn btn--w200">
          {token? 'Edit' : 'Invite'}
        </button>
      </form>
    </div>
  )
}


export default withRouter(
  reduxForm({ form: 'teamInviteForm' })(CreateInviteForm)
)
