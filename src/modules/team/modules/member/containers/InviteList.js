import React, { useEffect, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { push } from 'connected-react-router';
import { useDispatch, useSelector } from "react-redux";

import { openModal } from 'root/actions/creators/system';
import Loader from 'components/Loader';
import Dots from 'components/Dots';
import DropdownMenu from 'components/DropdownMenu';

import { fetchTeamInviteList, removeTeamInvite } from '../actions/creators';

const InviteListPage = () => {
  const dispatch = useDispatch();

  const inviteListState = useSelector(({ member }) => member.get('inviteList'));
  const teamId = useSelector(({ team }) => {
    return team.getIn(['selectTeam', 'data', 'id']);
  });


  const createDDmenu = useCallback((token) => {
    const redirectToInvite = () => {
      const url = `/${teamId}/members/invites/${token}/edit`;
      dispatch(push(url));
    };
    const callDeleteModal = () => {
      dispatch(openModal('ConfirmModal', {
        message: 'Are you sure you want to delete invite ?',
        actionFun: () => dispatch(removeTeamInvite(teamId, token)),
      }))
    };

    return [
      { title: 'Edit', action: redirectToInvite },
      { title: 'Delete', action: callDeleteModal }
    ]
  }, [ teamId, dispatch ]);


  useEffect(() => {
    if (inviteListState.get('teamId') !== teamId) {
      dispatch(fetchTeamInviteList(teamId));
    };
  });


  if (!inviteListState.get('isLoaded')) {
    return <Loader />;
  };

  return (
    <div className='page'>
      <h1 className='page__headline'>Team invite list</h1>
        <div className='page-toolbar'>
        <Link
          className='btn page-toolbar__btn'
          to={`/${teamId}/members`}
        >
          Team members
        </Link>
        <Link
          className='btn page-toolbar__btn'
          to={`/${teamId}/members/invites/create`}
        >
          Invite in team
        </Link>
      </div>
      <div className='table100 ver4'>
        <table>
          <thead>
            <tr className="row100 head">
              <th className="column100">E-mail</th>
              <th className="column100 col-dd"></th>
            </tr>
          </thead>
          <tbody>
            {inviteListState.get('data').map(item =>
              <tr className='row100' key={item.get('token')} >
                <td className="column100">
                  {item.get('email')}
                </td>
                <td className="column100 col-dd">
                  <DropdownMenu
                    className='list-item__menu'
                    list={createDDmenu(item.get('token'))}
                  >
                    <Dots />
                  </DropdownMenu>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
};


export default InviteListPage;
