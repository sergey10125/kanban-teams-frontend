export const FETCH_TEAM_ROLES = 'REQUEST/FETCH_TEAM_ROLES'
export const FETCH_TEAM_ROLES_SUCCESS = 'MEMBER/FETCH_TEAM_ROLES_SUCCESS'

export const FETCH_TEAM_MEMBER_LIST = 'REQUEST/FETCH_TEAM_MEMBER_LIST'
export const FETCH_TEAM_MEMBER_LIST_SUCCESS = 'MEMBER/FETCH_TEAM_MEMBER_LIST_SUCCESS'

export const FETCH_TEAM_MEMBER = 'REQUEST/FETCH_TEAM_MEMBER'
export const FETCH_TEAM_MEMBER_SUCCESS = 'MEMBER/FETCH_TEAM_MEMBER_SUCCESS'

export const CREATE_TEAM_MEMBER_NOTIFICATION = 'MEMBER/CREATE_TEAM_MEMBER_NOTIFICATION'

export const UPDATE_TEAM_MEMBER_ROLE = 'REQUEST/UPDATE_TEAM_MEMBER_ROLE'
export const UPDATE_TEAM_MEMBER_ROLE_SUCCESS = 'MEMBER/UPDATE_TEAM_MEMBER_ROLE_SUCCESS'

export const REMOVE_TEAM_MEMBER = 'REQUEST/REMOVE_TEAM_MEMBER'
export const REMOVE_TEAM_MEMBER_SUCCESS = 'TEAM/REMOVE_TEAM_MEMBER_SUCCESS'



export const FETCH_TEAM_INVITE_LIST = 'REQUEST/FETCH_TEAM_INVITE_LIST'
export const FETCH_TEAM_INVITE_LIST_SUCCESS = 'MEMBER/FETCH_TEAM_INVITE_LIST_SUCCESS'

export const FETCH_TEAM_INVITE = 'REQUEST/FETCH_TEAM_INVITE'
export const FETCH_TEAM_INVITE_SUCCESS = 'MEMBER/FETCH_TEAM_INVITE_SUCCESS'

export const CREATE_TEAM_INVITE = 'REQUEST/CREATE_TEAM_INVITE'
export const CREATE_TEAM_INVITE_SUCCESS = 'MEMBER/CREATE_TEAM_INVITE_SUCCESS'

export const UPDATE_TEAM_INVITE = 'REQUEST/UPDATE_TEAM_INVITE'
export const UPDATE_TEAM_INVITE_SUCCESS = 'MEMBER/UPDATE_TEAM_INVITE_SUCCESS'

export const REMOVE_TEAM_INVITE = 'REQUEST/REMOVE_TEAM_INVITE'
export const REMOVE_TEAM_INVITE_SUCCESS = 'MEMBER/REMOVE_TEAM_INVITE_SUCCESS'
