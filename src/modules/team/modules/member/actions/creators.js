import * as types from './types'

export const fetchTeamRoles = (teamId) => ({
  type: types.FETCH_TEAM_ROLES,
})
export const fetchTeamRolesSuccess = (payload, meta) => ({
  type: types.FETCH_TEAM_ROLES_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const fetchTeamMemberList = (teamId) => ({
  type: types.FETCH_TEAM_MEMBER_LIST,
  payload: {},
  meta: { teamId }
})
export const fetchTeamMemberListSuccess = (payload, meta) => ({
  type: types.FETCH_TEAM_MEMBER_LIST_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const fetchTeamMember = (teamId, memberId) => ({
  type: types.FETCH_TEAM_MEMBER,
  meta: { teamId, memberId }
})
export const fetchTeamMemberSuccess = (payload, meta) => ({
  type: types.FETCH_TEAM_MEMBER_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const createTeamMemberNotification = (payload, meta) => ({
  type: types.CREATE_TEAM_MEMBER_NOTIFICATION,
  payload: payload || {},
  meta: meta || {}
})

export const updateTeamMemberRole = (teamId, memberId, roleArray) => ({
  type: types.UPDATE_TEAM_MEMBER_ROLE,
  payload: { roleArray },
  meta: { teamId, memberId }
})
export const updateTeamMemberRoleSuccess = (payload, meta) => ({
  type: types.UPDATE_TEAM_MEMBER_ROLE_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})


export const removeTeamMember = (teamId, memberId) => ({
  type: types.REMOVE_TEAM_MEMBER,
  meta: { teamId, memberId }
})
export const removeTeamMemberSuccess = (payload, meta) => ({
  type: types.REMOVE_TEAM_MEMBER_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const fetchTeamInviteList = (teamId) => ({
  type: types.FETCH_TEAM_INVITE_LIST,
  meta: { teamId }
})
export const fetchTeamInviteListSuccess = (payload, meta) => ({
  type: types.FETCH_TEAM_INVITE_LIST_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const fetchTeamInvite = (teamId, token) => ({
  type: types.FETCH_TEAM_INVITE,
  payload: { token },
  meta: { teamId }
})
export const fetchTeamInviteSuccess = (payload, meta) => ({
  type: types.FETCH_TEAM_INVITE_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const createTeamInvite = (teamId, payload) => ({
  type: types.CREATE_TEAM_INVITE,
  payload,
  meta: { teamId }
})
export const createTeamInviteSuccess = (payload, meta) => ({
  type: types.CREATE_TEAM_INVITE_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const updateTeamInvite = (teamId, token, roleArray) => ({
  type: types.UPDATE_TEAM_INVITE,
  payload: { token, roleArray },
  meta: { teamId }
})
export const updateTeamInviteSuccess = (payload, meta) => ({
  type: types.UPDATE_TEAM_INVITE_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const removeTeamInvite = (teamId, token) => ({
  type: types.REMOVE_TEAM_INVITE,
  payload: { token },
  meta: { teamId }
})
export const removeTeamInviteSuccess = (payload, meta) => ({
  type: types.REMOVE_TEAM_INVITE_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
