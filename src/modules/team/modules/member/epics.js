import {
  mergeMap,
  catchError,
  takeUntil,
  map,
  take
} from 'rxjs/operators'
import { of, merge } from 'rxjs'
import { ofType } from 'redux-observable'
import { startSubmit } from 'redux-form'
import { LOCATION_CHANGE, push } from 'connected-react-router';

import { INIT_ERROR } from 'root/actions/types/system'
import { initError } from 'root/actions/creators/system'

import * as types from './actions/types'
import * as actions from './actions/creators'
import api from './api'

export const fetchTeamRole = action$ => action$.pipe(
  ofType(types.FETCH_TEAM_ROLES),
  mergeMap(action => api.fetchTeamRoles().pipe(
    map(payload => actions.fetchTeamRolesSuccess(payload)),
    takeUntil(action$.ofType(types.FETCH_TEAM_ROLES)),
    catchError(error => of(initError(error))),
  ))
)

export const fetchTeamMemberList = action$ => action$.pipe(
  ofType(types.FETCH_TEAM_MEMBER_LIST),
  mergeMap(({ payload, meta }) => api.fetchTeamMemberList(payload, meta).pipe(
    map(payload => actions.fetchTeamMemberListSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_TEAM_MEMBER_LIST)),
    catchError(error => of(initError(error))),
  ))
)

export const fetchTeamMember = action$ => action$.pipe(
  ofType(types.FETCH_TEAM_MEMBER),
  mergeMap(({ meta }) => api.fetchTeamMember(meta).pipe(
    map(payload => actions.fetchTeamMemberSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_TEAM_MEMBER)),
    catchError(error => of(initError(error))),
  ))
)

export const updateTeamMemberRole = action$ => action$.pipe(
  ofType(types.UPDATE_TEAM_MEMBER_ROLE),
  mergeMap(({ payload, meta }) => merge(
    of(startSubmit('updateMemberRolesForm')),
    api.updateTeamMemberRole(payload, meta).pipe(
      map((payload) => actions.updateTeamMemberRoleSuccess(payload, meta)),
      catchError((error) => of(initError(error, {
        formName: 'updateMemberRolesForm',
      })))
    ),
    action$.pipe(
      ofType(types.UPDATE_TEAM_MEMBER_ROLE_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${meta.teamId}/members`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  ))
)

export const removeTeamMember = action$ => action$.pipe(
  ofType(types.REMOVE_TEAM_MEMBER),
  mergeMap(({ payload, meta }) => api.removeTeamMember(payload, meta).pipe(
    map(() => actions.removeTeamMemberSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)

export const fetchTeamInviteList = action$ => action$.pipe(
  ofType(types.FETCH_TEAM_INVITE_LIST),
  mergeMap(({ meta }) => api.fetchTeamInviteList(meta).pipe(
    map(payload => actions.fetchTeamInviteListSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_TEAM_INVITE_LIST)),
    catchError(error => of(initError(error))),
  ))
)

export const fetchTeamInvite = action$ => action$.pipe(
  ofType(types.FETCH_TEAM_INVITE),
  mergeMap(({ payload, meta }) => api.fetchTeamInvite(payload, meta).pipe(
    map(payload => actions.fetchTeamInviteSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_TEAM_INVITE)),
    catchError(error => of(initError(error))),
  ))
)

export const createTeamInvite = action$ => action$.pipe(
  ofType(types.CREATE_TEAM_INVITE),
  mergeMap(({ payload, meta }) => merge(
    of(startSubmit('teamInviteForm')),
    api.createTeamInvite(payload, meta).pipe(
      map((payload) => actions.createTeamInviteSuccess(payload, meta)),
      catchError((error) => of(initError(error, {
        formName: 'teamInviteForm',
      })))
    ),
    action$.pipe(
      ofType(types.CREATE_TEAM_INVITE_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${meta.teamId}/members/invites`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  ))
)

export const updateTeamInvite = action$ => action$.pipe(
  ofType(types.UPDATE_TEAM_INVITE),
  mergeMap(({ payload, meta }) => merge(
    of(startSubmit('teamInviteForm')),
    api.updateTeamInvite(payload, meta).pipe(
      map((payload) => actions.updateTeamInviteSuccess(payload, meta)),
      catchError((error) => of(initError(error, {
        formName: 'teamInviteForm',
      })))
    ),
    action$.pipe(
      ofType(types.UPDATE_TEAM_INVITE_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${meta.teamId}/members/invites`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    ),
  ))
)

export const removeTeamInvite = action$ => action$.pipe(
  ofType(types.REMOVE_TEAM_INVITE),
  mergeMap(({ payload, meta }) =>
    api.removeTeamInvite(payload, meta).pipe(
      map(() => actions.removeTeamInviteSuccess(payload, meta)),
      catchError(error => of(initError(error))),
    )
  )
)
