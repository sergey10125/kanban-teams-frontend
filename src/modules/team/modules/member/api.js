import { from } from 'rxjs'
import request from 'utils/request'

export default {
  fetchTeamRoles: () => {
    const req = request('/api/team/services/roles')
      .then(response => response.json())

    return from(req)
  },
  fetchTeamMemberList: (payload, { teamId, ...meta }) => {
    const req = request(`/api/team/${teamId}/members`, 'GET', { meta })
      .then(response => response.json())

    return from(req)
  },
  fetchTeamMember: ({ teamId, memberId, ...meta }) => {
    const url = `/api/team/${teamId}/members/${memberId}`;
    const req = request(url, 'GET', { meta })
      .then(response => response.json());

    return from(req);
  },
  updateTeamMemberRole: (payload, { teamId, memberId, ...meta }) => {
    const url = `/api/team/${teamId}/members/${memberId}`;
    const req = request(url, 'PUT', { payload, meta })
      .then(response => response.json());

    return from(req)
  },
  removeTeamMember: (payload, { teamId, memberId, ...meta }) => {
    const req = request(`/api/team/${teamId}/members/${memberId}`, 'DELETE', { meta })
    return from(req)
  },

  fetchTeamInviteList: ({ teamId , ...meta }) => {
    const req = request(`/api/team/${teamId}/members/invite`, 'GET', { meta })
      .then(response => response.json())
    return from(req)
  },
  fetchTeamInvite: (payload, { teamId, ...meta }) => {
    const req = request(
      `/api/team/${teamId}/members/invite/fullinfo`, 'GET',
      { payload, meta }
    ).then(response => response.json())

    return from(req)
  },
  createTeamInvite: (payload, { teamId, ...meta }) => {
    const url = `/api/team/${teamId}/members/invite`;
    const req = request(url, 'POST', { payload, meta })
      .then(response => response.json())

    return from(req)
  },
  updateTeamInvite: (payload, { teamId, ...meta }) => {
    const url = `/api/team/${teamId}/members/invite`;
    const req = request(url, 'PUT', { payload, meta })
      .then(response => response.json());

    return from(req)
  },
  removeTeamInvite: (payload, { teamId, ...meta }) => {
    const req = request(`/api/team/${teamId}/members/invite`, 'DELETE', {
      payload,
      meta
    })
    return from(req)
  }
};
