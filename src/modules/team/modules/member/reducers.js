import { Map, fromJS } from 'immutable'

import { DISCONNECT_CONNECTION } from 'root/actions/types/socket';
import { LOGOUT_SUCCESS } from 'modules/auth/actions/types';
import { FETCH_TEAM } from 'modules/team/actions/types';

import * as types from './actions/types'

const mapStateInit = new Map({
  isLoaded: false,
  data: new Map()
})

const listStateInit = new Map({
  isLoaded: false,
  teamId: null,
  data: null
})

const initialState = new Map({
  teamRoles: mapStateInit,
  memberList: listStateInit,
  selectMember: mapStateInit,
  inviteList: listStateInit,
  selectInvite: mapStateInit
});


const fetchTeamRolesSuccess = (state, payload) => {
  return state.update('teamRoles', teamRolesState =>
    teamRolesState.merge({
      isLoaded: true,
      data: fromJS(payload)
    })
  )
}

const fetchTeamMemberList = (state, payload, meta) => {
  const { teamId } = meta;
  return state.set('memberList', listStateInit.set('teamId', teamId));
}

const fetchTeamMemberListSuccess = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('memberList', list => {
    if (list.get('teamId') !== teamId) return list;
    return list.merge({
      isLoaded: true,
      data: fromJS(payload)
    })
  })
}

const fetchTeamMember = (state, payload, meta) => {
  const { memberId } = meta;

  return state.set(
    'selectMember',
    mapStateInit.update('data', data => data.set('id', memberId))
  );
}
const fetchTeamMemberSuccess = (state, payload, meta) => {
  const { memberId } = meta;

  return state.update('selectMember', selectMember => {
    if (selectMember.getIn(['data', 'id']) !== memberId) return selectMember;
    return selectMember.merge({
      isLoaded: true,
      data: fromJS(payload)
    })
  });
}

const createTeamMember = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('memberList', list => {
    if (list.get('teamId') !== teamId || !list.get('isLoaded')) return list;
    return list.update('data', data => data.push(fromJS(payload)))
  })
}

const updateMemberRolesSuccess = (state, payload, meta) => {
  const { teamId, memberId } = meta

  return state
    .update('selectMember', selectMember => {
      if (selectMember.getIn(['data', 'id']) !== memberId) return selectMember;
      return selectMember.set('data', fromJS(payload));
    })
    .update('memberList', list => {
      if (!list.get('isLoaded') || list.get('teamId') !== teamId) return list;
      return list.update('data', data => {
        const index = data.findIndex(item => item.get('id') === memberId);
        if (index < 0) return data;

        const { title } = payload.roleArray.team;
        return data.update(index, item => item.set('roleTitle', title));
      })
    })
}

const removeTeamMember = (state, payload, meta) => {
  const { memberId } = meta;

  return state
    .update('selectMember', selectMember => {
      if (selectMember.getIn(['data', 'id']) !== memberId) return selectMember;
      return initialState;
    })
    .update('memberList', list => {
      if (!list.get('isLoaded')) return list;
      return list.update('data', data =>
        data.filter(member => member.get('id') !== memberId)
      )
    })
}

const fetchTeamInviteList = (state, payload, meta) => {
  const { teamId } = meta;
  return state.set('inviteList', listStateInit.set('teamId', teamId));
}

const fetchTeamInviteListSuccess = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('inviteList', list => {
    if (list.get('teamId') !== teamId) return list;
    return list.merge({
      isLoaded: true,
      data: fromJS(payload)
    })
  })
}


const fetchTeamInvite = (state, payload, meta) => {
  const { token } = payload;

  return state.set(
    'selectInvite',
    mapStateInit.update('data', data => data.set('token', token))
  )
}

const fetchTeamInviteSuccess = (state, payload, meta) => {
  const { token } = payload;

  return state.update('selectInvite', selectInvite => {
    if (selectInvite.getIn(['data', 'token']) !== token) return selectInvite;
    return selectInvite.merge({
      isLoaded: true,
      data: fromJS(payload)
    })
  });
}

const createTeamInvite = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('inviteList', list => {
    if (list.get('teamId') !== teamId || !list.get('isLoaded')) return list;
    return list.update('data', data => data.push(fromJS(payload)));
  })
}

const updateTeamInviteRoles = (state, payload, meta) => {
  const { token } = payload;

  return state.update('selectInvite', selectInvite => {
    if (selectInvite.getIn(['data', 'token']) !== token) return selectInvite;
    return selectInvite.set('data', fromJS(payload));
  })
}

const removeTeamInvite = (state, payload, meta) => {
  const { token } = payload;
  return state
    .update('selectInvite', selectInvite => {
      if (selectInvite.getIn(['data', 'token']) !== token) return selectInvite;
      return mapStateInit;
    })
    .update('inviteList', list => {
      if (!list.get('isLoaded')) return list;
      return list.update('data', data =>
        data.filter(invite => invite.get('token') !== token)
      )
    })
}


export default (state = initialState, action) => {
  switch (action.type) {

    case types.FETCH_TEAM_ROLES_SUCCESS:
      return fetchTeamRolesSuccess(state, action.payload, action.meta)

    case types.FETCH_TEAM_MEMBER_LIST:
      return fetchTeamMemberList(state, action.payload, action.meta)
    case types.FETCH_TEAM_MEMBER_LIST_SUCCESS:
      return fetchTeamMemberListSuccess(state, action.payload, action.meta)
    case types.FETCH_TEAM_MEMBER:
      return fetchTeamMember(state, action.payload, action.meta)
    case types.FETCH_TEAM_MEMBER_SUCCESS:
      return fetchTeamMemberSuccess(state, action.payload, action.meta)

    case types.CREATE_TEAM_MEMBER_NOTIFICATION:
      return createTeamMember(state, action.payload, action.meta)
    case types.UPDATE_TEAM_MEMBER_ROLE_SUCCESS:
      return updateMemberRolesSuccess(state, action.payload, action.meta)
    case types.REMOVE_TEAM_MEMBER_SUCCESS:
      return removeTeamMember(state, action.payload, action.meta)


    case types.FETCH_TEAM_INVITE_LIST:
      return fetchTeamInviteList(state, action.payload, action.meta)
    case types.FETCH_TEAM_INVITE_LIST_SUCCESS:
      return fetchTeamInviteListSuccess(state, action.payload, action.meta)
    case types.FETCH_TEAM_INVITE:
      return fetchTeamInvite(state, action.payload, action.meta)
    case types.FETCH_TEAM_INVITE_SUCCESS:
      return fetchTeamInviteSuccess(state, action.payload, action.meta)

    case types.CREATE_TEAM_INVITE_SUCCESS:
      return createTeamInvite(state, action.payload, action.meta)
    case types.UPDATE_TEAM_INVITE_SUCCESS:
      return updateTeamInviteRoles(state, action.payload, action.meta)
    case types.REMOVE_TEAM_INVITE_SUCCESS:
      return removeTeamInvite(state, action.payload, action.meta)

    case DISCONNECT_CONNECTION:
    case LOGOUT_SUCCESS:
    case FETCH_TEAM:
      return initialState;

    default:
      return state
  }
}
