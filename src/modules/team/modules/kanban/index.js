import * as actionTypes from './actions/types';
import * as actionCreators from './actions/creators';
import epics from './epics';
import reducers from './reducers';
import routes from './routes';
import socketActions from './socketActions';


export default {
  actionTypes,
  actionCreators,
  epics,
  reducers: { kanban: reducers },
  routes,
  socketActions
};
