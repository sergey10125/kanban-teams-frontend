import { Map, fromJS } from 'immutable';

import { LOGOUT_SUCCESS } from 'modules/auth/actions/types';
import {
  JOIN_SOCKET_ROOM_SUCCESS,
  DISCONNECT_CONNECTION
} from 'root/actions/types/socket';


import { FETCH_TEAM } from 'modules/team/actions/types';

import * as types from './actions/types';

import squeezeCardsList from './helpers/state/squeezeCardsList';
import changeCardPosition from './helpers/state/changeCardPosition';
import createCardFromZero from './helpers/state/createCardFromZero';
import replaceCapToCard from './helpers/state/replaceCapToCard';
import createCardCap from './helpers/state/createCardCap';

const listInit = new Map({
  teamId: null,
  isLoaded: false,
  data: null
});

const selectDeskInit = new Map({
  data: new Map({}),
  isLoaded: false,
  connectedToken: null,
  isSocketConnected: false,
});

const selectCardInit = new Map({
  isLoaded: false,
  data: new Map({})
});

const initialState = new Map({
  list: listInit,
  selectDesk: selectDeskInit,
  selectCard: selectCardInit
});



//-----------------------------------------------------------------------------
// Kanban desk state update functions
//-----------------------------------------------------------------------------


const fetchKanbanDeskList =  (state, payload, meta) => {
  const { teamId } = meta;
  return state.set('list', listInit.set('teamId', teamId));
};

const fetchKanbanDeskListSuccess = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('list', list => {
    if (list.get('teamId') !== teamId) return list;
    return list.merge({
      isLoaded: true,
      data: fromJS(payload)
    });
  });
};

const fetchKanbanDesk =  (state, payload, meta) => {
  const { deskId } = meta;

  return state.update('selectDesk', desk =>
    selectDeskInit.update('data', data => data.set('id', deskId))
  );
};

const fetchKanbanDeskSuccess = (state, payload, meta) => {
  const { desk, invite } = payload;
  const { deskId } = meta;

  return state.update('selectDesk', deskState => {
    if (deskState.getIn(['data', 'id']) !== deskId) return deskState;
    return deskState.merge({
      data: fromJS(desk),
      connectedToken: invite,
      isLoaded: true,
    });
  });
};


const kanbanDeskSokcetConnected = (state, payload) => {
  if (payload.roomType !== 'kanban') return state;
  return state.update('selectDesk', team => {
    if (team.getIn(['data', 'id']) !== payload.id) return team;
    return team.set('isSocketConnected', true)
  });
};

const createKanbanDesk = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('list', list => {
    if (list.get('teamId') !== teamId || !list.get('isLoaded')) return list;
    return list.update('data', data => data.unshift(fromJS(payload)));
  });
};


const updateDeskData = (state, payload, meta) => {
  const { deskId } = meta;
  const { title } = payload;

  return state
    .update('list', list => {
      if (!list.get('isLoaded')) return list;
      return list.update('data', data => {
        const index = data.findIndex(desk => desk.get('id') === deskId);
        if (index < 0) return data;
        return data.update(index, desk => desk.set('title', title));
      });
    })
    .update('selectDesk', desk => {
      if (!desk.get('isLoaded')) return desk;
      return desk.update('data', data => {
        if (data.get('id') !== deskId) return data;
        return data.set('title', title);
      });
    });
};

const removeKanbanDesk = (state, payload, meta) => {
  const { deskId } = meta;
  return state
    .update('list', list => {
      return list.update('data', data => {
        return data.filter(item => item.get('id') !== deskId);
      });
    })
    .update('selectDesk', desk => {
      if (!desk.get('isLoaded')) return desk;
      return desk.update('data', data => {
        if (data.get('id') !== deskId) return data;
        return selectDeskInit;
      });
    });
};

//-----------------------------------------------------------------------------
// Kanban list state update functions
//-----------------------------------------------------------------------------

const createList = (state, payload, meta) => {
  const { deskId } = meta;

  return state.update('selectDesk', desk => {
    if (!desk.get('isLoaded')) return desk;
    return desk.update('data', data => {
      if (data.get('id') !== deskId) return data;
      return data.update('lists', lists => lists.push(fromJS({
        ...payload,
        cards: []
      })));
    });
  });
};

const updateListTitle = (state, payload, meta) => {
  const { deskId, listId } = meta;
  const { title } = payload;

  return state.update('selectDesk', desk => {
    if (!desk.get('isLoaded')) return desk;
    return desk.update('data', data => {
      if (data.get('id') !== deskId) return data;
      return data.update('lists', lists => {
        const index = lists.findIndex(item => item.get('id') === listId);
        if (index < 0) return lists;
        return lists.update(index, item => item.set('title', title));
      });
    });
  });
};

const changeListPostition = (state, payload, meta) => {
  const { deskId, listId } = meta;
  const { position } = payload;

  return state.update('selectDesk', desk => {
    if (!desk.get('isLoaded')) return desk;
    return desk.update('data', data => {
      if (data.get('id') !== deskId) return data;
      return data.update('lists', lists => {
        const itemIndex = lists.findIndex(item => item.get('id') === listId);
        if (itemIndex < 0) return lists;

        const item = lists.get(itemIndex);
        const newPostiton = (itemIndex <= position)? position - 1 : position;
        return lists.delete(itemIndex).insert(newPostiton, item);
      });
    });
  });
};

const updateListWidth = (state, payload, meta) => {
  const { deskId, listId } = meta;
  const { width } = payload;

  return state.update('selectDesk', desk => {
    if (!desk.get('isLoaded')) return desk;
    return desk.update('data', data => {
      if (data.get('id') !== deskId) return data;
      return data.update('lists', lists => {
        const itemIndex = lists.findIndex(item => item.get('id') === listId);
        if (itemIndex < 0) return lists;
        return lists.update(itemIndex, item => item.merge({
          cards: squeezeCardsList(item.get('cards'), width),
          width
        }));
      });
    });
  });
};

const removeList = (state, payload, meta) => {
  const { deskId, listId } = meta;

  return state
    .update('selectDesk', desk => {
      if (!desk.get('isLoaded')) return desk;
      return desk.update('data', data => {
        if (data.get('id') !== deskId) return data;
        return data.update('lists', lists => {
          return lists.filter(item => item.get('id') !== listId);
        });
      });
    })
    .update('selectCard', card => {
      if (!card.get('isLoaded') || card.getIn(['data', 'listId']) !== listId) {
        return card;
      };
      return selectCardInit;
    });
};

//-----------------------------------------------------------------------------
// Kanban card state update functions
//-----------------------------------------------------------------------------

const fetchCardLoading =  (state, payload, { cardId }) => {
  return state.update('selectCard', desk =>
    selectCardInit.update('data', data => data.set('id', cardId))
  );
};

const fetchCardSuccess = (state, payload, { cardId }) => {
  return state.update('selectCard', desk => {
    return desk.set('isLoaded', true).update('data', data => {
      if (data.get('id') !== cardId) return data;
      return fromJS(payload);
    });
  });
};

const createCard = (state, payload, meta) => {
  const { deskId, listId } = meta;
  const { title, position } = payload;

  return state.update('selectDesk', desk => {
    if (!desk.get('isLoaded')) return desk;
    return desk.update('data', data => {
      if (data.get('id') !== deskId) return data;
      return createCardCap(data,
        { id: payload.token, title, },
        { listId, x: position.x, y: position.y },
      );
    });
  });
};

const createCardSuccess = (state, payload, meta) => {
  const { deskId } = meta;

  return state.update('selectDesk', desk => {
    if (!desk.get('isLoaded')) return desk;
    return desk.update('data', data => {
      if (data.get('id') !== deskId) return data;
      return replaceCapToCard(data, payload.token, fromJS(payload.card));
    });
  });
};

const createCardNotification = (state, payload, meta) => {
  const { deskId, listId } = meta;
  const { positionX, positionY } = payload;

  return state.update('selectDesk', desk => {
    if (!desk.get('isLoaded')) return desk;
    return desk.update('data', data => {
      if (data.get('id') !== deskId) return data;
      return createCardFromZero(data, fromJS(payload), {
        listId, x: positionX, y: positionY
      });
    });
  });
};

const updateCardData = (state, payload, meta) => {
  const { deskId, cardId } = meta;

  return state
    .update('selectCard', desk => {
      return desk.update('data', data => {
        if (data.get('id') !== cardId) return data;
        return data.merge(payload);
      })
    })
    .update('selectDesk', deskState => {
      if (!deskState.get('isLoaded')) return deskState;
      return deskState.update('data', data => {
        if (data.get('id') !== deskId) return data;
        return data.update('lists', lists => {
          let listInex, cardIndex;
          lists.forEach((list, i) => {
            if (listInex) return;
            list.get('cards').forEach((card, j) => {
              if (card.get('id') === cardId) {
                listInex = i;
                cardIndex = j;
              };
            });
          });

          if (listInex < 0) return lists;
          return lists.update(listInex, list => {
            return list.update('cards', cards => {
              return cards.update(cardIndex, card => card.merge(payload));
            })
          });
        });
      });
    });
}

const updateCardPosition = (state, payload, meta) => {
  const { deskId, listId, cardId } = meta;
  const { position } = payload;

  return state.update('selectDesk', desk => {
    if (!desk.get('isLoaded')) return desk;
    return desk.update('data', data => {
      if (data.get('id') !== deskId) return data;
      return changeCardPosition(data, cardId, listId, position);
    });
  });
};

const removeCard = (state, payload, meta) => {
  const { deskId, cardId } = meta;

  return state
    .update('selectDesk', deskState => {
      if (!deskState.get('isLoaded')) return deskState;
      return deskState.update('data', data => {
        if (data.get('id') !== deskId) return data;
        return data.update('lists', lists => {
          return lists.map(list => {
            return list.update('cards', cards => {
              return cards.filter(item => item.get('id') !== cardId);
            });
          });
        });
      });
    })
    .update('selectCard', card => {
      if (!card.get('isLoaded') || card.getIn(['data', 'id']) !== cardId) {
        return card;
      };
      return selectCardInit;
    });
};



export default (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_TEAM_KANBAN_LIST:
      return fetchKanbanDeskList(state, action.payload, action.meta);
    case types.FETCH_TEAM_KANBAN_LIST_SUCCESS:
      return fetchKanbanDeskListSuccess(state, action.payload, action.meta);

    case types.FETCH_KANBAN_DESK:
      return fetchKanbanDesk(state, action.payload, action.meta);
    case types.FETCH_KANBAN_DESK_SUCCESS:
      return fetchKanbanDeskSuccess(state, action.payload, action.meta);
    case JOIN_SOCKET_ROOM_SUCCESS:
      return kanbanDeskSokcetConnected(state, action.payload, action.meta);
    case types.CREATE_KANBAN_DESK_SUCCESS:
      return createKanbanDesk(state, action.payload, action.meta);
    case types.UPDATE_KANBAN_DESK_DATA_SUCCESS:
      return updateDeskData(state, action.payload, action.meta);
    case types.REMOVE_KANBAN_DESK_SUCCESS:
      return removeKanbanDesk(state, action.payload, action.meta);

    case types.CREATE_KANBAN_LIST_SUCCESS:
      return createList(state, action.payload, action.meta);
    case types.UPDATE_KANBAN_LIST_SUCCESS:
      return updateListTitle(state, action.payload, action.meta);
    case types.UPDATE_KANBAN_LIST_WIDTH_SUCCESS:
    case types.UPDATE_KANBAN_LIST_WIDTH_NOTIFICATION:
      return updateListWidth(state, action.payload, action.meta);
    case types.UPDATE_KANBAN_LIST_POSITION:
    case types.UPDATE_KANBAN_LIST_POSITION_NOTIFICATION:
      return changeListPostition(state, action.payload, action.meta);
    case types.REMOVE_KANBAN_LIST_SUCCESS:
      return removeList(state, action.payload, action.meta);

    case types.FETCH_KANBAN_CARD:
      return fetchCardLoading(state, action.payload, action.meta);
    case types.FETCH_KANBAN_CARD_SUCCESS:
      return fetchCardSuccess(state, action.payload, action.meta);
    case types.CREATE_KANBAN_CARD:
      return createCard(state, action.payload, action.meta);
    case types.CREATE_KANBAN_CARD_SUCCESS:
      return createCardSuccess(state, action.payload, action.meta);
    case types.CREATE_KANBAN_CARD_NOTIFICATION:
      return createCardNotification(state, action.payload, action.meta);
    case types.UPDATE_KANBAN_CARD_POSITION:
    case types.UPDATE_KANBAN_CARD_POSITION_NOTIFICATION:
      return updateCardPosition(state, action.payload, action.meta);
    case types.UPDATE_KANBAN_CARD_SUCCESS:
      return updateCardData(state, action.payload, action.meta);
    case types.REMOVE_KANBAN_CARD_SUCCESS:
      return removeCard(state, action.payload, action.meta);

    case DISCONNECT_CONNECTION:
    case LOGOUT_SUCCESS:
    case FETCH_TEAM:
      return initialState;
    default:
      return state;
  }
}
