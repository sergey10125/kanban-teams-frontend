import React, { useRef, useCallback } from 'react';
import classNames from 'classnames';


const DragScrollContainer = ({ children, className, accessDragClassName }) => {
  const lastClickPosition = useRef({});
  const containerDOM = useRef(null);
  const isDragging = useRef(false);

  const isAccessClassName = useCallback((dragingElClass) => {
    if (!Array.isArray(accessDragClassName)) {
      return accessDragClassName === dragingElClass
    };

    return accessDragClassName.some(item => item === dragingElClass)
  }, [ accessDragClassName ]);


  const mouseUpHandle = useCallback((e) => {
    if (isDragging.current && e.button === 0) {
      isDragging.current = false;
    }
  }, []);

  const mouseDownHandle = useCallback((e) => {
    if (isAccessClassName(e.target.className) && e.button === 0) {
      isDragging.current = true;
      lastClickPosition.current.x = e.clientX
      lastClickPosition.current.y = e.clientY
    }
  }, [ isAccessClassName ]);

  const mouseMoveHandle = useCallback((e) => {
    if (isDragging.current) {
      const { x, y } = lastClickPosition.current;

      containerDOM.current.scrollLeft -= e.clientX - x;
      lastClickPosition.current.x = e.clientX;

      containerDOM.current.scrollTop -= e.clientY - y;
      lastClickPosition.current.y = e.clientY;
    }
  }, []);

  return (
    <div
      className={classNames('drag-scroll-conteiner', className)}
      ref={containerDOM}
      onMouseDown={mouseDownHandle}
      onMouseMove={mouseMoveHandle}
      onMouseUp={mouseUpHandle}
      onMouseLeave={mouseUpHandle}
    >
      {children}
    </div>
  )
};


export default DragScrollContainer;
