import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";

import hasTeamPermissionInit from 'utils/hasTeamPermissionInit'
import Loader from 'components/Loader'

import actions from '../actions/creators'


const KanbanListWidget = () => {
  const dispatch = useDispatch();

  const listState = useSelector(({ kanban }) => kanban.get('list'));
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');

  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );


  useEffect(() => {
    if (listState.get('teamId') !== teamId) {
      dispatch(actions.fetchTeamKanbanDeskList(teamId));
    };
  });


  return (
    <div className='widget-container'>
      <div className='widget'>
        <div className='widget__headline'>
          <Link to={`/${teamId}/kanbans`} className='widget__link'>
            Kanbans
          </Link>
          {hasTeamPermission('createDesk') &&
            <Link to={`/${teamId}/kanbans/new`} className='widget__create'>
              +
            </Link>
          }
        </div>
        <div className='widget__content'>
          {!listState.get('isLoaded') && <Loader />}
          {listState.get('isLoaded') &&
            listState.get('data').take(4).map(item =>
              <div className='widget-item' key={item.get('id')}>
                <Link
                  className='widget-item__title link'
                  to={`/${teamId}/kanbans/${item.get('id')}`}
                >
                  {item.get('title')}
                </Link>
              </div>
            )
          }
      </div>
      </div>
    </div>
  )
}


export default KanbanListWidget;
