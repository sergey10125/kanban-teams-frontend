import React from 'react'
import { useSelector } from "react-redux";

import KanbanForm from './KanbanForm'

const EditKanbanForm = () => {
  const descData = useSelector(({ kanban }) =>
    kanban.getIn(['selectDesk', 'data'])
  );

  const initialValues = {
    title: descData.get('title')
  }

  return (
    <KanbanForm
      deskId={descData.get('id')}
      initialValues={initialValues}
    />
  )
}

export default EditKanbanForm;
