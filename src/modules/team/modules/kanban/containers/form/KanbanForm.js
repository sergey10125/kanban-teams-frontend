import React, { useCallback } from 'react';
import { withRouter } from 'react-router';
import { Field, reduxForm } from 'redux-form';
import { useDispatch, useSelector } from "react-redux";

import actions  from '../../actions/creators';

import Input from 'components/form/redux-form-adapter/Input';
import ErrorMessage from 'components/form/ErrorMessage';

import * as validations from 'utils/validations';
const minLength3 = validations.minLength(3);
const maxLength30 = validations.maxLength(30);


const KanbanForm = ({ deskId, handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();

  const teamId = useSelector(({ team }) =>
    team.getIn(['selectTeam', 'data', 'id'])
  );

  const heandle = useCallback(({ title }) => {
    const payload = { title };
    if (!deskId) {
      dispatch(actions.createKanbanDesk(teamId, payload));
    } else {
      dispatch(actions.updateKanbanDeskData(teamId, deskId, payload));
    }
  }, [ teamId, deskId, dispatch ]);

    return (
      <div className='page-form'>
        <div className="page-form__headline">Create a new Kanban</div>
        <form className="form" onSubmit={handleSubmit(heandle)}>
          <fieldset className="form-fieldset">
            <Field
              component={Input}
              name="title"
              type="text"
              label="Kanban title"
              validate={[
                validations.isRequired,
                minLength3,
                maxLength30,
              ]}
              disabled={submitting}
            />
            <ErrorMessage error={error}/>
          </fieldset>
          <button disabled={submitting} className="btn btn--w200">
            Create
          </button>
        </form>
      </div>
    )
}

export default withRouter(
  reduxForm({ form: 'kanbanForm' })(KanbanForm)
);
