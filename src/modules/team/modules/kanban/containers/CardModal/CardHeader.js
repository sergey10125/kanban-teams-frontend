import React, { useState, useEffect, useRef, useCallback } from 'react'
import TextareaAutosize  from 'react-textarea-autosize'

import usePrevious from 'utils/hooks/usePrevious';

const DeskCardHeader = ({ title: initTitle, changeAction, hasPermission }) => {
  const [ value, setValue ] = useState(initTitle || '');
  const prevInitTitle = usePrevious(initTitle);
  const inputEl = useRef(null);

  const canEditCard = hasPermission('editCard');


  const changeTitle = useCallback((title) => {
    if (!title.length) {
      setValue(initTitle);
    } else if (title !== initTitle) {
      changeAction(title);
    };
  }, [ initTitle, changeAction ]);

  const onEnterPress = useRef((e) => {
    if (e.key === 'Enter') {
      inputEl.current.blur();
    };
  }).current;


  useEffect(() => {
    if (prevInitTitle !== initTitle) {
      setValue(initTitle);
    };
  }, [ prevInitTitle, initTitle, setValue ]);


  return (
    <div className='card-modal__headline'>
      <TextareaAutosize
        className='card-modal__headline-input'
        value={value}
        inputRef={inputEl}
        onChange={(e) => setValue(e.target.value)}
        onBlur={() => changeTitle(value)}
        onKeyPress={onEnterPress}
        disabled={!canEditCard}
        maxLength={100}
      />
    </div>
  )
}

export default DeskCardHeader;
