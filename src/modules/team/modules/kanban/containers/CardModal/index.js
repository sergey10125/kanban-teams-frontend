import React, { Fragment, useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { push } from 'connected-react-router';
import { IoIosClose } from "react-icons/io";

import hasTeamPermissionInit from 'utils/hasTeamPermissionInit';
import NotFoundPage from 'components/NotFoundPage';
import Loader from 'components/Loader';

import actions from '../../actions/creators';

import DeskCardHeader from './CardHeader';
import DeskCardSidebar from './CardSidebar';
import DeskCardDescription from './CardDescription';

const defaultDeskEditState = {
  isEdit: false,
  value: ''
}

const DeskCardModal = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const [ descEditState, setDescEditState ] = useState(defaultDeskEditState);

  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');
  const deskId = useSelector(({ kanban }) => {
    return kanban.getIn(['selectDesk', 'data', 'id']);
  });

  const hasPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );

  const notFoundState = useSelector(({ system }) => system.get('notFoundState'));
  const cardState = useSelector(({ kanban }) => kanban.get('selectCard'));
  const card = cardState.get('data');
  const paramCardId = +params.cardId;

  const closeEditDeskForm = () => setDescEditState(defaultDeskEditState);
  const updateEditDescValue = (value) => setDescEditState({
    ...descEditState, value
  })
  const openEditDeskForm = () => setDescEditState({
    isEdit: true,
    value: card.get('description')
  })


  const updateTitle = useCallback((title) => {
    const description = card.get('description');
    const payload = { title, description };
    dispatch(
      actions.updateKanbanCard(teamId, deskId, card.get('id'), payload)
    );
  }, [ teamId, deskId, card, dispatch ]);

  const saveUpdateDeskValue = () => {
    const description = descEditState.value || null;
    const title = card.get('title');
    const payload = { title, description };
    dispatch(
      actions.updateKanbanCard(teamId, deskId, card.get('id'), payload)
    );
    closeEditDeskForm();
  };

  const closeModal = useCallback(() => {
    dispatch(push(`/${teamId}/kanbans/${deskId}`));
  }, [ teamId, deskId, dispatch ]);

  const deleteCard = useCallback(() => {
    dispatch(actions.removeKanbanCard(teamId, deskId, paramCardId));
  }, [ teamId, deskId, paramCardId, dispatch ]);


  useEffect(() => {
    if (card.get('id') !== paramCardId) {
      dispatch(actions.fetchCard(teamId, deskId, paramCardId));
    };
  });


  const canEditCard = hasPermission('editCard');
  const canRemoveCard = hasPermission('removeCard');

  const isNotFound = (
    notFoundState.get('status') &&
    notFoundState.get('location') === 'kanbanCardModal'
  );

  return (
    <Fragment>
      <div className='overlay' onClick={closeModal}/>
      <div className="modal card-modal">
        <IoIosClose onClick={closeModal} className='modal__closer'/>
        {isNotFound ?
          <NotFoundPage />
        :
          <Fragment>
            {!cardState.get('isLoaded') && <Loader />}
            {cardState.get('isLoaded') &&
              <Fragment>
                <DeskCardHeader
                  title={card.get('title')}
                  changeAction={updateTitle}
                  hasPermission={hasPermission}
                />
                <div className='card-modal__content'>
                  <div className='card-modal-body'>
                    <div className='card-modal-body__field'>Description:</div>
                    <DeskCardDescription
                      isEditDesc={descEditState.isEdit}
                      descEditValue={descEditState.value}
                      description={card.get('description')}
                      setDescValue={updateEditDescValue}
                    />
                  </div>
                  <DeskCardSidebar
                    isEditDesc={descEditState.isEdit}
                    canEditCard={canEditCard}
                    canRemoveCard={canRemoveCard}
                    openEditDeskForm={openEditDeskForm}
                    closeEditDeskForm={closeEditDeskForm}
                    saveUpdateDeskValue={saveUpdateDeskValue}
                    removeCard={deleteCard}
                  />
                </div>
              </Fragment>
            }
          </Fragment>
        }
      </div>
    </Fragment>
  )
}


export default DeskCardModal;
