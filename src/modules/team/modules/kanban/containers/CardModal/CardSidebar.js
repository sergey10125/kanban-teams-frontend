import React, { useState } from 'react'


const DeskCardModalSidebar = ({
  isEditDesc,
  canEditCard,
  canRemoveCard,
  openEditDeskForm,
  closeEditDeskForm,
  saveUpdateDeskValue,
  removeCard
}) => {
  const [ isRemoving, setIsRemoving ] = useState(false);

  if (isRemoving) {
    return (
      <div className='card-modal-sidebar'>
        <button className='btn btn--small btn--red' onClick={removeCard} >
          Подтвердить
        </button>
        <button
          className='btn btn--small btn--gray'
          onClick={() => setIsRemoving(false)}
        >
          Отмена
        </button>
      </div>
    );
  } else if (isEditDesc) {
    return (
      <div className='card-modal-sidebar'>
        <button className='btn btn--small' onClick={saveUpdateDeskValue} >
          Сохранить
        </button>
        <button
          className='btn btn--small btn--gray'
          onClick={closeEditDeskForm}
        >
          Отмена
        </button>
      </div>
    )
  } else {
    return (
      <div className='card-modal-sidebar'>
        {canRemoveCard && (
          <button
            className='btn btn--small'
            onClick={openEditDeskForm}
          >
            Редактировать описание
          </button>
        )}
        {canRemoveCard && (
          <button
            className='btn btn--small btn--red'
            onClick={() => setIsRemoving(true)}
          >
            Удалить
          </button>
        )}
      </div>
    )
  }
}


export default DeskCardModalSidebar
