import React, { useEffect, useRef } from 'react';
import TextareaAutosize  from 'react-textarea-autosize'

const DeskCardDescription = ({
  isEditDesc,
  description,
  descEditValue,
  setDescValue
}) => {
  const inputDOM = useRef();

  useEffect(() => {
    if (isEditDesc) {
      inputDOM.current.focus();
    };
  }, [ isEditDesc ]);

  return (
    <div className='card-modal-body__field' >
      {isEditDesc ?
        <TextareaAutosize
          className='card-modal-body__description'
          inputRef={inputDOM}
          value={descEditValue}
          onChange={(e) => setDescValue(e.target.value)}
        />
      :
        !!description && description.split('\n').map((item, i) =>
          <div key={i}>{item}</div>
        )
      }
    </div>
  );
}
export default DeskCardDescription;
