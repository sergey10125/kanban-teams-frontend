import React, { useState, useMemo, useCallback } from 'react';
import { getEmptyImage } from 'react-dnd-html5-backend';
import { useDrag } from 'react-dnd';
import classNames from 'classnames';

import * as dragTypes from '../../constants/dragTypes';

const CreateTask = ({ dragDeskState }) => {
  const [isActive, setActive] = useState(false);
  const [title, setTitle] = useState('');

  const canDrag = isActive && !!title.length;

  const [{ isDragging }, dragRef, preview] = useDrag({
    item: { type: dragTypes.NEW_TASK, title },
    canDrag: canDrag,
    end: (item, monitor) => {
      dragDeskState.clear();
      if (monitor.didDrop()) {
        setActive(false);
        setTitle('');
      }
    },
    collect: monitor => ({
      isDragging: monitor.isDragging()
    }),
  });


  const changeTitle  = useCallback((e) => {
    const title  = e.target.value;
    setTitle(title)
  }, []);

  const onEnterPress = useCallback((e) => {
    if(e.keyCode !== 13) return;
    e.preventDefault();
  }, []);


  preview(getEmptyImage());

  const className = useMemo(() => classNames('create-task', {
    'create-task--hide': isDragging,
    'create-task--active': isActive
  }), [ isDragging, isActive ]);
  
  return (
    <div className={className}>
      <div
        className='create-task__headline'
        ref={dragRef}
        onClick={() => setActive(!isActive)}
      >
        <span>
          {canDrag
            ? 'Drag new card'
            : isActive
              ? 'Write content'
              : 'Create new card'
          }
        </span>
      </div>
      {isActive &&
        <textarea
          name='title'
          className='textarea textarea--no-resize'
          maxLength={100}
          value={title}
          onKeyDown={onEnterPress}
          onChange={changeTitle}
        />
      }
    </div>
  )
}

export default CreateTask
