import React, { useState, useEffect, useCallback } from 'react';
import classNames from 'classnames';

import usePrevious from 'utils/hooks/usePrevious';
import { openModal } from 'root/actions/creators/system';

import * as sizeType from './../../../constants/size';
import actions from './../../../actions/creators';

import DeskListHeader from './DeskListHeader';
import DeksListContent from './DeksListContent';
import DeskListResizeBorder from './DeskListResizeBorder';

const DeskList = ({ data, listDrag, isDragging, ...props }) => {
  const { teamId, deskId, dispatch, hasPermission } = props;
  const listId = data.get('id');

  const listWidth = data.get('width');
  const prevListWidth = usePrevious(listWidth);
  const [ stateWidth, setStateWidth ] = useState(listWidth);

  const updateListData = useCallback((payload) => {
    dispatch(
      actions.updateKanbanList(teamId, deskId, listId, payload)
    );
  }, [ teamId, deskId, listId, dispatch ])

  const updateListWidth = useCallback((width) => {
    const payload = { width };
    dispatch(
      actions.updateKanbanListWidth(teamId, deskId, listId, payload)
    );
  }, [ teamId, deskId, listId, dispatch ])

  const callDeleteListModal = useCallback(() => {
    dispatch(openModal('ConfirmModal', {
      message: 'Are you sure you want to remove a kanban desk list ?',
      actionFun: () => {
        dispatch(actions.removeKanbanList(teamId, deskId, listId));
      }
    }));
  }, [ teamId, deskId, listId, dispatch ])


  useEffect(() => {
    if (prevListWidth !== listWidth) {
      setStateWidth(listWidth)
    }
  }, [ listWidth, prevListWidth ]);



  const className = classNames('desk-list', {
    'desk-list--hide': isDragging
  });
  const style = {};
  style.width = (sizeType.DESK_CARD_WIDTH * stateWidth) + 20;

  return (
    <div className={className} style={style} >
      <DeskListHeader
        data={data}
        listDrag={listDrag}
        hasPermission={hasPermission}
        updateListData={updateListData}
        callDeleteListModal={callDeleteListModal}
      />
      <DeksListContent
        {...props}
        listId={data.get('id')}
        cards={data.get('cards')}
        isWidthSqueezed={listWidth > stateWidth}
        width={stateWidth}
      />
      <DeskListResizeBorder
        data={data}
        width={stateWidth}
        setStateWidth={setStateWidth}
        updateListWidth={updateListWidth}
        hasPermission={hasPermission}
      />
    </div>
  )
}

export default DeskList;
