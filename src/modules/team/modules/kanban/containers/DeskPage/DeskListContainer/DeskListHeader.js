import React, { Fragment, useState, useEffect, useCallback, useRef } from 'react';
import classNames from 'classnames';
import { MdDone } from 'react-icons/md';

import Dots from 'components/Dots';
import usePrevious from 'utils/hooks/usePrevious';
import DropdownMenu from 'components/DropdownMenu';

const DeskListHeader = (props) => {
  const { data, updateListData, callDeleteListModal } = props;

  const [ isEdit, setEditStatus ] = useState(false);
  const [ title, setTitle ] = useState(data.get('title'));
  const prevTitle = usePrevious(data.get('title'));

  const canEditList = props.hasPermission('editList');
  const canRemoveList = props.hasPermission('removeList');

  const headerDOM = useRef(null);
  const inputDOM = useRef(null);


  const outsideClick = useRef(function outsideClick(e) {
    if (!!headerDOM.current && !headerDOM.current.contains(e.target)) {
      document.removeEventListener('mousedown', outsideClick)
      setEditStatus(false);
    }
  }).current;

  const startEditTitle = useCallback(() => {
    if (!canEditList) return;
    document.addEventListener('mousedown', outsideClick);
    setEditStatus(true);
  }, [ canEditList, outsideClick ]);

  const editTitle = useCallback(() => {
    updateListData({ title });
    setEditStatus(false);
  }, [ title, updateListData ]);

  const onEnterPress = useCallback((e) => {
    if (e.key !== 'Enter') return;
    editTitle(title);
  }, [ title, editTitle ]);


  useEffect(() => {
    if (prevTitle !== data.get('title')) {
      setTitle(data.get('title'));
    };
  }, [ prevTitle, data, setTitle ]);

  useEffect(() => {
    if (!isEdit) return;
    inputDOM.current.focus();
  }, [ isEdit ]);


  const menuArr = [];
  if (canEditList) {
    menuArr.push({ title: 'Edit', action: startEditTitle });
  };
  if (canRemoveList) {
    menuArr.push({ title: 'Delete', action: callDeleteListModal });
  };

  const dragClassName = classNames('desk-list__drag', {
    'desk-list__drag--disable': !canEditList
  });

  return (
    <div className='desk-list__header' ref={headerDOM}>
      {isEdit?
        <Fragment>
          <input
            name='title'
            className='desk-list__input'
            maxLength={80}
            ref={inputDOM}
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            onKeyPress={onEnterPress}
          />
          <MdDone className='desk-list__accept' onClick={editTitle} />
        </Fragment>
      :
        <Fragment>
          <div
            className={dragClassName}
            ref={props.listDrag}
          />
          <div
            onDoubleClick={startEditTitle}
            className='desk-list__title'
            title={title}
          >
            {title}
          </div>
          {!!menuArr.length && (
            <DropdownMenu className='desk-list__menu' list={menuArr}>
              <Dots color='white' size='small'/>
            </DropdownMenu>
          )}
        </Fragment>
      }
    </div>
  )
}

export default DeskListHeader;
