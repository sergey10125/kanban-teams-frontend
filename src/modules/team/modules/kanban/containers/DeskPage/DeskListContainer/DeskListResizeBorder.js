import React, { useRef } from 'react';
import classNames from 'classnames';

import * as sizeType from './../../../constants/size';

const DeskListResizeBorder = (props) => {
  const lastClientX = useRef(null);
  const propsRef = useRef(null)
  propsRef.current = props;

  const canEditList = props.hasPermission('editList');

  const headlers = {
    onMouseMove(e) {
      const { width, setStateWidth } = propsRef.current;
      const differenceX = e.clientX - lastClientX.current;
      const additionalWidth = Math.round(differenceX/sizeType.DESK_CARD_WIDTH);
      let newWidth = props.width + additionalWidth;

      if (newWidth <= 0) newWidth = 1;
      if (newWidth >= 5) newWidth = 5;

      if (width !== newWidth) {
        setStateWidth(newWidth);
      };
    },
    onBorderMouseUp() {
      const { width, data, updateListWidth } = propsRef.current;
      document.body.style.cursor = 'default'
      document.removeEventListener('mousemove', headlers.onMouseMove)
      document.removeEventListener('mouseup', headlers.onBorderMouseUp)
      if (width !== data.get('width')) {
        updateListWidth(width);
      };
    },
    onBorderMouseDown(e) {
      if (!canEditList) return;
      lastClientX.current = e.clientX

      document.body.style.cursor = 'col-resize'
      document.addEventListener('mousemove', headlers.onMouseMove)
      document.addEventListener('mouseup', headlers.onBorderMouseUp)
    }
  };

  const className = classNames('desk-list__resize-border', {
    'desk-list__resize-border--disable': !canEditList
  })

  return (
    <div
      className={className}
      onMouseDown={headlers.onBorderMouseDown}
    />
  )
}

export default DeskListResizeBorder;
