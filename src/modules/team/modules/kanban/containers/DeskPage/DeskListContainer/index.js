import React from 'react'
import { useDrop, useDrag } from 'react-dnd'

import actions from '../../../actions/creators';
import * as dragTypes from '../../../constants/dragTypes';

import DeskList from './DeskList'

const DeskListContainer = (props) => {
  const { teamId, deskId, data, hasPermission, dispatch } = props;
  const position = data.get('position');
  const listId = data.get('id');

  const [{ isDragging }, listDrag] = useDrag({
    item: { type: dragTypes.LIST, listId },
    canDrag: hasPermission('editList'),
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult()
      if (!dropResult) return;
      dispatch(actions.updateKanbanListPosition(
        teamId, deskId, listId, {
          position: dropResult.position
        }
      ));
    },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  })

  const [{ isOver }, listDrop] = useDrop({
    accept: dragTypes.LIST,
    drop: () => ({ position }),
    collect: monitor => ({
      isOver: monitor.isOver(),
    }),
  })

  return (
    <div className='list-space' ref={listDrop}>
      {isOver && <div className='list-cap' />}
      <DeskList
        isDragging={isDragging}
        listDrag={listDrag}
        {...props}
      />
    </div>
  )
}

export default DeskListContainer;
