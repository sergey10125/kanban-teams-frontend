import React from 'react';

import squeezeСardsList from '../../../helpers/state/squeezeCardsList';
import DeskCardContainer from '../DeskCardContainer';


const createCardDOM = ({ cards, width, isWidthSqueezed, ...props }) => {
  const { listId, dragDeskState } = props;

  let cardArray = isWidthSqueezed? squeezeСardsList(cards, width) : cards;
  const cardDOMArray = [];
  let maxY = 4;

  if (cardArray.size) {
    const maxCardY = cardArray.getIn([ (cards.size - 1), 'positionY']);
    maxY = maxCardY >= maxY ? maxCardY + 1 : maxY;
  };

  let index = 0;
  for(let y=1; y <= maxY; y++) {
    for(let x=1; x <= width; x++) {
      let card = cardArray.get(index);
      let shownCard = null;
      const cardX = !!card && card.get('positionX');
      const cardY = !!card && card.get('positionY');

      if (!!card && (cardY < y || (cardY === y && cardX <= x)) ) {
        if (
          dragDeskState.isHoverCell(listId, x, y) &&
          card.get('id') !== dragDeskState.dragCard.id
        ) {
          card = null;
        } else {
          index += 1;
          const nextCard = cardArray.get(index);

          if (
            (!!nextCard && card.get('id') === dragDeskState.dragCard.id) &&
            (nextCard.get('positionY') === y) &&
            (nextCard.get('positionX') === x)
          ) {
            shownCard = nextCard;
            index += 1;
          }
        }
      } else {
        card = null;
      }

      cardDOMArray.push(
        <DeskCardContainer
          {...props}
          key={card ? card.get('id') :`y${y}x${x}`}
          position={{ x, y }}
          card={card}
          shownCard={shownCard}
        />
      )
    }
  }

  return cardDOMArray;
}

const KanbanList = (props) => {
  const cardsDOM = createCardDOM(props);
  return (
    <div className='desk-list__content'>
      {cardsDOM}
    </div>
  )
}

export default KanbanList
