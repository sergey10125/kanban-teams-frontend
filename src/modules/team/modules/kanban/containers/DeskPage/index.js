import React from 'react';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { useDispatch, useSelector } from "react-redux";

import hasTeamPermissionInit from 'utils/hasTeamPermissionInit';

import DragScrollContainer from '../../components/DragScrollContainer';
import useDragDeskState from '../../helpers/useDragDeskState';

import DeskHeader from './DeskHeader';
import DeskDragLayer from './DeskDragLayer';
import DeskListContainer from './DeskListContainer';

import CreateDeskList from './CreateDeskList';
import CreateDeskCard from './CreateDeskCard';


const DeskPage = () => {
  const dispatch = useDispatch();
  const dragDeskState = useDragDeskState();

  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const desk = useSelector(({ kanban }) => kanban.getIn(['selectDesk', 'data']));

  const teamId = team.get('id');
  const hasPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );


  return (
    <DndProvider backend={HTML5Backend}>
      <div className='desk'>
        <DeskDragLayer />
        <div className='desk__header'>
          <DeskHeader
            teamId={teamId}
            desk={desk}
            dispatch={dispatch}
            isChangeAllowed={hasPermission('editDesk')}
          />
        </div>
        <DragScrollContainer
          className='desk__content'
          accessDragClassName={[
            'desk-list__content',
            'card-space',
          ]}
        >
          {desk.get('lists').map((list, index) =>
            <DeskListContainer
              key={list.get('id')}
              teamId={teamId}
              deskId={desk.get('id')}
              data={list}
              dragDeskState={dragDeskState}
              hasPermission={hasPermission}
              dispatch={dispatch}
            />
          )}
          {hasPermission('createList') && (
            <CreateDeskList
              teamId={teamId}
              deskId={desk.get('id')}
              position={desk.get('lists').size}
              dispatch={dispatch}
            />
          )}
        </DragScrollContainer>
        {hasPermission('createCard') && (
          <CreateDeskCard dragDeskState={dragDeskState}/>
        )}
      </div>
    </DndProvider>
  )

}


export default DeskPage;
