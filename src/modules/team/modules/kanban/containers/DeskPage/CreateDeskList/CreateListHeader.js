import React, { useState, useEffect, useRef, useCallback } from 'react'
import { MdDone } from 'react-icons/md'

import actions from '../../../actions/creators';

const CreateListHeader = ({ teamId, deskId, dispatch }) => {
  const [ isEdit, setEditStatus ] = useState(false);
  const [ title, setTitle ] = useState('');
  const headerDOM = useRef(null);
  const inputDOM = useRef(null);

  const outsideClick = useRef(function outsideClick(e) {
    if (!!headerDOM.current && !headerDOM.current.contains(e.target)) {
      document.removeEventListener('mousedown', outsideClick)
      setEditStatus(false);
    }
  }).current;

  const startEditTitle = useCallback(() => {
    document.addEventListener('mousedown', outsideClick);
    setEditStatus(true);
  }, [ outsideClick ]);

  const createList = useCallback((title) => {
    if (!title.length) return;
    const payload = { title };

    dispatch(actions.createKanbanList(teamId, deskId, payload));
    setEditStatus(false)
    setTitle('')
  }, [ teamId, deskId, dispatch ]);

  const onEnterPress = useCallback((e, title) => {
    if (e.key === 'Enter') createList(title);
  }, [ createList ]);


  useEffect(() => {
    if (!isEdit) return;
    inputDOM.current.focus();
  }, [ isEdit ]);


  if (isEdit) {
    return (
      <div className='desk-list__header' ref={headerDOM} >
        <input
          className='desk-list__input'
          name='title'
          ref={inputDOM}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          maxLength={80}
          onKeyPress={(e) => onEnterPress(e, title)}
        />
        <MdDone
          className='desk-list__accept'
          onClick={() => createList(title)}
          />
      </div>
    );
  };


  return (
    <div className='desk-list__header'>
      <div
        className='desk-list__title desk-list__title--button'
        onClick={startEditTitle}
      >
        Create a new List
      </div>
    </div>
  );
};

export default CreateListHeader;
