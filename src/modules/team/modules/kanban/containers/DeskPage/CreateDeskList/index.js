import React from 'react';
import { useDrop } from 'react-dnd';

import * as dragTypes from '../../../constants/dragTypes';
import * as sizeTypes from '../../../constants/size';

import CreateListHeader from './CreateListHeader';

const CreateList = ({ position, ...props }) => {
  const style = { width: sizeTypes.DESK_CARD_WIDTH * 2 };

  const [{ isOver }, listDrop] = useDrop({
    accept: dragTypes.LIST,
    drop: () => ({ position }),
    collect: monitor => ({
      isOver: monitor.isOver(),
    }),
  });

  return (
    <div className='list-space' ref={listDrop}>
      {isOver && <div className='list-cap' />}
      <div className='desk-list desk-list--form' style={style}>
        <CreateListHeader {...props} />
        <div className='desk-list__content' />
      </div>
    </div>
  );
};


export default CreateList;
