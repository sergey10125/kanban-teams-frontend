import React, { useCallback } from 'react'
import { useDrop, useDrag } from 'react-dnd'
import { getEmptyImage } from 'react-dnd-html5-backend';
import { push } from 'connected-react-router'
import uuid from 'uuid';

import * as dragTypes from '../../../constants/dragTypes';
import actions from './../../../actions/creators';

import DeskCard from './DeskCard'

const DeskCardSpace = ({
  card,
  teamId,
  deskId,
  listId,
  position,
  dispatch,
  dragDeskState,
  shownCard,
  hasPermission
}) => {
  const isCardCap = !card || card.get('isCap');
  const cardId = !card || card.get('id');

  const openModal = useCallback(() => {
    if (isCardCap) return;
    const modalUrl = `/${teamId}/kanbans/${deskId}/card/${cardId}`;
    dispatch(push(modalUrl));
  }, [ teamId, deskId, cardId, isCardCap, dispatch ]);

  const onDropCard = (item, monitor) => {
    if (item.type === dragTypes.TASK) {
      const { card } = item;
      dispatch(actions.updateKanbanCardPosition(
        teamId, deskId, card.get('id'), listId, position
      ));
    } else if (item.type === dragTypes.NEW_TASK) {
      const token = uuid.v4()
      const { title } = item;

      dispatch(actions.createKanbanCard(
        teamId, deskId, listId, { token, title, position }
      ));
    }
  }

  const [ { isOver }, dropRef ] = useDrop({
    accept: [ dragTypes.TASK, dragTypes.NEW_TASK ],
    drop: onDropCard,
    hover: (item, monitor) => {
      dragDeskState.setHoverCellPosition(listId, position.x, position.y);
    },
    collect: (monitor) => ({ isOver: monitor.isOver() })
  });


  const [ { isDragging }, dragRef, preview ] = useDrag({
    item: { card: !card || card, type: dragTypes.TASK },
    canDrag: hasPermission('editCard'),
    begin: (monitor) => {
      dragDeskState.setDragCard(card.get('id'), position.x, position.y);
    },
    end: (item, monitor) => dragDeskState.clear(),
    collect: (monitor) => ({ isDragging: monitor.isDragging() }),
  });

  preview(getEmptyImage());

  return (
    <div className='card-space' ref={dropRef} >
      {isOver && (<div className='card-space__cap' />)}
      {!!card && (
        <DeskCard
          id={card.get('id')}
          title={card.get('title')}
          openModal={openModal}
          dragRef={dragRef}
          isHidden={isDragging}
        />
      )}
      {!!shownCard && (
        <DeskCard id={shownCard.get('id')} title={shownCard.get('title')} />
      )}
    </div>
  )
}

export default DeskCardSpace
