import React from 'react'
import classNames from 'classnames'

const DeskCard = ({
  id,
  title,
  dragRef,
  openModal,
  isHidden = false,
  className: additionalClassName
}) => {
  const className = classNames('card', additionalClassName, {
    'card--hide': isHidden
  })

  return (
    <div ref={dragRef} onClick={openModal} className={className}>
      <div className='card__title'>{title}</div>
    </div>
  )
}


export default DeskCard
