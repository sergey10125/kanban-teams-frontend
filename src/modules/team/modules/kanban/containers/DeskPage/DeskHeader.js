import React, { useState, useEffect, useRef, useCallback } from 'react'

import actions from '../../actions/creators'

const DeskHeader = ({ teamId, desk, dispatch, isChangeAllowed = true }) => {
  const [ isEdit, setIsEdited ] = useState(false);
  const [ value, setValue ] = useState(desk.get('title'));
  const inputEl = useRef(null);
  const deskId = desk.get('id');


  const onEnterPress = useCallback((e) => {
    if(e.key !== 'Enter' || !value.length) return;
    const options = { fromForm: false };
    const payload = { title: value };

    dispatch(actions.updateKanbanDeskData(teamId, deskId, payload, options));
    setIsEdited(false);
  }, [ teamId, deskId, value, dispatch ]);

  const onDoubleClick = useCallback((e)=> {
    if (!isChangeAllowed) return;
    e.preventDefault();
    setIsEdited(true);
  }, [ isChangeAllowed ]);

  const startEdit = useCallback((e)=> {
    if (!isChangeAllowed) return;
    setIsEdited(true);
  }, [ isChangeAllowed ]);


  useEffect(() => {
    if (!isEdit) return;
    inputEl.current.focus();
    inputEl.current.addEventListener("blur", () => setIsEdited(false));
  }, [ isEdit ]);


  if (!isEdit) {
    return (
      <div className='desk__title' onDoubleClick={onDoubleClick} >
        {desk.get('title')}
      </div>
    );
  };

  return (
    <div onClick={startEdit} className='desk__title'>
      <input
        className='desk__input'
        ref={inputEl}
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onKeyPress={(e) => onEnterPress(e, value)}
      />
    </div>
  );
};

export default DeskHeader;
