import React from 'react'
import { useDragLayer } from 'react-dnd'

import * as dragTypes from '../../constants/dragTypes';
import DeskCard from './DeskCardContainer/DeskCard';

let layerStyles = {
  position: 'fixed',
  pointerEvents: 'none',
  zIndex: 100,
  left: 0,
  top: 0,
  width: '100%',
  height: '100%',
}



const snapToGrid = (x, y) =>  {
  const ROUNDING_PX = 15
  const snappedX = Math.round(x / ROUNDING_PX) * ROUNDING_PX
  const snappedY = Math.round(y / ROUNDING_PX) * ROUNDING_PX

  return [snappedX, snappedY]
}

const getItemStyles = (initialOffset, currentOffset) => {
  if (!initialOffset || !currentOffset)
    return { display: 'none' };
  let { x, y } = currentOffset;

  [x, y] = snapToGrid(x - initialOffset.x, y - initialOffset.y)
  x += initialOffset.x;
  y += initialOffset.y;

  const transform = `translate(${x}px, ${y}px)`;
  return {
    WebkitTransform: transform,
    transform
  };
}

const DeskDragLayer = (props) => {

  const {
    itemType,
    isDragging,
    item,
    initialOffset,
    currentOffset,
  } = useDragLayer(monitor => ({
    item: monitor.getItem(),
    itemType: monitor.getItemType(),
    initialOffset: monitor.getInitialSourceClientOffset(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging(),
  }))


  if (!isDragging || itemType === dragTypes.LIST) {
    return null
  };

  let title = item.title;
  if (dragTypes.TASK === itemType) {
    title = item.card.get('title');
  };

  return (
    <div style={layerStyles}>
      <div style={getItemStyles(initialOffset, currentOffset)}>
        <DeskCard title={title} className='card--layer' />
      </div>
    </div>
  )
}

export default DeskDragLayer
