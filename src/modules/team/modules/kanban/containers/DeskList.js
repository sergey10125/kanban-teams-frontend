import React, { useEffect, useCallback } from 'react'
import { Link } from 'react-router-dom'
import { push } from 'connected-react-router'
import { useDispatch, useSelector } from "react-redux";

import hasTeamPermissionInit from 'utils/hasTeamPermissionInit'
import DropdownMenu from 'components/DropdownMenu'
import Loader from 'components/Loader'
import Dots from 'components/Dots'

import { openModal } from 'root/actions/creators/system'

import actions from '../actions/creators'


const KanbanListPage = () => {
  const dispatch = useDispatch();

  const listState = useSelector(({ kanban }) => kanban.get('list'));
  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');

  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );


  const canEditDesk = hasTeamPermission('editDesk');
  const canRemoveDesk = hasTeamPermission('removeDesk');



  const createDDmenu = useCallback((deskId) => {

    const redirectToDeskEdit = () => {
      const url = `/${teamId}/kanbans/${deskId}/edit`;
      dispatch(push(url));
    };
    const callDeleteModal = () => {
      dispatch(openModal('ConfirmModal', {
        message: 'Are you sure you want to remove a team member ?',
        actionFun: () => dispatch(actions.removeKanbanDesk(teamId, deskId))
      }));
    };

    const options = [];
    if (canEditDesk) {
      options.push({ title: 'Edit', action: redirectToDeskEdit });
    };
    if (canRemoveDesk) {
      options.push({ title: 'Delete', action: callDeleteModal });
    };

    return options;

  }, [ teamId, dispatch, canEditDesk, canRemoveDesk ]);


  useEffect(() => {
    if (listState.get('teamId') !== teamId) {
      dispatch(actions.fetchTeamKanbanDeskList(teamId));
    };
  });


  if (!listState.get('isLoaded')) return <Loader />;

  const showDDMenu = canEditDesk || canRemoveDesk;

  return (
    <div className='page'>
      <h1 className='page__headline'>Kanban desk list</h1>
      {hasTeamPermission('createDesk') && (
        <div className='page-toolbar'>
          <Link
            className='btn page-toolbar__btn'
            to={`/${teamId}/kanbans/new`}
          >
            Create new kanban desk
          </Link>
        </div>
      )}
      <div className='table100'>
        <table>
          <thead>
            <tr className="row100 head">
              <th className="column100">Title</th>
              <th className="column100 col-dd"></th>
            </tr>
          </thead>
          <tbody>
            {listState.get('data').map(item =>
              <tr className='row100' key={item.get('id')} >
                <td className="column100 column1">
                  <Link
                    to={`/${teamId}/kanbans/${item.get('id')}`}
                    className='list-item__title link'
                  >
                    {item.get('title')}
                  </Link>
                </td>
                <td className="column100 col-dd">
                {showDDMenu && (
                  <DropdownMenu
                    className='list-item__menu'
                    list={createDDmenu(item.get('id'))}
                  >
                    <Dots />
                  </DropdownMenu>
                )}
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  )
}


export default KanbanListPage;
