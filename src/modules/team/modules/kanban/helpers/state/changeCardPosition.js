export default (desk, cardId, listId, position) => {
  const { x, y } = position
  let deskLists = desk.get('lists')
  let cardPosition = {}
  let card = null

  for (let x = 0; x < deskLists.size; x++) {
    let cards = deskLists.get(x).get('cards')

    for (let y = 0; y < cards.size; y++) {
      const selectCard = cards.get(y)
      if (selectCard.get('id') === cardId) {
        cardPosition.listIndex = x
        cardPosition.cardIndex = y
        card = selectCard
        break
      }
    }

    if (card)
      break
  }

  if (card) {
    card = card.set('positionX', x).set('positionY', y)
    deskLists = deskLists.update(cardPosition.listIndex, list => {
      return list.update('cards', cards => {
        return cards.delete(cardPosition.cardIndex)
      });
    })

    let listIndex = deskLists.findIndex(item => item.get('id') === listId);
    deskLists = deskLists.update(listIndex, list => {
      return list.update('cards', cards => {
        if (!cards.size) return cards.push(card);
        let listCards = cards;
        let insertCard = card;

        for (let i=0; i < listCards.size; i++) {
          let card = listCards.get(i)
          if (
            card.get('positionX') === insertCard.get('positionX') &&
            card.get('positionY') === insertCard.get('positionY')
          ) {
            listCards = listCards.set(i, insertCard)
            insertCard = card

            if (insertCard.get('positionX') === list.get('width')) {
              insertCard = insertCard
                .update('positionY', value => value + 1)
                .set('positionX', 1)
            } else {
              insertCard = insertCard.update('positionX', value => value + 1)
            }
          } else if (
            (
              card.get('positionX') > insertCard.get('positionX') &&
              card.get('positionY') === insertCard.get('positionY')
            ) ||
            card.get('positionY') > insertCard.get('positionY')
          ) {
            listCards = listCards.insert(i, insertCard)
            insertCard = null
            break
          }
        }

        if (insertCard) {
          listCards = listCards.push(insertCard)
        }

        return listCards;
      })
    })
  }

  return desk.set('lists', deskLists)
}
