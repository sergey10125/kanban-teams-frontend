export default (desk, card, position) => {
  const { listId } = position

  let deskLists = desk.get('lists')
  let listIndex = deskLists.findIndex(item => item.get('id') === listId)
  let list = deskLists.get(listIndex)
  let listCards = list.get('cards')

  if (!listCards.size) {
    listCards = listCards.push(card)
  } else {
    let insertCard = card;
    for (let i=0; i < listCards.size; i++) {
      let card = listCards.get(i)
      if (card.get('positionX') === insertCard.get('positionX') &&
        card.get('positionY') === insertCard.get('positionY')) {
          listCards = listCards.set(i, insertCard)
          insertCard = card

          if (insertCard.get('positionX') === list.get('width')) {
            insertCard = insertCard.update('positionY', value => value + 1)
              .set('positionX', 1)
          } else {
            insertCard = insertCard.update('positionX', value => value + 1)
          }
      } else if (
        (card.get('positionX') > insertCard.get('positionX') &&
        card.get('positionY') === insertCard.get('positionY')) ||
        card.get('positionY') > insertCard.get('positionY')
      ) {
        listCards = listCards.insert(i, card)
        insertCard = null
        break
      }
    }

    if (insertCard) {
      listCards = listCards.push(insertCard)
    }
  }

  list = list.set('cards', listCards)
  deskLists = deskLists.set(listIndex, list)
  return desk.set('lists', deskLists)
}
