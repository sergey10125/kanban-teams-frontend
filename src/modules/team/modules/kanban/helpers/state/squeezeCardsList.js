import { List } from 'immutable'

export default (cardArr, width) => {
  let changedCardArr = []
  let rowArr = []
  let tmpArr = []

  for(let i=0; i <= cardArr.size - 1; i++) {
    const card = cardArr.get(i)
    const nextCard = cardArr.get(i + 1)
    const prevCard = rowArr[rowArr.length - 1]

    if (!!prevCard) {
      const positionDifference = card.get('positionX') - prevCard.get('positionX')

      if (positionDifference > 1)
        rowArr = rowArr.concat(new Array(positionDifference - 1))

    } else if (card.get('positionX') !== 1) {
      const emptyArr = new Array(card.get('positionX') - 1)
      rowArr = emptyArr.concat(rowArr)
    }

    rowArr.push(card)
    if (!nextCard || card.get('positionY') !== nextCard.get('positionY')) {
      const positionY = card.get('positionY')
      rowArr = tmpArr.concat(rowArr)
      tmpArr = []
      // clear all extra array empty item

      for (let i = rowArr.length - 1; i >= 0; i--) {
        if (rowArr.length <= width) break;
        if (!rowArr[i]) rowArr.splice(i, 1)
      }

      // cut off the excess cards and save them in tmpArr
      if (rowArr.length > width) {
        tmpArr = rowArr.splice(width)
      }

      //save card in new array with new positions
      for (let x = 0; x < rowArr.length; x++) {
        let item = rowArr[x]
        if (!!item) {
          item = item.set('positionY', positionY)
          item = item.set('positionX', x + 1)
          changedCardArr.push(item)
        }
      }
      rowArr = []
      const positionYDiff = !!nextCard && nextCard.get('positionY') - positionY
      if (!!tmpArr.length && !!positionYDiff && positionYDiff > 1) {
        for (let y=1; y < positionYDiff; y++) {
          for (let x=1; x <= width; x++) {
            let item = tmpArr.shift()
            item = item.set('positionY', positionY + y)
            item = item.set('positionX', x)
            changedCardArr.push(item)

            if (!tmpArr.length)
              break
          }
          if (!tmpArr.length)
            break
        }
      }
    }
  }
  if (!!tmpArr.length) {
    let positionY = changedCardArr[changedCardArr.length - 1].get('positionY') + 1
    let positionX = 1
    while(tmpArr.length) {
      let item = tmpArr.shift()
      item = item.set('positionY', positionY)
      item = item.set('positionX', positionX)
      positionX += 1
      changedCardArr.push(item)

      if (positionX > width) {
        positionY += 1
        positionX = 1
      }
    }
  }

  return List(changedCardArr)
}
