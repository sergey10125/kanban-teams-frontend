export default (desk, capId, card) => {
  let deskLists = desk.get('lists')
  let cardPosition = {}
  let isCardFind = false

  for (let x = 0; x < deskLists.size; x++) {
    let cards = deskLists.get(x).get('cards')
    for (let y = 0; y < cards.size; y++) {
      const selectCard = cards.get(y)
      if (selectCard.get('id') === capId) {
        cardPosition.listIndex = x
        cardPosition.cardIndex = y
        isCardFind = true
        break
      }
    }

    if (isCardFind)
      break
  }

  if (isCardFind) {
    deskLists = deskLists.update(cardPosition.listIndex, list =>
      list.update('cards', listCards =>
        listCards.update(cardPosition.cardIndex, cap =>
          card.set('positionX', cap.get('positionX'))
            .set('positionY', cap.get('positionY'))
        )
      )
    )
  }

  return desk.set('lists', deskLists)
}
