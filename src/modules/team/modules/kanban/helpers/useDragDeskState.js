import { useState } from 'react';

const defaultDragDeskState = {
  hoverCellPosition: {},
  dragCard: {}
}

const useDragDeskState = () => {
  const [ value, setValue ] = useState(defaultDragDeskState);

  return {
    dragCard: value.dragCard,
    setDragCard: (id, x, y) => setValue({
      ...value,
      dragCard: { id, x, y }
    }),
    hoverCellPosition: value.hoverCellPosition,
    isHoverCell(listId, x, y) {
      const hCP = value.hoverCellPosition;
      return (hCP.listId === listId && hCP.x === x && hCP.y === y);
    },
    setHoverCellPosition(listId, x, y) {
      if (!this.isHoverCell(listId, x, y)) {
        setValue({
          ...value,
          hoverCellPosition: { listId, x, y }
        });
      };
    },
    clear: () => setValue(defaultDragDeskState)
  };
};

export default useDragDeskState;
