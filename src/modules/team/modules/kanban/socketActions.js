import actions from './actions/creators';

export default {
  'createKanbanDesk': actions.createKanbanDeskSuccess,
  'updateKanbanDeskData': actions.updateKanbanDeskDataSuccess,
  'removeKanbanDesk': actions.removeKanbanDeskSuccess,

  'createKanbanList': actions.createKanbanListSuccess,
  'updateKanbanListData': actions.updateKanbanListSuccess,
  'updateKanbanListPosition': actions.updateKanbanListPositionNotification,
  'updateKanbanListWidth': actions.updateKanbanListWidthNotification,
  'removeKanbanList': actions.removeKanbanListSuccess,

  'createKanbanCard': actions.createKanbanCardNotification,
  'updateKanbanCardPosition': actions.updateKanbanCardPositionNotification,
  'updateKanbanCardData': actions.updateKanbanCardSuccess,
  'removeKanbanCard': actions.removeKanbanCardSuccess
};
