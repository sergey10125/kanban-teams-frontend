import {
  mergeMap,
  catchError,
  takeUntil,
  map,
  take
} from 'rxjs/operators'
import { of, merge } from 'rxjs'
import { ofType } from 'redux-observable'
import { startSubmit } from 'redux-form'
import { LOCATION_CHANGE, push } from 'connected-react-router';

import { INIT_ERROR } from 'root/actions/types/system'
import { initError } from 'root/actions/creators/system'

import * as types from '../actions/types'
import actions from '../actions/creators'
import api from '../api'

export const fetchTeamKanbanDeskList = action$ => action$.pipe(
  ofType(types.FETCH_TEAM_KANBAN_LIST),
  mergeMap(({ payload, meta }) => api.fetchTeamKanbanList(payload, meta).pipe(
    map(payload => actions.fetchTeamKanbanDeskListSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_TEAM_KANBAN_LIST)),
    catchError(error => of(initError(error))),
  ))
)

export const getDesk = action$ => action$.pipe(
  ofType(types.FETCH_KANBAN_DESK),
  mergeMap(({ payload, meta }) => api.fetchKanban(payload, meta).pipe(
    map(payload => actions.fetchKanbanDeskSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_KANBAN_DESK)),
    catchError(error => of(initError(error))),
  ))
)

export const createDesk = action$ => action$.pipe(
  ofType(types.CREATE_KANBAN_DESK),
  mergeMap(({ payload, meta }) => merge(
    of(startSubmit('kanbanForm')),
    api.createKanban(payload, meta).pipe(
      map(payload => actions.createKanbanDeskSuccess(payload, meta)),
      catchError((error) => of(initError(error, {
        formName: 'kanbanForm',
      })))
    ),
    action$.pipe(
      ofType(types.CREATE_KANBAN_DESK_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${meta.teamId}/kanbans/${payload.id}`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  ))
)

export const updateKanbanDataFromForm = action$ => action$.pipe(
  ofType(types.UPDATE_KANBAN_DESK_DATA),
  mergeMap(({ payload, meta }) => merge(
    api.updateKanbanData(payload, meta).pipe(
      map((payload) => actions.updateKanbanDeskDataSuccess(payload, meta)),
      catchError((error) => of(initError(error)))
    ),
    action$.pipe(
      ofType(types.UPDATE_KANBAN_DESK_DATA_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${meta.teamId}/kanbans`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  )),
)

export const updateKanbanDataFromDeskPage = action$ => action$.pipe(
  ofType(types.UPDATE_KANBAN_DESK_DATA_FROM_DESK_PAGE),
  mergeMap(({ payload, meta }) => api.updateKanbanData(payload, meta).pipe(
    map((payload) => actions.updateKanbanDeskDataSuccess(payload, meta)),
    catchError((error) => of(initError(error)))
  )),
)

export const removeKanban = action$ => action$.pipe(
  ofType(types.REMOVE_KANBAN_DESK),
  mergeMap(({ payload, meta }) => api.removeKanban(payload, meta).pipe(
    map(() => actions.removeKanbanDeskSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)
