import {
  mergeMap,
  catchError,
  takeUntil,
  map,
} from 'rxjs/operators'
import { of } from 'rxjs'
import { ofType } from 'redux-observable'

import { initError } from 'root/actions/creators/system'

import * as types from '../actions/types'
import actions from '../actions/creators'
import api from '../api'

export const fetchCard = action$ => action$.pipe(
  ofType(types.FETCH_KANBAN_CARD),
  mergeMap(({ payload, meta }) => api.fetchCard(payload, meta).pipe(
    map(payload => actions.fetchCardSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_KANBAN_CARD)),
    catchError(error => of(initError(error, {
      notFoundLocation: 'kanbanCardModal'
    }))),
  ))
)

export const createCard = action$ => action$.pipe(
  ofType(types.CREATE_KANBAN_CARD),
  mergeMap(({ payload, meta }) => api.createCard(payload, meta).pipe(
    map(card =>
      actions.createKanbanCardSuccess(
        { card, token: payload.token },
        meta
      )
    ),
    catchError(error => of(initError(error))),
  ))
)

export const updateCardData = action$ => action$.pipe(
  ofType(types.UPDATE_KANBAN_CARD),
  mergeMap(({ payload, meta }) => api.updateCard(payload, meta).pipe(
    map(payload => actions.updateKanbanCardSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)

export const updateCardPosition = action$ => action$.pipe(
  ofType(types.UPDATE_KANBAN_CARD_POSITION),
  mergeMap(({ payload, meta }) => api.updateCardPosition(payload, meta).pipe(
    map(payload => actions.updateKanbanCardPositionSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)

export const removeCard = action$ => action$.pipe(
  ofType(types.REMOVE_KANBAN_CARD),
  mergeMap(({ payload, meta }) => api.removeCard(payload, meta).pipe(
    map(data => actions.removeKanbanCardSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)
