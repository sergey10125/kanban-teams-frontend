import * as kanbanDescEpic from './desc';
import * as kanbanListEpic from './list';
import * as kanbanCardEpic from './card';

export default {
  ...kanbanDescEpic,
  ...kanbanListEpic,
  ...kanbanCardEpic
}
