import {
  mergeMap,
  catchError,
  map,
} from 'rxjs/operators'
import { of } from 'rxjs'
import { ofType } from 'redux-observable'

import { initError } from 'root/actions/creators/system'

import * as types from '../actions/types'
import actions from '../actions/creators'
import api from '../api'

export const createList = action$ => action$.pipe(
  ofType(types.CREATE_KANBAN_LIST),
  mergeMap(({ payload, meta }) => api.createList(payload, meta).pipe(
    map(payload => actions.createKanbanListSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)

export const updateListTitle = action$ => action$.pipe(
  ofType(types.UPDATE_KANBAN_LIST),
  mergeMap(({ payload, meta }) => api.updateListTitle(payload, meta).pipe(
    map(() => actions.updateKanbanListSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)

export const updateListPosition = action$ => action$.pipe(
  ofType(types.UPDATE_KANBAN_LIST_POSITION),
  mergeMap(({ payload, meta }) => api.updateListPosition(payload, meta).pipe(
    map(() => actions.updateKanbanListPositionSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)

export const updateListWidth = action$ => action$.pipe(
  ofType(types.UPDATE_KANBAN_LIST_WIDTH),
  mergeMap(({ payload, meta }) => api.updateListWidth(payload, meta).pipe(
    map(() => actions.updateKanbanListWidthSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)

export const removeList = action$ => action$.pipe(
  ofType(types.REMOVE_KANBAN_LIST),
  mergeMap(({ payload, meta }) => api.removeList(payload, meta).pipe(
    map(() => actions.removeKanbanListSuccess(payload, meta)),
    catchError(error => of(initError(error))),
  ))
)
