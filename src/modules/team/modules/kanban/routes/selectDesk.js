import React, { useEffect } from 'react'
import { Switch, Route, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";

import hasTeamPermissionInit from 'utils/hasTeamPermissionInit'
import { joinSocketRoom, leaveSocketRoom } from 'root/actions/creators/socket';
import Loader from 'components/Loader';
import NotFoundPage from 'components/NotFoundPage';

import actions from '../actions/creators';
import EditKanbanForm from '../containers/form/EditKanbanForm';
import KanbanPageRoutes from './kanbanPage';

const SelectDeskRoutes = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');
  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );

  const deskState = useSelector(({ kanban }) => kanban.get('selectDesk'));
  const deskId = deskState.getIn(['data', 'id']);
  const isLoaded = deskState.get('isLoaded');

  const isSocketConnected = deskState.get('isSocketConnected');
  const connectedToken = deskState.get('connectedToken');

  const paramDeskId = +params.deskId;

  useEffect(() => {
    if (deskId !== paramDeskId) {
      dispatch(actions.fetchKanbanDesk(teamId, paramDeskId));
      if (isSocketConnected) {
        dispatch(leaveSocketRoom('kanban', teamId));
      };
    };

    if (isLoaded && !isSocketConnected) {
      dispatch(joinSocketRoom(connectedToken));
    };
  });


  if (!isLoaded || !isSocketConnected) {
    return <Loader />;
  };

  let renderDeskEditPageComponent = EditKanbanForm;
  if (!hasTeamPermission('editDesk')) {
    renderDeskEditPageComponent = NotFoundPage;
  }

  return (
    <Switch>
      <Route
        exact
        path='/:teamId/kanbans/:deskId/edit'
        component={renderDeskEditPageComponent}
      />
      <Route path='/:teamId/kanbans/:deskId' component={KanbanPageRoutes} />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  )
}

export default SelectDeskRoutes;
