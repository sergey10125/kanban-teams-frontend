import React from 'react'
import { Switch, Route } from 'react-router-dom'

import NotFoundPage from 'components/NotFoundPage';

import DeskList from '../containers/DeskList';
import CreateKanbanForm from '../containers/form/KanbanForm';
import SelectDeskRoutes from './selectDesk';

const KanbanRoutes = () => {
  return (
    <Switch>
      <Route path='/:teamId/kanbans/new' component={CreateKanbanForm} />
      <Route path='/:teamId/kanbans/:deskId' component={SelectDeskRoutes} />
      <Route path='/:teamId/kanbans' component={DeskList} />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  );
};

export default KanbanRoutes;
