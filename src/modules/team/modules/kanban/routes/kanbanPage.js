import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';

import DeskPage from '../containers/DeskPage';
import CardModal from '../containers/CardModal';

const KanbanPageRoutes = () => {
  return (
    <Fragment>
      <Route
        path='/:teamId/kanbans/:deskId'
        component={DeskPage}
      />
      <Route
        exact
        path='/:teamId/kanbans/:deskId/card/:cardId'
        component={CardModal}
      />
    </Fragment>
  )
};

export default KanbanPageRoutes;
