import { from } from 'rxjs'
import request from 'utils/request'

const api = {
  fetchTeamKanbanList: (payload, { teamId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban`;
    const req = request(url, 'GET', { meta })
      .then(response => response.json());

    return from(req);
  },


  fetchKanban: (payload, { teamId, deskId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}`;
    const req = request(url, 'GET', { meta })
      .then(response => response.json());

    return from(req);
  },
  createKanban: (payload, { teamId, ...meta}) => {
    const url = `/api/team/${teamId}/kanban`;
    const req = request(url, 'POST', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  updateKanbanData: (payload, { teamId, deskId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}`;
    const req = request(url, 'PUT', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  removeKanban: (payload, { teamId, deskId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}`;
    const req = request(url, 'DELETE', { meta });
    return from(req);
  },


  createList: (payload, { teamId, deskId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/lists`;
    const req = request(url, 'POST', { payload, meta })
      .then(response => response.json());

    return from(req)
  },
  updateListTitle: (payload, { teamId, deskId, listId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/lists/${listId}`;
    const req = request(url, 'PUT', { payload, meta });
    return from(req);
  },
  updateListPosition: (payload, { teamId, deskId, listId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/lists/${listId}/position`;
    const req = request(url, 'PUT', { payload, meta });
    return from(req);
  },
  updateListWidth: (payload, { teamId, deskId, listId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/lists/${listId}/width`;
    const req = request(url, 'PUT', { payload, meta });
    return from(req);
  },
  removeList: (payload, { teamId, deskId, listId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/lists/${listId}`;
    const req = request(url, 'DELETE', { payload, meta });
    return from(req);
  },


  fetchCard: (payload, { teamId, deskId, cardId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/cards/${cardId}`;
    const req = request(url, 'GET', { meta })
      .then(response => response.json());

    return from(req);
  },
  createCard: (payload, { teamId, deskId, listId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/lists/${listId}/card`;
    const req = request(url, 'POST', { meta, payload })
      .then(response => response.json());

    return from(req);
  },
  updateCard: (payload, { teamId, deskId, cardId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/cards/${cardId}`;
    const req = request(url, 'PUT', { payload, meta })
      .then(response => response.json());
      
    return from(req);
  },
  updateCardPosition: (payload, { teamId, deskId, listId, cardId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/cards/${cardId}/position/${listId}`;
    const req = request(url, 'PUT', { payload, meta });
    return from(req);
  },
  removeCard: (payload, { teamId, deskId, cardId, ...meta }) => {
    const url = `/api/team/${teamId}/kanban/${deskId}/cards/${cardId}`;
    const req = request(url, 'DELETE', { payload, meta });
    return from(req);
  },
};

export default api;
