import * as types from '../types'


export const fetchCard = (teamId, deskId, cardId) => ({
  type: types.FETCH_KANBAN_CARD,
  meta: { teamId, deskId, cardId }
})

export const fetchCardSuccess = (payload, meta) => ({
  type: types.FETCH_KANBAN_CARD_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const createKanbanCard = (teamId, deskId, listId, payload) => ({
  type: types.CREATE_KANBAN_CARD,
  payload,
  meta: { teamId, deskId, listId }
})

export const createKanbanCardSuccess = (payload, meta) => ({
  type: types.CREATE_KANBAN_CARD_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
export const createKanbanCardNotification = (payload, meta) => ({
  type: types.CREATE_KANBAN_CARD_NOTIFICATION,
  payload: payload || {},
  meta: meta || {}
})


export const updateKanbanCard = (teamId, deskId, cardId, payload) => ({
  type: types.UPDATE_KANBAN_CARD,
  payload,
  meta: { teamId, deskId, cardId }
})

export const updateKanbanCardSuccess = (payload, meta) => ({
  type: types.UPDATE_KANBAN_CARD_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})


export const updateKanbanCardPosition = (
  teamId,
  deskId,
  cardId,
  listId,
  position
) => ({
  type: types.UPDATE_KANBAN_CARD_POSITION,
  payload: { position },
  meta: { teamId, deskId, cardId, listId}
})

export const updateKanbanCardPositionSuccess = (payload, meta) => ({
  type: types.UPDATE_KANBAN_CARD_POSITION_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
export const updateKanbanCardPositionNotification = (payload, meta) => ({
  type: types.UPDATE_KANBAN_CARD_POSITION_NOTIFICATION,
  payload: payload || {},
  meta: meta || {}
})

export const removeKanbanCard = (teamId, deskId, cardId) => ({
  type: types.REMOVE_KANBAN_CARD,
  meta: { teamId, deskId, cardId }
})

export const removeKanbanCardSuccess = (payload, meta) => ({
  type: types.REMOVE_KANBAN_CARD_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
