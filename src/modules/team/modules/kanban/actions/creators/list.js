import * as types from '../types'


export const createKanbanList = (teamId, deskId, payload) => ({
  type: types.CREATE_KANBAN_LIST,
  payload,
  meta: { teamId, deskId }
})

export const createKanbanListSuccess = (payload, meta) => ({
  type: types.CREATE_KANBAN_LIST_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})


export const updateKanbanList = (teamId, deskId, listId, payload) => ({
  type: types.UPDATE_KANBAN_LIST,
  payload,
  meta: { teamId, deskId, listId }
})

export const updateKanbanListSuccess = (payload, meta) => ({
  type: types.UPDATE_KANBAN_LIST_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})


export const updateKanbanListPosition = (teamId, deskId, listId, payload) => ({
  type: types.UPDATE_KANBAN_LIST_POSITION,
  payload,
  meta: { teamId, deskId, listId }
})
export const updateKanbanListPositionSuccess = (payload, meta) => ({
  type: types.UPDATE_KANBAN_LIST_POSITION_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
export const updateKanbanListPositionNotification = (payload, meta) => ({
  type: types.UPDATE_KANBAN_LIST_POSITION_NOTIFICATION,
  payload: payload || {},
  meta: meta || {}
})


export const updateKanbanListWidth = (teamId, deskId, listId, payload) => ({
  type: types.UPDATE_KANBAN_LIST_WIDTH,
  payload,
  meta: { teamId, deskId, listId }
})
export const updateKanbanListWidthSuccess = (payload, meta) => ({
  type: types.UPDATE_KANBAN_LIST_WIDTH_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
export const updateKanbanListWidthNotification = (payload, meta) => ({
  type: types.UPDATE_KANBAN_LIST_WIDTH_NOTIFICATION,
  payload: payload || {},
  meta: meta || {}
})


export const removeKanbanList = (teamId, deskId, listId) => ({
  type: types.REMOVE_KANBAN_LIST,
  meta: { teamId, deskId, listId }
})

export const removeKanbanListSuccess = (payload, meta) => ({
  type: types.REMOVE_KANBAN_LIST_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
