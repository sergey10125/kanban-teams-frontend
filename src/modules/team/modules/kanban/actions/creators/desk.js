import * as types from '../types'


export const fetchTeamKanbanDeskList = (teamId) => ({
  type: types.FETCH_TEAM_KANBAN_LIST,
  meta: { teamId }
})
export const fetchTeamKanbanDeskListSuccess = (payload, meta) => ({
  type: types.FETCH_TEAM_KANBAN_LIST_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})


export const fetchKanbanDesk = (teamId, deskId) => ({
  type: types.FETCH_KANBAN_DESK,
  meta: { teamId, deskId }
})
export const fetchKanbanDeskSuccess = (payload, meta) => ({
  type: types.FETCH_KANBAN_DESK_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})


export const createKanbanDesk = (teamId, payload) => ({
  type: types.CREATE_KANBAN_DESK,
  payload,
  meta: { teamId }
})
export const createKanbanDeskSuccess = (payload, meta) => ({
  type: types.CREATE_KANBAN_DESK_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const updateKanbanDeskData = (
  teamId,
  deskId,
  payload,
  options = { fromForm: true }
) => {
  let type = types.UPDATE_KANBAN_DESK_DATA;
  if (!options.fromForm) type = types.UPDATE_KANBAN_DESK_DATA_FROM_DESK_PAGE;

  return {
    type,
    payload,
    meta: { teamId, deskId }
  }
}
export const updateKanbanDeskDataSuccess = (payload, meta) => ({
  type: types.UPDATE_KANBAN_DESK_DATA_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const removeKanbanDesk = (teamId, deskId) => ({
  type: types.REMOVE_KANBAN_DESK,
  meta: { teamId, deskId }
})
export const removeKanbanDeskSuccess = (payload, meta) => ({
  type: types.REMOVE_KANBAN_DESK_SUCCESS,
  meta: meta || {}
})
