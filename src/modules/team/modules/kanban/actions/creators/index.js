import * as deskActionType from './desk';
import * as listActionType from './list';
import * as cardActionType from './card';

export default {
  ...deskActionType,
  ...listActionType,
  ...cardActionType
}
