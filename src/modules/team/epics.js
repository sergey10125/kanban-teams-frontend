import {
  mergeMap,
  catchError,
  takeUntil,
  map,
  take
} from 'rxjs/operators'
import { of, merge } from 'rxjs'
import { ofType } from 'redux-observable'
import { startSubmit } from 'redux-form'
import { LOCATION_CHANGE, push } from 'connected-react-router';

import { INIT_ERROR } from 'root/actions/types/system'
import { initError } from 'root/actions/creators/system'

import * as types from './actions/types'
import * as actions from './actions/creators'
import api from './api'


export const confirmTeamInvite = action$ => action$.pipe(
  ofType(types.CONFIRM_TEAM_INVITE),
  mergeMap(({ payload, meta }) => merge(
    api.confirmTeamInvite(payload, meta).pipe(
      map(payload => actions.confirmTeamInviteSuccess(payload, meta)),
      catchError(error => of(initError(error))),
    ),
    action$.pipe(
      ofType(types.CONFIRM_TEAM_INVITE_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${payload.id}`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  ))
)

export const fetchUserTeams = action$ => action$.pipe(
  ofType(types.FETCH_TEAM_LIST),
  mergeMap(action => api.fetchTeamList().pipe(
    map(payload => actions.fetchTeamListSuccess(payload)),
    takeUntil(action$.ofType(types.FETCH_TEAM_LIST)),
    catchError(error => of(initError(error))),
  ))
)

export const fetchTeam = action$ => action$.pipe(
  ofType(types.FETCH_TEAM),
  mergeMap(({ payload, meta }) => api.fetchTeam(payload, meta).pipe(
    map((payload) => actions.fetchTeamSuccess(payload, meta)),
    takeUntil(action$.ofType(types.FETCH_TEAM)),
    catchError(error => of(initError(error))),
  ))
)

export const createTeam = action$ => action$.pipe(
  ofType(types.CREATE_TEAM),
  mergeMap(({ payload, meta }) => merge(
    of(startSubmit('teamForm')),
    api.createTeam(payload, meta).pipe(
      map(payload => actions.createTeamSuccess(payload)),
      catchError((error) => of(initError(error, {
        formName: 'teamForm',
      })))
    ),
    action$.pipe(
      ofType(types.CREATE_TEAM_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${payload.id}`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  ))
)

export const updateTeamData = action$ => action$.pipe(
  ofType(types.UPDATE_TEAM_DATA),
  mergeMap(({ payload, meta }) => merge(
    of(startSubmit('teamForm')),
    api.updateTeamData(payload, meta).pipe(
      map(() => actions.updateTeamDataSuccess(payload, meta)),
      catchError((error) => of(initError(error, {
        formName: 'teamForm',
      })))
    ),
    action$.pipe(
      ofType(types.UPDATE_TEAM_DATA_SUCCESS),
      take(1),
      map(({ payload, meta }) => push(`/${meta.teamId}`)),
      takeUntil(action$.ofType(INIT_ERROR, LOCATION_CHANGE))
    )
  ))
)

export const leaveTeam = action$ => action$.pipe(
  ofType(types.LEAVE_TEAM),
  mergeMap(({ payload, meta }) => api.leaveTeam(payload, meta).pipe(
    map(() => actions.leaveTeamSuccess(payload, meta)),
    catchError((error) => of(initError(error)))
  ))
)

export const removeTeam = (action$, store) => action$.pipe(
  ofType(types.REMOVE_TEAM),
  mergeMap(({ payload, meta }) => api.removeTeam(payload, meta).pipe(
    map(() => actions.removeTeamSuccess(payload, meta)),
    catchError((error) => of(initError(error)))
  ))
)
