import { REMOVE_TEAM_SUCCESS, LEAVE_TEAM_SUCCESS } from './actions/types';
import { push } from 'connected-react-router';

const deleteTeamTriggeractions = [
  REMOVE_TEAM_SUCCESS,
  LEAVE_TEAM_SUCCESS
];

const deleteTeamRedirect = store => next => action => {
  if (deleteTeamTriggeractions.includes(action.type)) {
    const { router, team } = store.getState();

    // target url path part has index 1
    // example: host/{teamId}/members
    const pathnameArr = router.location.pathname.split('/');
    const paramTeamId = +pathnameArr[1];

    const teamId = team.getIn(['selectTeam', 'data', 'id']);

    if (team.get('isLoaded') && teamId === paramTeamId) {
      const firstOtherTeam = team.get('list').find(
        team => team.get('id') !== teamId
      );

      const rederectAction = !!firstOtherTeam
        ? push(`/${firstOtherTeam.get('id')}`)
        : push('/team/new');

      store.dispatch(rederectAction);
    };
  }

  return next(action);
}

export default [
  deleteTeamRedirect
]
