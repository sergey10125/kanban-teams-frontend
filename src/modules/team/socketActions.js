import * as actions from './actions/creators';

export default {
  'editTeamData': actions.updateTeamDataSuccess,
  'updateTeamRoles': actions.updateRolesInTeamNotification,
  'deleteTeam': actions.removeTeamSuccess
}
