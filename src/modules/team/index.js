import mergeObjectLists from 'utils/mergeObjectList';

import * as teamActionTypes from './actions/types';
import * as actionCreators from './actions/creators';
import * as teamEpics from './epics';
import teamReducer from './reducers';
import routes from './routes';
import teamMiddlewares from './middlewares'
import teamSocketActions from './socketActions';


import modules from './modules';

export const actionTypes = mergeObjectLists(
  teamActionTypes,
  modules.chat.actionTypes,
  modules.kanban.actionTypes,
  modules.member.actionTypes
);

export const reducers = mergeObjectLists(
  { team: teamReducer },
  modules.chat.reducers,
  modules.kanban.reducers,
  modules.member.reducers
);

export const epics = mergeObjectLists(
  teamEpics,
  modules.chat.epics,
  modules.kanban.epics,
  modules.member.epics
);

export const socketActions = mergeObjectLists(
  teamSocketActions,
  modules.chat.socketActions,
  modules.kanban.socketActions,
  modules.member.socketActions
);

export const middlewares = [
  ...modules.chat.middlewares,
  ...teamMiddlewares
];


export default {
  reducers,
  actionTypes,
  actionCreators,
  epics,
  routes,
  middlewares,
  socketActions
};
