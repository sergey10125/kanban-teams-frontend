import React, { useEffect } from 'react'
import { Switch, Route, Redirect, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";

import { createConnectionRequest } from 'root/actions/creators/socket';
import usePrevious from 'utils/hooks/usePrevious';
import NotFoundPage from 'components/NotFoundPage';

import { fetchTeamList } from '../actions/creators';
import Loader from 'components/Loader';

import CreateTeamForm from '../containers/form/TeamForm';

import SelectTeamRoutes from './selectTeam';
import RootRedirectRoute from './rootRedirect';
import JoinTeamRoute from './joinTeam';


const TeamRoutes = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const isLoaded = useSelector(({ team }) => team.get('isLoaded'));
  const loggedIn = useSelector(({ auth }) => auth.get('loggedIn'));
  const isSocketConnected = useSelector(({ socket }) => socket.get('isConnection'));

  const prevLoggedIn = usePrevious(loggedIn);

  useEffect(() => {
    if (!isLoaded && loggedIn) {
      dispatch(fetchTeamList());
    };
    if (isLoaded && loggedIn && !isSocketConnected) {
      dispatch(createConnectionRequest());
    };
  }, [ isLoaded, loggedIn, isSocketConnected, dispatch ]);


  if (!loggedIn) {
    const redirectTo = { pathname: "/auth/signIn" };
    if (!prevLoggedIn) redirectTo.state = { from: location };
    return <Redirect replace to={redirectTo} />;
  };

  if (!isLoaded || !isSocketConnected) return <Loader />;

  return (
    <Switch>
      <Route exact path='/' component={RootRedirectRoute} />
      <Route exact path='/team/new' component={CreateTeamForm} />
      <Route exact path='/team/join' component={JoinTeamRoute} />
      <Route path='/:teamId' component={SelectTeamRoutes} />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  );
};

export default TeamRoutes;
