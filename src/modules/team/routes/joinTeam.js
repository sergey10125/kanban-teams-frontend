import React, { useEffect } from 'react'
import { useDispatch } from "react-redux";
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'

import Loader from 'components/Loader';
import { confirmTeamInvite } from '../actions/creators';

const JoinTeamRoute = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    const { token } = queryString.parse(location.search);
    dispatch(confirmTeamInvite(token));
  }, [ location.search, dispatch ]);

  return <Loader />;
}

export default JoinTeamRoute;
