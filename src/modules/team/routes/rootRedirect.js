import React from 'react';
import { useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';
import Cookies from 'js-cookie';

const RootRedirect = () => {
  const teamList = useSelector(({ team }) => team.get('list'));
  if (!teamList.size) {
    return <Redirect to="/team/new" replace/>;
  };

  let teamId = +Cookies.get('selectTeamId');
  const teamIdExist = teamList.some(item => item.get('id') === teamId);
  if (!teamIdExist) {
    teamId =  teamList.first().get('id');
  };

  return <Redirect to={`/${teamId}`} replace />;
};

export default RootRedirect;
