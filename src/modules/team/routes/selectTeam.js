import React, { useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";
import Cookies from 'js-cookie';

import { joinSocketRoom, leaveSocketRoom } from 'root/actions/creators/socket';
import Loader from 'components/Loader';
import NotFoundPage from 'components/NotFoundPage';

import { fetchTeam } from '../actions/creators';
import Dashboard from '../containers/Dashboard';
import EditTeamForm from '../containers/form/EditTeamForm';


import memberModules from '../modules/member';
import kanbanModules from '../modules/kanban';
import chatModules from '../modules/chat';


const SelectTeamRoutes = ({ match: { params } }) => {
  const dispatch = useDispatch();

  const teamState = useSelector(({ team }) => team.get('selectTeam'));
  const teamId = teamState.getIn(['data', 'id']);
  const isLoaded = teamState.get('isLoaded');

  const isSocketConnected = teamState.get('isSocketConnected');
  const connectedToken = teamState.get('connectedToken');

  const paramTeamId = +params.teamId;

  useEffect(() => {
    if (teamId !== paramTeamId) {
      Cookies.set('selectTeamId', paramTeamId);
      dispatch(fetchTeam(paramTeamId));
      if (isSocketConnected) {
        dispatch(leaveSocketRoom('team', teamId));
      };
    };

    if (isLoaded && !isSocketConnected) {
      dispatch(joinSocketRoom(connectedToken));
    };
  });

  if (!isLoaded || !isSocketConnected) {
    return <Loader />;
  };


  return (
    <Switch>
      <Route exact path='/:teamId' component={Dashboard} />
      <Route exact path='/:teamId/edit' component={EditTeamForm} />
      <Route path='/:teamId/chats' component={chatModules.routes} />
      <Route path='/:teamId/kanbans' component={kanbanModules.routes} />
      <Route path='/:teamId/members' component={memberModules.routes} />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  )
}

export default SelectTeamRoutes;
