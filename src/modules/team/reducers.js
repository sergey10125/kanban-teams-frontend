import { Map, List, fromJS } from 'immutable';

import { LOGOUT_SUCCESS } from 'modules/auth/actions/types';
import {
  JOIN_SOCKET_ROOM_SUCCESS,
  DISCONNECT_CONNECTION
} from 'root/actions/types/socket';

import * as types from './actions/types'

const initSelectTeam = new Map({
  data: new Map({}),
  isLoaded: false,
  connectedToken: null,
  isSocketConnected: false,
})

const initialState = new Map({
  isLoaded: false,
  list: new List(),
  selectTeam: initSelectTeam
});

const fetchTeamsList = (state, payload) => {
  return state.merge({
    list: fromJS(payload),
    isLoaded: true
  })
};


const fetchTeam = (state, payload, meta) => {
  const { teamId } = meta;
  return state.set('selectTeam', initSelectTeam.update('data', data => {
    return data.set('id', teamId);
  }));
};

const fetchTeamSuccess = (state, payload, meta) => {
  const { team, invite } = payload;
  const { teamId } = meta;

  return state.update('selectTeam', teamState => {
    if (teamState.getIn(['data', 'id']) !== teamId) return teamState;
    return teamState.merge({
      data: fromJS(team),
      connectedToken: invite,
      isLoaded: true
    });
  });
}

const teamSocketConnected = (state, payload) => {
  if (payload.roomType !== 'team') return state;
  return state.update('selectTeam', team => {
    if (team.getIn(['data', 'id']) !== payload.id) return team;
    return team.set('isSocketConnected', true)
  });
};

const addTeam = (state, payload) => {
  if (!state.get('isLoaded')) return state;
  return state.update('list', list => list.unshift(fromJS(payload)));
};

const editTeamInfo = (state, payload, meta) => {
  const { title, description } = payload;
  const { teamId } = meta;

  if (!state.get('isLoaded')) return state;

  return state
    .update('list', list => {
      const index = list.findIndex(item => item.get('id') === teamId);
      if (index < 0) return list;
      return list.update(index, item => item.merge({ title }));
    })
    .update('selectTeam', teamState => {
      if (!teamState.get('isLoaded')) return teamState;
      return teamState.update('data', data => {
        if (data.get('id') !== teamId) return data;
        return data.merge({ title, description });
      })
    });
};

const updateTeamRole = (state, payload, meta) => {
  const { teamId } = meta;

  return state.update('selectTeam', teamState => {
    if (!teamState.get('isLoaded')) return teamState;
    return teamState.update('data', data => {
      if (data.get('id') !== teamId) return data;
      return data.set('userRoles', fromJS(payload));
    });
  });
};


const removeTeam = (state, payload, meta) => {
  const { teamId } = meta;

  if (!state.get('isLoaded')) return state;

  return state
    .update('list', list => list.filter(item => item.get('id') !== teamId))
    .update('selectTeam', team => {
      if (team.get('isLoaded') && team.getIn(['data', 'id']) === teamId) {
        return initSelectTeam;
      };

      return team;
    });
}


export default (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_TEAM_LIST_SUCCESS:
      return fetchTeamsList(state, action.payload, action.meta)
    case types.FETCH_TEAM:
      return fetchTeam(state, action.payload, action.meta)
    case types.FETCH_TEAM_SUCCESS:
      return fetchTeamSuccess(state, action.payload, action.meta)
    case JOIN_SOCKET_ROOM_SUCCESS:
      return teamSocketConnected(state, action.payload, action.meta)
    case types.CREATE_TEAM_SUCCESS:
    case types.CONFIRM_TEAM_INVITE_SUCCESS:
      return addTeam(state, action.payload, action.meta)
    case types.UPDATE_TEAM_DATA_SUCCESS:
      return editTeamInfo(state, action.payload, action.meta)
    case types.UPDATE_ROLES_IN_TEAM_NOTIFICATION:
      return updateTeamRole(state, action.payload, action.meta)
    case types.LEAVE_TEAM_SUCCESS:
    case types.REMOVE_TEAM_SUCCESS:
      return removeTeam(state, action.payload, action.meta)
    case DISCONNECT_CONNECTION:
    case LOGOUT_SUCCESS:
      return initialState
    default:
      return state;
  }
}
