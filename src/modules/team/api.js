import { from } from 'rxjs';
import request from 'utils/request';

const api = {
  fetchTeamList: () => {
    const req = request('/api/team')
      .then(response => response.json());

    return from(req);
  },
  confirmTeamInvite: (payload, meta) => {
    const req = request('/api/team/invite/accept', 'POST', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  fetchTeam: (payload, { teamId, ...meta }) => {
    const req = request(`/api/team/${teamId}`, 'GET', { meta })
      .then(response => response.json());

    return from(req);
  },
  createTeam: (payload, meta) => {
    const req = request('/api/team', 'POST', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  updateTeamData: (payload, { teamId, ...meta}) => {
    const req = request(`/api/team/${teamId}`, 'PUT', { payload, meta })
      .then(response => response.json());

    return from(req);
  },
  leaveTeam: (payload, { teamId, ...meta}) => {
    const url = `/api/team/${teamId}/leave`;
    const req = request(url, 'DELETE', { payload, meta });
    return from(req);
  },
  removeTeam: (payload, { teamId, ...meta }) => {
    const req = request(`/api/team/${teamId}`, 'DELETE', { meta });
    return from(req);
  },
};

export default api;
