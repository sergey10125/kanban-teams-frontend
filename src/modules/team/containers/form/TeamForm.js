import React, { useCallback } from 'react';
import { Field, reduxForm } from 'redux-form';
import { useDispatch } from "react-redux";

import Input from 'components/form/redux-form-adapter/Input';
import ErrorMessage from 'components/form/ErrorMessage';

import * as actions from '../../actions/creators';
import * as validations from 'utils/validations';


const minLength3 = validations.minLength(3);
const maxLength30 = validations.maxLength(30);


const TeamForm = ({ teamId, handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();

  const handleFunc = useCallback(({ title, description }) => {
    if (!teamId) {
      dispatch(actions.createTeam({ title, description }));
    } else {
      dispatch(actions.updateTeamData(teamId, { title, description }));
    }
  }, [ teamId, dispatch ]);


  return (
    <div className='page-form'>
      <div className="page-form__headline">
        {teamId? 'Update team' : 'Create a new team'}
      </div>
      <form className="form" onSubmit={handleSubmit(handleFunc)}>
        <fieldset className="form-fieldset">
          <Field
            component={Input}
            name="title"
            type="text"
            label="Team title"
            validate={[
              validations.isRequired,
              minLength3,
              maxLength30,
            ]}
            disabled={submitting}
          />
          <Field
            component={Input}
            name="description"
            label="Description"
            disabled={submitting}
          />
          <ErrorMessage error={error}/>
        </fieldset>
        <button disabled={submitting} className="btn btn--w200">
          {teamId? 'Update' : 'Create'}
        </button>
      </form>
    </div>
  )
}

export default reduxForm({
  form: 'teamForm'
})(TeamForm);
