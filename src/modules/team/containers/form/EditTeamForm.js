import React from 'react'
import { useSelector } from "react-redux";

import CompanyFormPage from './TeamForm';

const EditTeamForm = () => {
  const teamData = useSelector(({ team }) =>
    team.getIn(['selectTeam', 'data'])
  );

  const initialValues = {
    title: teamData.get('title'),
    description: teamData.get('description'),
  };

  return (
    <CompanyFormPage
      teamId={teamData.get('id')}
      initialValues={initialValues}
    />
  );
};

export default EditTeamForm;
