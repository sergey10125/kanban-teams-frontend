import React, { useMemo } from 'react'
import { push } from 'connected-react-router';
import { useDispatch, useSelector } from "react-redux";

import { openModal } from 'root/actions/creators/system';
import hasTeamPermissionInit from 'utils/hasTeamPermissionInit';
import * as actions from '../actions/creators';

import Dots from 'components/Dots';
import DropdownMenu from 'components/DropdownMenu';

import MemberWidget from '../modules/member/containers/Widget';
import KanbanWidget from '../modules/kanban/containers/Widget';
import ChatRoomWidget from '../modules/chat/containers/Widget';


const TeamDashboard = () => {
  const dispatch = useDispatch();

  const team = useSelector(({ team }) => team.getIn(['selectTeam', 'data']));
  const teamId = team.get('id');
  const hasTeamPermission = hasTeamPermissionInit(
    team.getIn(['userRoles', 'permissions'])
  );

  const hasOwnerPermissions = hasTeamPermission('ownerPermissions');

  const menuList = useMemo(() => {
    if (hasOwnerPermissions) {
      const redirectToEditForm = () => dispatch(push(`/${teamId}/edit`));
      const callDeleteModal = () => dispatch(openModal('ConfirmModal', {
        message: 'Are you sure you want to remove a team ?',
        actionFun: () => dispatch(actions.removeTeam(teamId))
      }))

      return  [
        { title: 'Edit', action: redirectToEditForm },
        { title: 'Delete', action: callDeleteModal }
      ];
    } else {
      const callLeaveModal = () => dispatch(openModal('ConfirmModal', {
        message: 'Are you sure you want to leave a team ?',
        actionFun: () => dispatch(actions.leaveTeam(teamId))
      }));

      return  [
        { title: 'Leave team', action: callLeaveModal },
      ];
    };
  }, [ teamId, dispatch, hasOwnerPermissions ]);

  return (
    <div className='page'>
      <div className='page__header'>
        <h1 className='page__headline'>{team.get('title')}</h1>
          <DropdownMenu className='page__dots' list={menuList}>
            <Dots size="large"/>
          </DropdownMenu>
      </div>
      <div className='page__description'>
        {team.get('description') &&
          team.get('description').split('\n').map((item, i) =>
            <div key={i}>{item}</div>
          )
        }
      </div>
      <div className='page__cards'>
        <MemberWidget />
        <KanbanWidget />
        <ChatRoomWidget />
      </div>
    </div>
  );
};

export default TeamDashboard;
