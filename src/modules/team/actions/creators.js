import * as types from './types'

export const fetchTeamList = () => ({
  type: types.FETCH_TEAM_LIST,
})
export const fetchTeamListSuccess = (payload) => ({
  type: types.FETCH_TEAM_LIST_SUCCESS,
  payload,
})

export const confirmTeamInvite = (token) => ({
  type: types.CONFIRM_TEAM_INVITE,
  payload: { token }
})
export const confirmTeamInviteSuccess = (payload, meta) => ({
  type: types.CONFIRM_TEAM_INVITE_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const fetchTeam = (teamId) => ({
  type: types.FETCH_TEAM,
  meta: { teamId },
})
export const fetchTeamSuccess = (payload, meta) => ({
  type: types.FETCH_TEAM_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const createTeam = (payload) => ({
  type: types.CREATE_TEAM,
  payload
})
export const createTeamSuccess = (payload, meta) => ({
  type: types.CREATE_TEAM_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const updateTeamData = (teamId, payload) => ({
  type: types.UPDATE_TEAM_DATA,
  payload,
  meta: { teamId }
})
export const updateTeamDataSuccess = (payload, meta) => ({
  type: types.UPDATE_TEAM_DATA_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const updateRolesInTeamNotification = (payload, meta) => ({
  type: types.UPDATE_ROLES_IN_TEAM_NOTIFICATION,
  payload: payload || {},
  meta: meta || {}
})

export const leaveTeam = (teamId) => ({
  type: types.LEAVE_TEAM,
  meta: { teamId }
})
export const leaveTeamSuccess = (payload, meta) => ({
  type: types.LEAVE_TEAM_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})

export const removeTeam = (teamId) => ({
  type: types.REMOVE_TEAM,
  meta: { teamId }
})
export const removeTeamSuccess = (payload, meta) => ({
  type: types.REMOVE_TEAM_SUCCESS,
  payload: payload || {},
  meta: meta || {}
})
