import React, { Fragment, useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";

import { setTabFocus } from "root/actions/creators/system";
import mergeObjectLists from 'utils/mergeObjectList'
import Loader from 'components/Loader';
import NotFoundPage from 'components/NotFoundPage';

import Header from 'root/containers/Header';
import Modal from 'root/containers/Modal';

import auth from './auth';
import team from './team';

export const socketActions = mergeObjectLists(auth.socketActions, team.socketActions);
export const actionTypes = mergeObjectLists(auth.actionTypes, team.actionTypes);
export const reducers = mergeObjectLists(auth.reducers, team.reducers);
export const epics = mergeObjectLists(auth.epics, team.epics);
export const middlewares = [
  ...team.middlewares
];


const AppRoutes = () => {
  const dispatch = useDispatch();
  const isAuthLoaded = useSelector(({ auth }) => auth.get('isLoaded'));
  const NotFoundState = useSelector(({ system }) => system.get('notFoundState'));


  useEffect(() => {
    const setFocus = () => dispatch(setTabFocus(true));
    const setBlur = () => dispatch(setTabFocus(false));
    window.addEventListener('focus', setFocus);
    window.addEventListener('blur', setBlur);
    return () => {
      window.removeEventListener('focus', setFocus);
      window.removeEventListener('blur', setBlur);
    }
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (!isAuthLoaded) {
      dispatch(auth.actionCreators.getUserData());
    }
  })


  if (!isAuthLoaded){
   return <Loader/>;
  }

  const isPageNotFound = (
    NotFoundState.get('status') &&
    NotFoundState.get('location') === 'global'
  );

  return (
    <Fragment>
      <Header/>
      <div className="page-wrap">
        {isPageNotFound && (<NotFoundPage />)}
        {!isPageNotFound && (
          <Switch>
            <Route path='/auth' component={auth.routes} />
            <Route path='/' component={team.routes} />
          </Switch>
        )}
      </div>
      <Modal/>
    </Fragment>
  )
}

export default AppRoutes;
