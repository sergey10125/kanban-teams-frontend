import {
  mergeMap,
  catchError,
  takeUntil,
  map,
} from 'rxjs/operators';
import { of, merge } from 'rxjs';
import { ofType } from 'redux-observable';
import { startSubmit } from 'redux-form';

import { initError } from 'root/actions/creators/system';

import * as types from './actions/types';
import * as actions from './actions/creators';
import api from './api';

export const getUserData = action$ => action$.pipe(
  ofType(types.GET_USER_DATA),
  mergeMap(action => api.getUserData().pipe(
    map(payload => actions.getUserDataSuccess(payload)),
    takeUntil(action$.ofType(types.GET_USER_DATA)),
    catchError(error => of(actions.getUserDataFailed())),
  ))
);

export const signUp = action$ => action$.pipe(
  ofType(types.SIGN_UP),
  mergeMap(action => merge(
    of(startSubmit('signUpForm')),
    api.signUp(action.payload).pipe(
      map(payload => actions.signInSuccess(payload)),
      catchError((error) => of(initError(error, {
        formName: 'signUpForm',
      })))
    ))
  )
);

export const signIn = action$ => action$.pipe(
  ofType(types.SIGN_IN),
  mergeMap(action => merge(
    of(startSubmit('signInForm')),
    api.signIn(action.payload).pipe(
      map(payload => actions.signInSuccess(payload)),
      catchError((error) => of(initError(error, {
        formName: 'signInForm',
      })))
    ))
  )
);

export const resetRequest = action$ => action$.pipe(
  ofType(types.RESET_PASSWORD_REQUEST),
  mergeMap(action => merge(
    of(startSubmit('resetRequestForm')),
    api.resetRequest(action.payload).pipe(
      map(() => actions.resetPasswordRequestSuccess()),
      catchError((error) => of(initError(error, {
        formName: 'resetRequestForm',
      })))
    ))
  )
);

export const tokenVerification = action$ => action$.pipe(
  ofType(types.CHECK_RESET_TOKEN),
  mergeMap(action => api.tokenVerification(action.payload).pipe(
    map(payload => actions.resetTokenValide()),
    catchError(error => of(actions.resetTokenInvalide())),
  ))
);

export const resetChangePass = action$ => action$.pipe(
  ofType(types.RESET_PASSWORD_CHANGE),
  mergeMap(({ payload }) => merge(
    of(startSubmit('resetPasswordChangeForm')),
    api.resetChangePass(payload).pipe(
      map(userdata => actions.resetPasswordChangeSuccess(userdata)),
      catchError((error) => of(initError(error, {
        formName: 'resetPasswordChangeForm',
      })))
    ))
  )
);

export const logout = action$ => action$.pipe(
  ofType(types.LOGOUT),
  mergeMap(action => api.logout().pipe(
    map(payload => actions.logoutSuccess()),
    catchError(error =>  of(initError(error)))
  ))
);
