import { from } from 'rxjs'
import request from 'utils/request'

const api = {
  getUserData: () => {
    const req = request('/api/auth/getUserData')
      .then(response => response.json());
      
    return from(req)
  },
  signUp: (payload) => {
    const req = request('/api/auth/signUp', 'POST', { payload })
      .then(response => response.json());
      
    return from(req)
  },
  signIn: (payload) => {
    const req = request('/api/auth/signIn', 'POST', { payload })
      .then(response => response.json());
      
    return from(req)
  },
  resetRequest: (payload) => {
    const req = request('/api/auth/resetRequest', 'POST', { payload });
    
    return from(req)
  },
  tokenVerification: (payload) => {
    const url = '/api/auth/resetTokenVerification';
    const req = request(url, 'POST', { payload });

    return from(req)
  },
  resetChangePass: (payload) => {
    const req = request('/api/auth/resetChangePass', 'POST', { payload })
      .then(response => response.json());

    return from(req)
  },
  logout: () => {
    const req = request('/api/auth/logout', 'POST');
    return from(req)
  }
};

export default api;
