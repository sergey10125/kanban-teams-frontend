import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { useSelector } from "react-redux";

import NotFoundPage from 'components/NotFoundPage';

import SignIn from './containers/SignIn';
import SignUp from './containers/SignUp';
import ResetRequest from './containers/ResetRequest';
import ChangePassword from './containers/ChangePassword';

const AuthRoutes = ({ location }) => {
  const loggedIn = useSelector(({ auth }) => auth.get('loggedIn'));
  let { from: fromPath } = location.state || { from: { pathname: "/" } };

  if (loggedIn) {
    return <Redirect to={fromPath} replace/>
  }

  return (
    <Switch>
      <Route path='/auth/signIn' component={SignIn} />
      <Route path='/auth/signUp' component={SignUp} />
      <Route path='/auth/restore' component={ResetRequest} />
      <Route path='/auth/changePass' component={ChangePassword} />
      <Route path='*' component={NotFoundPage} />
    </Switch>
  )
}

export default AuthRoutes;
