import * as types from './types'


export const getUserData = () => ({
  type: types.GET_USER_DATA,
})
export const getUserDataSuccess = (payload) => ({
  type: types.GET_USER_DATA_SUCCESS,
  payload,
})
export const getUserDataFailed = () => ({
  type: types.GET_USER_DATA_FAILED,
})


export const signUp = (userData) => ({
  type: types.SIGN_UP,
  payload: userData
})
export const signUpSuccess = (payload) => ({
  type: types.SIGN_UP_SUCCESS,
  payload
})

export const signIn = (email, password) => ({
  type: types.SIGN_IN,
  payload: { email, password }
})
export const signInSuccess = (payload) => ({
  type: types.SIGN_IN_SUCCESS,
  payload
})

export const logout = () => ({
  type: types.LOGOUT,
})
export const logoutSuccess = () => ({
  type: types.LOGOUT_SUCCESS,
})

export const resetPasswordRequest = (email) => ({
  type: types.RESET_PASSWORD_REQUEST,
  payload: { email }
})
export const resetPasswordRequestSuccess = () => ({
  type: types.RESET_PASSWORD_REQUEST_SUCCESS,
})

export const checkResetToken = (token) => ({
  type: types.CHECK_RESET_TOKEN,
  payload: { token }
})
export const resetTokenValide = () => ({
  type: types.CHECK_RESET_TOKEN_SUCCESS,
})
export const resetTokenInvalide = () => ({
  type: types.CHECK_RESET_TOKEN_FAILED,
})

export const resetPasswordChange = (token, password) => ({
  type: types.RESET_PASSWORD_CHANGE,
  payload: { token, password }
})
export const resetPasswordChangeSuccess = (payload) => ({
  type: types.RESET_PASSWORD_CHANGE_SUCCESS,
  payload
})
