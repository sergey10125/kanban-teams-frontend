import * as actionTypes from './actions/types'
import * as actionCreators from './actions/creators'
import * as epics from './epics'
import authReducers from './reducers'
import routes from './routes'
import socketActions from './socketActions';


const reducers = {
  auth: authReducers
}


export default {
  actionTypes,
  actionCreators,
  epics,
  reducers,
  routes,
  socketActions
}
