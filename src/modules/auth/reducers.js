import { Map, fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'connected-react-router';

import { DISCONNECT_CONNECTION } from 'root/actions/types/socket';

import * as types from './actions/types'

const initFlags = new Map({
  isResetTokenSend: false,
  isTokenChecked: false,
  isTokenValide: false,
})

const initialState = new Map({
  loggedIn: false,
  isLoaded: false,
  user: null,
  flags: initFlags
});


const setUserData = (state, userData) => {
  return state.merge({
    isLoaded: true,
    loggedIn: !!userData,
    user: fromJS(userData)
  });
};

const resetTokenSend = (state) => {
  return state.update('flags', flags => flags.set('isResetTokenSend', true));
};

const setTokenState = (state, status) => {
  return state.update('flags', flags => flags.merge({
    isTokenChecked: true,
    isTokenValide: status
  }));
};


const clearFlags = (state) => state.set('flags', initFlags);
const logout = () => initialState.set('isLoaded', true);


export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_USER_DATA_SUCCESS:
    case types.RESET_PASSWORD_CHANGE_SUCCESS:
    case types.SIGN_IN_SUCCESS:
      return setUserData(state, action.payload);
    case types.GET_USER_DATA_FAILED:
      return setUserData(state, null);


    case types.RESET_PASSWORD_REQUEST_SUCCESS:
      return resetTokenSend(state);
    case types.CHECK_RESET_TOKEN_SUCCESS:
      return setTokenState(state, true);
    case types.CHECK_RESET_TOKEN_FAILED:
      return setTokenState(state, false);

    case LOCATION_CHANGE:
      return clearFlags(state);
    case types.LOGOUT_SUCCESS:
      return logout();
    case DISCONNECT_CONNECTION:
      return initialState;
    default:
      return state;
  }
}
