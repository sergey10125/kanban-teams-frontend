import React, { useCallback } from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { useDispatch } from "react-redux";

import Input from 'components/form/redux-form-adapter/Input';
import ErrorMessage from 'components/form/ErrorMessage';
import { combineNormalizes, trim, lowerCase } from 'utils/form-normalizes';
import * as validations from 'utils/validations';

import { signUp } from '../actions/creators';

const trimCaseNormalize = combineNormalizes([ trim, lowerCase ]);
const minLength3 = validations.minLength(3);
const minLength6 = validations.minLength(6);


const SignUpForm = ({ handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();

  const handleFunc = useCallback(({ username, email, password }) => {
    email = trimCaseNormalize(email);
    dispatch(signUp({ username, email, password }));
  }, [ dispatch ]);

  return (
    <div className='auth-block'>
      <form
        className="form auth-block__form"
        onSubmit={handleSubmit(handleFunc)}
      >
        <fieldset className="form-fieldset">
          <Field
            component={Input}
            name="username"
            type="text"
            label="Username"
            validate={[ validations.isRequired, minLength3 ]}
            disabled={submitting}
          />
          <Field
            component={Input}
            name="email"
            type="text"
            label="E-mail"
            validate={[ validations.isRequired, validations.isEmail ]}
            disabled={submitting}
          />
          <Field
            component={Input}
            name="password"
            type="password"
            label="Password"
            validate={[
              validations.isRequired,
              validations.withoutSpace,
              validations.alphanumeric,
              minLength6
            ]}
            disabled={submitting}
          />
          <Field
            component={Input}
            name="repeatPass"
            type="password"
            label="Repeat password"
            validate={[ validations.isRequired, validations.repeatPass ]}
            disabled={submitting}
          />
          <ErrorMessage error={error} />
        </fieldset>
        <button disabled={submitting} className="btn">
          Sign Up
        </button>
      </form>
      <div className="auth-block__text auth-block__text--bottom-offset">
        Atready a member?&nbsp;
        <Link className='link' to='/auth/signin'>
          Sign In
        </Link>
      </div>
    </div>
  );
};

export default withRouter(
  reduxForm({
    form: 'signUpForm'
  })(SignUpForm)
)
