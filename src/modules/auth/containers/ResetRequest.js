import React, { useCallback } from 'react';
import { Field, reduxForm } from 'redux-form';
import { useDispatch, useSelector } from "react-redux";

import Input from 'components/form/redux-form-adapter/Input';
import ErrorMessage from 'components/form/ErrorMessage';
import * as validations from 'utils/validations';

import { resetPasswordRequest } from '../actions/creators';


const ResetRequestForm = ({ handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();

  const isTokenSend = useSelector(({ auth }) =>
    auth.getIn(['flags', 'isResetTokenSend'])
  );


  const handleFunc = useCallback(({ email }) => {
    email = email.trim();
    dispatch(resetPasswordRequest(email));
  }, [ dispatch ]);


  if (isTokenSend) {
    return (
      <div className='auth-block'>
        Mail is send
      </div>
    )
  }


  return (
    <div className='auth-block'>
      <div className="auth-block__text auth-block__text--bottom-offset auth-block__text--headline">
        Please enter the email that you used to sign in.
      </div>
      <form
        className="form auth-block__form"
        onSubmit={handleSubmit(handleFunc)}
      >
        <fieldset className="form-fieldset">
          <Field
            component={Input}
            name="email"
            type="text"
            label="E-mail"
            validate={[ validations.isRequired, validations.isEmail ]}
            disabled={submitting}
          />
        <ErrorMessage error={error}/>
        </fieldset>
        <button disabled={submitting} className="btn">
          Next
        </button>
      </form>
    </div>
  )
}

export default reduxForm({
  form: 'resetRequestForm'
})(ResetRequestForm);
