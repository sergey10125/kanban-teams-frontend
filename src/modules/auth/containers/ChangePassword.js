import React, { useEffect, useCallback } from 'react'
import { useLocation } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import { useDispatch, useSelector } from "react-redux";
import queryString from 'query-string';

import Input from 'components/form/redux-form-adapter/Input'
import ErrorMessage from 'components/form/ErrorMessage'
import Loader from 'components/Loader'
import NotFoundPage from 'components/NotFoundPage'

import * as validations from 'utils/validations';

import {
  checkResetToken,
  resetTokenInvalide,
  resetPasswordChange
} from '../actions/creators'

const minLength6 = validations.minLength(6);


const ChangePasswordForm = ({ handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();
  const location = useLocation();

  const authFlags = useSelector(({ auth }) => auth.get('flags'));

  const handleFunc = useCallback(({ password }) => {
    const { token } = queryString.parse(location.search);
    password = password.trim();
    dispatch(resetPasswordChange(token, password));
  }, [ dispatch, location ]);

  useEffect(() => {
    const { token } = queryString.parse(location.search);
    const actionCreator = !token? resetTokenInvalide() : checkResetToken(token);
    dispatch(actionCreator);
  // eslint-disable-next-line
  }, []);


  if (!authFlags.get('isTokenChecked')) return <Loader />;
  if (!authFlags.get('isTokenValide')) return <NotFoundPage />;

  return (
    <div className='auth-block'>
      <form
        className="form auth-block__form"
        onSubmit={handleSubmit(handleFunc)}
      >
        <fieldset className="form-fieldset">
          <Field
            component={Input}
            name="password"
            type="password"
            label="Password"
            validate={[ validations.isRequired, minLength6 ]}
            disabled={submitting}
          />
          <Field
            component={Input}
            name="repeatPass"
            type="password"
            label="Repeat password"
            validate={[ validations.isRequired, validations.repeatPass ]}
            disabled={submitting}
          />
        <ErrorMessage error={error}/>
        </fieldset>
        <button disabled={submitting} className="btn">
          Change password
        </button>
      </form>
    </div>
  )
}

export default reduxForm({
  form: 'resetPasswordChangeForm'
})(ChangePasswordForm);
