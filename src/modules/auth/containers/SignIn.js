import React, { useCallback } from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { useDispatch } from "react-redux";

import Input from 'components/form/redux-form-adapter/Input';
import ErrorMessage from 'components/form/ErrorMessage';
import * as validations from 'utils/validations';
import { combineNormalizes, trim, lowerCase } from 'utils/form-normalizes';

import { signIn } from '../actions/creators';

const trimCaseNormalize = combineNormalizes([ trim, lowerCase ]);


const SignInForm = ({ handleSubmit, submitting, error }) => {
  const dispatch = useDispatch();

  const handleFunc = useCallback(({ email, password }) => {
    email = trimCaseNormalize(email);
    dispatch(signIn(email, password))
  }, [ dispatch ]);

  return (
    <div className='auth-block'>
      <form
        className="form auth-block__form"
        onSubmit={handleSubmit(handleFunc)}
      >
        <fieldset className="form-fieldset">
          <Field
            component={Input}
            name="email"
            type="text"
            label="E-mail"
            validate={[ validations.isRequired, validations.isEmail ]}
            disabled={submitting}
          />
          <Field
            component={Input}
            name="password"
            type="password"
            label="password"
            validate={[ validations.isRequired ]}
            disabled={submitting}
          />
          <ErrorMessage error={error}/>
        </fieldset>
        <button disabled={submitting} className="btn">
          Sign In
        </button>
      </form>
      <div className="auth-block__text auth-block__text--bottom-offset">
        Not registered?&nbsp;
        <Link className='link' to='/auth/signup'>
          Create an account
        </Link>
      </div>
      <div className="auth-block__text">
        <Link className='link' to='/auth/restore'>
          Forgotten password ?
        </Link>
      </div>
    </div>
  );
};

export default reduxForm({
  form: 'signInForm'
})(SignInForm);
